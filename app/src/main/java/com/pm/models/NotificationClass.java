package com.pm.models;

public class NotificationClass {
    public int id;
    public String DATE;
    public String AUFNR;
    public String OBJID;
    public String TIME;
    public String NPRIOK;

    public NotificationClass() { }

    public NotificationClass(int id, String DATE, String AUFNR, String OBJID, String TIME, String NPRIOK) {
        this.id = id;
        this.DATE = DATE;
        this.AUFNR = AUFNR;
        this.OBJID = OBJID;
        this.TIME = TIME;
        this.NPRIOK = NPRIOK;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAUFNR() {
        return AUFNR;
    }

    public void setAUFNR(String AUFNR) {
        this.AUFNR = AUFNR;
    }

    public String getOBJID() {
        return OBJID;
    }

    public void setOBJID(String OBJID) {
        this.OBJID = OBJID;
    }

    public String getTIME() {
        return TIME;
    }

    public void setTIME(String TIME) {
        this.TIME = TIME;
    }

    public String getNPRIOK() {
        return NPRIOK;
    }

    public void setNPRIOK(String NPRIOK) {
        this.NPRIOK = NPRIOK;
    }
}
