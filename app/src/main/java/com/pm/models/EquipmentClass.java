package com.pm.models;

public class EquipmentClass {
    public int id;
    private String code;
    private String objectNumber;
    private String indicatorABC;
    private String catalogProfile;
    private String technicalLocationCode;
    private String siteCenter;
    private String site;
    private String description;

    public EquipmentClass(){ }

    public EquipmentClass(int _id, String _code, String _objectNumber, String _indicatorABC, String _catalogProfile, String _technicalLocationCode, String _siteCenter, String _site, String _description) {
        this.id = _id;
        this.code = _code;
        this.objectNumber = _objectNumber;
        this.indicatorABC = _indicatorABC;
        this.catalogProfile = _catalogProfile;
        this.technicalLocationCode = _technicalLocationCode;
        this.siteCenter = _siteCenter;
        this.site = _site;
        this.description = _description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getObjectNumber() {
        return objectNumber;
    }

    public void setObjectNumber(String objectNumber) {
        this.objectNumber = objectNumber;
    }

    public String getIndicatorABC() {
        return indicatorABC;
    }

    public void setIndicatorABC(String indicatorABC) {
        this.indicatorABC = indicatorABC;
    }

    public String getCatalogProfile() {
        return catalogProfile;
    }

    public void setCatalogProfile(String catalogProfile) {
        this.catalogProfile = catalogProfile;
    }

    public String getTechnicalLocationCode() {
        return technicalLocationCode;
    }

    public void setTechnicalLocationCode(String technicalLocationCode) {
        this.technicalLocationCode = technicalLocationCode;
    }

    public String getSiteCenter() {
        return siteCenter;
    }

    public void setSiteCenter(String siteCenter) {
        this.siteCenter = siteCenter;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
