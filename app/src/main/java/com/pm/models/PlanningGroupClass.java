package com.pm.models;

public class PlanningGroupClass {

    public int id;
    public String planningGroup;
    public String description;

    public PlanningGroupClass() { }

    public PlanningGroupClass(int _id, String _planningGroup, String _description){
        this.id = _id;
        this.planningGroup = _planningGroup;
        this.description = _description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlanningGroup() {
        return planningGroup;
    }

    public void setPlanningGroup(String planningGroup) {
        this.planningGroup = planningGroup;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
