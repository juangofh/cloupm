package com.pm.models;

public class OTMeasurementPointClass {
    public int id;
    public String numberOrder;
    public String numberPoint;
    public String equipment;
    public String equipmentName;
    public String objUser;
    public String _date;
    public String hour;
    public String status;
    public String observation;
    public String measuredValue;

    public OTMeasurementPointClass(){ }

    public OTMeasurementPointClass(int id, String _numberOrder, String _numberPoint, String _equipment, String _equipmentName, String _objUser, String __date, String _hour,
        String _status, String _observation, String _measuredValue) {
        this.id = id;
        this.numberOrder = _numberOrder;
        this.numberPoint = _numberPoint;
        this.equipment = _equipment;
        this.equipmentName = _equipmentName;
        this.objUser = _objUser;
        this._date =__date;
        this.hour = _hour;
        this.status = _status;
        this.observation = _observation;
        this.measuredValue = _measuredValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumberOrder() {
        return numberOrder;
    }

    public void setNumberOrder(String numberOrder) {
        this.numberOrder = numberOrder;
    }

    public String getNumberPoint() {
        return numberPoint;
    }

    public void setNumberPoint(String numberPoint) {
        this.numberPoint = numberPoint;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getObjUser() {
        return objUser;
    }

    public void setObjUser(String objUser) {
        this.objUser = objUser;
    }

    public String get_date() {
        return _date;
    }

    public void set_date(String _date) {
        this._date = _date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getMeasuredValue() {
        return measuredValue;
    }

    public void setMeasuredValue(String measuredValue) {
        this.measuredValue = measuredValue;
    }
}
