package com.pm.models;

public class PlanningGroupCenterClass {
    public int id;
    public String planningGroupCode;
    public String planningCenterCode;

    public PlanningGroupCenterClass(){ }

    public PlanningGroupCenterClass(int _id, String _planningGroupCode, String _planningCenterCode){
        this.id = _id;
        this.planningGroupCode = _planningGroupCode;
        this.planningCenterCode = _planningCenterCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlanningGroupCode() {
        return planningGroupCode;
    }

    public void setPlanningGroupCode(String planningGroupCode) {
        this.planningGroupCode = planningGroupCode;
    }

    public String getPlanningCenterCode() {
        return planningCenterCode;
    }

    public void setPlanningCenterCode(String planningCenterCode) {
        this.planningCenterCode = planningCenterCode;
    }
}
