package com.pm.models;

public class JobPositionClass {
    public int id;
    public String objectId;
    public String description;

    public JobPositionClass(){ }

    public JobPositionClass(int _id, String _objectId, String _description){
        this.id = _id;
        this.objectId = _objectId;
        this.description = _description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
