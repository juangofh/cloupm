package com.pm.models;

public class SupportTicketClass {
    public int id;
    private String ticketNumber;
    private String noticeType;
    private String nnoticeType;
    private String center;
    private String location;
    private String nlocation;
    private String equipment;
    private String nequipment;
    private String noticeDate;
    private String noticeHour;
    private String symptomGroupCode;
    private String symptomGroupSymptom;
    private String nsymptomGroupSymptom;
    private String causeText;
    private String stoppedEquipment;
    private String approval;
    private String load;

    public SupportTicketClass(){ }

    public SupportTicketClass(int id, String ticketNumber, String noticeType, String nnoticeType, String center, String location, String nlocation, String equipment, String nequipment, String noticeDate, String noticeHour, String symptomGroupCode, String symptomGroupSymptom, String nsymptomGroupSymptom, String causeText, String stoppedEquipment, String approval, String _load) {
        this.id = id;
        this.ticketNumber = ticketNumber;
        this.noticeType = noticeType;
        this.nnoticeType = nnoticeType;
        this.center = center;
        this.location = location;
        this.nlocation = nlocation;
        this.equipment = equipment;
        this.nequipment = nequipment;
        this.noticeDate = noticeDate;
        this.noticeHour = noticeHour;
        this.symptomGroupCode = symptomGroupCode;
        this.symptomGroupSymptom = symptomGroupSymptom;
        this.nsymptomGroupSymptom = nsymptomGroupSymptom;
        this.causeText = causeText;
        this.stoppedEquipment = stoppedEquipment;
        this.approval = approval;
        this.load = _load;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(String noticeType) {
        this.noticeType = noticeType;
    }

    public String getNnoticeType() {
        return nnoticeType;
    }

    public void setNnoticeType(String nnoticeType) {
        this.nnoticeType = nnoticeType;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setNlocation(String nlocation) {
        this.nlocation = nlocation;
    }

    public String getNlocation() {
        return nlocation;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getNequipment() {
        return nequipment;
    }

    public void setNequipment(String nequipment) {
        this.nequipment = nequipment;
    }

    public String getNoticeDate() {
        return noticeDate;
    }

    public void setNoticeDate(String noticeDate) {
        this.noticeDate = noticeDate;
    }

    public String getNoticeHour() {
        return noticeHour;
    }

    public void setNoticeHour(String noticeHour) {
        this.noticeHour = noticeHour;
    }

    public String getSymptomGroupCode() {
        return symptomGroupCode;
    }

    public void setSymptomGroupCode(String symptomGroupCode) {
        this.symptomGroupCode = symptomGroupCode;
    }

    public String getSymptomGroupSymptom() {
        return symptomGroupSymptom;
    }

    public void setSymptomGroupSymptom(String symptomGroupSymptom) {
        this.symptomGroupSymptom = symptomGroupSymptom;
    }

    public String getNsymptomGroupSymptom() {
        return nsymptomGroupSymptom;
    }

    public void setNsymptomGroupSymptom(String nsymptomGroupSymptom) {
        this.nsymptomGroupSymptom = nsymptomGroupSymptom;
    }

    public String getCauseText() {
        return causeText;
    }

    public void setCauseText(String causeText) {
        this.causeText = causeText;
    }

    public String getStoppedEquipment() {
        return stoppedEquipment;
    }

    public void setStoppedEquipment(String stoppedEquipment) {
        this.stoppedEquipment = stoppedEquipment;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public String getLoad() {
        return load;
    }

    public void setLoad(String load) {
        this.load = load;
    }
}
