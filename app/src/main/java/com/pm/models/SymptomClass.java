package com.pm.models;

public class SymptomClass {
    public int id;
    private String catalogProfile;
    private String symptomCode;
    private String groupCode;
    private String version;
    private String inactive;
    private String description;

    public  SymptomClass(){}

    public SymptomClass(int _id, String _catalogProfile, String _symptomCode, String _groupCode, String _version,
        String _inactive, String _description) {
        this.id = _id;
        this.catalogProfile = _catalogProfile;
        this.symptomCode = _symptomCode;
        this.groupCode = _groupCode;
        this.version = _version;
        this.inactive = _inactive;
        this.description = _description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCatalogProfile() {
        return catalogProfile;
    }

    public void setCatalogProfile(String catalogProfile) {
        this.catalogProfile = catalogProfile;
    }

    public String getSymptomCode() {
        return symptomCode;
    }

    public void setSymptomCode(String symptomCode) {
        this.symptomCode = symptomCode;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getInactive() {
        return inactive;
    }

    public void setInactive(String inactive) {
        this.inactive = inactive;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
