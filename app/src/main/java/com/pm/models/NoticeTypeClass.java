package com.pm.models;

public class NoticeTypeClass {
    public int id;
    private String code;
    private String description;
    private String status;

    public NoticeTypeClass(){ }

    public NoticeTypeClass(int _id, String _code, String _description, String _status) {
        this.id = _id;
        this.code = _code;
        this.description = _description;
        this.status = _status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
