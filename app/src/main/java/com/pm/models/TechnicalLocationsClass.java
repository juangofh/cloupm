package com.pm.models;

public class TechnicalLocationsClass {
    public int id;
    public String technicalLocationCode;
    public String descriptionLocation;
    public String siteCenter;

    public TechnicalLocationsClass() { }

    public TechnicalLocationsClass(int _id, String _technicalLocationCode, String _descriptionLocation, String _siteCenter) {
        this.id = _id;
        this.technicalLocationCode = _technicalLocationCode;
        this.descriptionLocation = _descriptionLocation;
        this.siteCenter = _siteCenter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTechnicalLocationCode() {
        return technicalLocationCode;
    }

    public void setTechnicalLocationCode(String technicalLocationCode) {
        this.technicalLocationCode = technicalLocationCode;
    }

    public String getDescriptionLocation() {
        return descriptionLocation;
    }

    public void setDescriptionLocation(String descriptionLocation) {
        this.descriptionLocation = descriptionLocation;
    }

    public String getSiteCenter() {
        return siteCenter;
    }

    public void setSiteCenter(String siteCenter) {
        this.siteCenter = siteCenter;
    }
}

