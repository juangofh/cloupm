package com.pm.models;

public class PriorityClass {
    public int id;
    public String priorityCode;
    public String description;

    public PriorityClass(){ }

    public PriorityClass(int _id, String _priorityCode, String _description){
        this.id = _id;
        this.priorityCode = _priorityCode;
        this.description = _description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPriorityCode() {
        return priorityCode;
    }

    public void setPriorityCode(String priorityCode) {
        this.priorityCode = priorityCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
