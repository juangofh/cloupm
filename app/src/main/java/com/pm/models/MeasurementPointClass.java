package com.pm.models;

public class MeasurementPointClass {
    public int id;
    public String measurementPointCode;
    public String measurementPointObject;
    public String innerFeature;
    public String measurementPointDescription;

    public MeasurementPointClass(){ }

    public MeasurementPointClass(int _id, String _measurementPointCode, String _measurementPointObject, String _innerFeature, String _measurementPointDescription){
        this.id = _id;
        this.measurementPointCode = _measurementPointCode;
        this.measurementPointObject = _measurementPointObject;
        this.innerFeature = _innerFeature;
        this.measurementPointDescription = _measurementPointDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMeasurementPointCode() {
        return measurementPointCode;
    }

    public void setMeasurementPointCode(String measurementPointCode) {
        this.measurementPointCode = measurementPointCode;
    }

    public String getMeasurementPointObject() {
        return measurementPointObject;
    }

    public void setMeasurementPointObject(String measurementPointObject) {
        this.measurementPointObject = measurementPointObject;
    }

    public String getInnerFeature() {
        return innerFeature;
    }

    public void setInnerFeature(String innerFeature) {
        this.innerFeature = innerFeature;
    }

    public String getMeasurementPointDescription() {
        return measurementPointDescription;
    }

    public void setMeasurementPointDescription(String measurementPointdescription) {
        this.measurementPointDescription = measurementPointdescription;
    }

}
