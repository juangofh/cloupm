package com.pm.models;

public class CenterClass {
    public int id;
    private String centerCode;
    private String centerName;
    private String society;
    private String latitude;
    private String longitude;

    public  CenterClass () { }

    public CenterClass(int _id, String _centerCode, String _centerName, String _society, String _latitude, String _longitude) {
        this.id = _id;
        this.centerCode = _centerCode;
        this.centerName = _centerName;
        this.society = _society;
        this.latitude = _latitude;
        this.longitude = _longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getSociety() {
        return society;
    }

    public void setSociety(String society) {
        this.society = society;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
