package com.pm.models;

public class OTOperationsClass {
    public int id;
    public String numberOrder;
    public String numberOperation;
    public String jobTitle;
    public String jobTitleName;
    public String center;
    public String centerName;
    public String objID;
    public String controlKey;
    public String shortText;
    public String startDate;
    public String startTime;
    public String endDate;
    public String endTime;
    public String objUser;
    public String status;
    public String observation;
    public String timeRequired;

    public OTOperationsClass(){ }

    public OTOperationsClass(int id, String _numberOrder, String _numberOperation, String _jobTitle, String _jobTitleName, String _center, String _centerName, String _objID,
         String _controlKey, String _shortText, String _startDate, String _startTime, String _endDate, String _endTime, String _objUser, String _status,
         String _observation, String _timeRequired) {
        this.id = id;
        this.numberOrder = _numberOrder;
        this.numberOperation = _numberOperation;
        this.jobTitle = _jobTitle;
        this.jobTitleName = _jobTitleName;
        this.center = _center;
        this.centerName = _centerName;
        this.objID = _objID;
        this.controlKey = _controlKey;
        this.shortText = _shortText;
        this.startDate = _startDate;
        this.startTime = _startTime;
        this.endDate = _endDate;
        this.endTime = _endTime;
        this.objUser = _objUser;
        this.status = _status;
        this.observation = _observation;
        this.timeRequired = _timeRequired;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumberOrder() {
        return numberOrder;
    }

    public void setNumberOrder(String numberOrder) {
        this.numberOrder = numberOrder;
    }

    public String getNumberOperation() {
        return numberOperation;
    }

    public void setNumberOperation(String numberOperation) {
        this.numberOperation = numberOperation;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobTitleName() {
        return jobTitleName;
    }

    public void setJobTitleName(String jobTitleName) {
        this.jobTitleName = jobTitleName;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getObjID() {
        return objID;
    }

    public void setObjID(String objID) {
        this.objID = objID;
    }

    public String getControlKey() {
        return controlKey;
    }

    public void setControlKey(String controlKey) {
        this.controlKey = controlKey;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getObjUser() {
        return objUser;
    }

    public void setObjUser(String objUser) {
        this.objUser = objUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getTimeRequired() {
        return timeRequired;
    }

    public void setTimeRequired(String timeRequired) {
        this.timeRequired = timeRequired;
    }
}
