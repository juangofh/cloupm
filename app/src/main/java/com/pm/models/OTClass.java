package com.pm.models;

public class OTClass {


    public int id;
    public String numberOrder;
    public String classOrder;
    public String classOrderName;
    public String planningGroup;
    public String planningGroupName;
    public String planningCenter;
    public String planningCenterName;
    public String jobTitle;
    public String jobTitleName;
    public String jobCenter;
    public String jobCenterName;
    public String objID;
    public String objID2;
    public String siteCenter;
    public String siteCenterName;
    public String text;
    public String extremeStartDate;
    public String extremeEndDate;
    public String startTime;
    public String endTime;
    public String orderReference;
    public String priority;
    public String priorityName;
    public String technicalLocation;
    public String technicalLocationName;
    public String equipment;
    public String equipmentName;
    public String objUser;
    public String technicianClosing;
    public String clientClosure;
    public String observation;
    public String observationc;
    public String workStartDate;
    public String workStartTime;
    public String workEndDate;
    public String endTimeWork;
    public String totalTimeHours;

    public String technicianValue;
    public String technicianObservation;
    public String supervisorValue;
    public String supervisorObservation;

    public String status;
    public String load;


    //Solo se puede editar si es tecnico
    public String created; //Agregar en BD
    public String obsi; //observacion del tecnico OBSI ||   Ceci me lo retorna

    //Si la ot ha sido enviada uso el servicio set incidence es decir load == 1



    public OTClass(int id, String numberOrder, String classOrder, String classOrderName, String planningGroup,
                   String planningGroupName, String planningCenter, String planningCenterName, String jobTitle, String jobTitleName,
                   String jobCenter, String jobCenterName, String objID, String objID2, String siteCenter, String siteCenterName,
                   String text, String extremeStartDate, String extremeEndDate, String startTime, String endTime,
                   String orderReference, String priority, String priorityName, String technicalLocation,
                   String technicalLocationName, String equipment, String equipmentName, String objUser, String technicianClosing,
                   String clientClosure, String observation, String observationc, String workStartDate, String workStartTime,
                   String workEndDate, String endTimeWork, String totalTimeHours, String technicianValue,
                   String technicianObservation, String supervisorValue, String supervisorObservation, String status, String load,
                   String created, String obsi) {
        this.id = id;
        this.numberOrder = numberOrder;
        this.classOrder = classOrder;
        this.classOrderName = classOrderName;
        this.planningGroup = planningGroup;
        this.planningGroupName = planningGroupName;
        this.planningCenter = planningCenter;
        this.planningCenterName = planningCenterName;
        this.jobTitle = jobTitle;
        this.jobTitleName = jobTitleName;
        this.jobCenter = jobCenter;
        this.jobCenterName = jobCenterName;
        this.objID = objID;
        this.objID2 = objID2;
        this.siteCenter = siteCenter;
        this.siteCenterName = siteCenterName;
        this.text = text;
        this.extremeStartDate = extremeStartDate;
        this.extremeEndDate = extremeEndDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.orderReference = orderReference;
        this.priority = priority;
        this.priorityName = priorityName;
        this.technicalLocation = technicalLocation;
        this.technicalLocationName = technicalLocationName;
        this.equipment = equipment;
        this.equipmentName = equipmentName;
        this.objUser = objUser;
        this.technicianClosing = technicianClosing;
        this.clientClosure = clientClosure;
        this.observation = observation;
        this.observationc = observationc;
        this.workStartDate = workStartDate;
        this.workStartTime = workStartTime;
        this.workEndDate = workEndDate;
        this.endTimeWork = endTimeWork;
        this.totalTimeHours = totalTimeHours;
        this.technicianValue = technicianValue;
        this.technicianObservation = technicianObservation;
        this.supervisorValue = supervisorValue;
        this.supervisorObservation = supervisorObservation;
        this.status = status;
        this.load = load;
        this.created = created;
        this.obsi = obsi;
    }

    public OTClass(int _id, String _numberOrder, String _classOrder, String _classOrderName, String _planningGroup, String _planningGroupName, String _planningCenter, String _planningCenterName,
                   String _jobTitle, String _jobTitleName, String _jobCenter, String _jobCenterName, String _objID, String _objID2, String _siteCenter, String _siteCenterName, String _text, String _extremeStartDate, String _extremeEndDate,
                   String _startTime, String _endTime, String _orderReference, String _priority, String _priorityName, String _technicalLocation, String _technicalLocationName, String _equipment, String _equipmentName, String _objUser,
                   String _technicianClosing, String _clientClosure, String _observation, String _observationc, String _workStartDate, String _workStartTime, String _workEndDate, String _endTimeWork, String _totalTimeHours, String _status, String _load) {
        this.id = _id;
        this.numberOrder = _numberOrder;
        this.classOrder = _classOrder;
        this.classOrderName = _classOrderName;
        this.planningGroup = _planningGroup;
        this.planningGroupName = _planningGroupName;
        this.planningCenter = _planningCenter;
        this.planningCenterName = _planningCenterName;
        this.jobTitle = _jobTitle;
        this.jobTitleName = _jobTitleName;
        this.jobCenter = _jobCenter;
        this.jobCenterName = _jobCenterName;
        this.objID = _objID;
        this.objID2 = _objID2;
        this.siteCenter = _siteCenter;
        this.siteCenterName = _siteCenterName;
        this.text = _text;
        this.extremeStartDate = _extremeStartDate;
        this.extremeEndDate = _extremeEndDate;
        this.startTime = _startTime;
        this.endTime = _endTime;
        this.orderReference = _orderReference;
        this.priority = _priority;
        this.priorityName = _priorityName;
        this.technicalLocation = _technicalLocation;
        this.technicalLocationName = _technicalLocationName;
        this.equipment = _equipment;
        this.equipmentName = _equipmentName;
        this.objUser = _objUser;
        this.technicianClosing = _technicianClosing;
        this.clientClosure = _clientClosure;
        this.observation = _observation;
        this.observationc = _observationc;
        this.workStartDate = _workStartDate;
        this.workStartTime = _workStartTime;
        this.workEndDate = _workEndDate;
        this.endTimeWork = _endTimeWork;
        this.totalTimeHours = _totalTimeHours;
        this.status = _status;
        this.load = _load;
    }

    public OTClass() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumberOrder() {
        return numberOrder;
    }

    public void setNumberOrder(String numberOrder) {
        this.numberOrder = numberOrder;
    }

    public String getClassOrder() {
        return classOrder;
    }

    public void setClassOrder(String classOrder) {
        this.classOrder = classOrder;
    }

    public String getClassOrderName() {
        return classOrderName;
    }

    public void setClassOrderName(String classOrderName) {
        this.classOrderName = classOrderName;
    }

    public String getPlanningGroup() {
        return planningGroup;
    }

    public void setPlanningGroup(String planningGroup) {
        this.planningGroup = planningGroup;
    }

    public String getPlanningGroupName() {
        return planningGroupName;
    }

    public void setPlanningGroupName(String planningGroupName) {
        this.planningGroupName = planningGroupName;
    }

    public String getPlanningCenter() {
        return planningCenter;
    }

    public void setPlanningCenter(String planningCenter) {
        this.planningCenter = planningCenter;
    }

    public String getPlanningCenterName() {
        return planningCenterName;
    }

    public void setPlanningCenterName(String planningCenterName) {
        this.planningCenterName = planningCenterName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobTitleName() {
        return jobTitleName;
    }

    public void setJobTitleName(String jobTitleName) {
        this.jobTitleName = jobTitleName;
    }

    public String getJobCenter() {
        return jobCenter;
    }

    public void setJobCenter(String jobCenter) {
        this.jobCenter = jobCenter;
    }

    public String getJobCenterName() {
        return jobCenterName;
    }

    public void setJobCenterName(String jobCenterName) {
        this.jobCenterName = jobCenterName;
    }

    public String getObjID() {
        return objID;
    }

    public void setObjID(String objID) {
        this.objID = objID;
    }

    public String getObjID2() {
        return objID2;
    }

    public void setObjID2(String objID2) {
        this.objID2 = objID2;
    }

    public String getSiteCenter() {
        return siteCenter;
    }

    public void setSiteCenter(String siteCenter) {
        this.siteCenter = siteCenter;
    }

    public String getSiteCenterName() {
        return siteCenterName;
    }

    public void setSiteCenterName(String siteCenterName) {
        this.siteCenterName = siteCenterName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getExtremeStartDate() {
        return extremeStartDate;
    }

    public void setExtremeStartDate(String extremeStartDate) {
        this.extremeStartDate = extremeStartDate;
    }

    public String getExtremeEndDate() {
        return extremeEndDate;
    }

    public void setExtremeEndDate(String extremeEndDate) {
        this.extremeEndDate = extremeEndDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOrderReference() {
        return orderReference;
    }

    public void setOrderReference(String orderReference) {
        this.orderReference = orderReference;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPriorityName() {
        return priorityName;
    }

    public void setPriorityName(String priorityName) {
        this.priorityName = priorityName;
    }

    public String getTechnicalLocation() {
        return technicalLocation;
    }

    public void setTechnicalLocation(String technicalLocation) {
        this.technicalLocation = technicalLocation;
    }

    public String getTechnicalLocationName() {
        return technicalLocationName;
    }

    public void setTechnicalLocationName(String technicalLocationName) {
        this.technicalLocationName = technicalLocationName;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getObjUser() {
        return objUser;
    }

    public void setObjUser(String objUser) {
        this.objUser = objUser;
    }

    public String getTechnicianClosing() {
        return technicianClosing;
    }

    public void setTechnicianClosing(String technicianClosing) {
        this.technicianClosing = technicianClosing;
    }

    public String getClientClosure() {
        return clientClosure;
    }

    public void setClientClosure(String clientClosure) {
        this.clientClosure = clientClosure;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getObservationc() {
        return observationc;
    }

    public void setObservationc(String observationc) {
        this.observationc = observationc;
    }

    public String getWorkStartDate() {
        return workStartDate;
    }

    public void setWorkStartDate(String workStartDate) {
        this.workStartDate = workStartDate;
    }

    public String getWorkStartTime() {
        return workStartTime;
    }

    public void setWorkStartTime(String workStartTime) {
        this.workStartTime = workStartTime;
    }

    public String getWorkEndDate() {
        return workEndDate;
    }

    public void setWorkEndDate(String workEndDate) {
        this.workEndDate = workEndDate;
    }

    public String getEndTimeWork() {
        return endTimeWork;
    }

    public void setEndTimeWork(String endTimeWork) {
        this.endTimeWork = endTimeWork;
    }

    public String getTotalTimeHours() {
        return totalTimeHours;
    }

    public void setTotalTimeHours(String totalTimeHours) {
        this.totalTimeHours = totalTimeHours;
    }

    public String getTechnicianValue() {
        return technicianValue;
    }

    public void setTechnicianValue(String technicianValue) {
        this.technicianValue = technicianValue;
    }

    public String getTechnicianObservation() {
        return technicianObservation;
    }

    public void setTechnicianObservation(String technicianObservation) {
        this.technicianObservation = technicianObservation;
    }

    public String getSupervisorValue() {
        return supervisorValue;
    }

    public void setSupervisorValue(String supervisorValue) {
        this.supervisorValue = supervisorValue;
    }

    public String getSupervisorObservation() {
        return supervisorObservation;
    }

    public void setSupervisorObservation(String supervisorObservation) {
        this.supervisorObservation = supervisorObservation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLoad() {
        return load;
    }

    public void setLoad(String load) {
        this.load = load;
    }


    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getObsi() {
        return obsi;
    }

    public void setObsi(String obsi) {
        this.obsi = obsi;
    }
}

