package com.pm.models;

public class ConfigurationClass {
    public int id;
    //Color of Order Work
    //Color of Status Order
    //Geolocation
    //Satisfaction Question
    //Satisfaction Nivel
    private String colorEONotStarting;
    private String geoDistance;
    private String sftcnMedium;
    private String stfcnQstnTecnic;
    private String colorOTEmergency;
    private String sftcnHigh;
    private String notification;
    private String stfcnQstnManager;
    private String colorOTRutine;
    private String colorEOInProcess;
    private String colorEOReject;
    private String obligatory;
    private String colorEOFinalized;
    private String sftcnLow;
    private String colorOTOther;
    private String automatic;

    public ConfigurationClass() {
    }

    public ConfigurationClass( String colorEONotStarting, String geoDistance, String sftcnMedium, String stfcnQstnTecnic, String colorOTEmergency, String sftcnHigh, String notification, String stfcnQstnManager, String colorOTRutine, String colorEOInProcess, String colorEOReject, String obligatory, String colorEOFinalized, String sftcnLow, String colorOTOther, String automatic,int id) {
        this.id = id;
        this.colorEONotStarting = colorEONotStarting;
        this.geoDistance = geoDistance;
        this.sftcnMedium = sftcnMedium;
        this.stfcnQstnTecnic = stfcnQstnTecnic;
        this.colorOTEmergency = colorOTEmergency;
        this.sftcnHigh = sftcnHigh;
        this.notification = notification;
        this.stfcnQstnManager = stfcnQstnManager;
        this.colorOTRutine = colorOTRutine;
        this.colorEOInProcess = colorEOInProcess;
        this.colorEOReject = colorEOReject;
        this.obligatory = obligatory;
        this.colorEOFinalized = colorEOFinalized;
        this.sftcnLow = sftcnLow;
        this.colorOTOther = colorOTOther;
        this.automatic = automatic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColorEONotStarting() {
        return colorEONotStarting;
    }

    public void setColorEONotStarting(String colorEONotStarting) {
        this.colorEONotStarting = colorEONotStarting;
    }

    public String getGeoDistance() {
        return geoDistance;
    }

    public void setGeoDistance(String geoDistance) {
        this.geoDistance = geoDistance;
    }

    public String getSftcnMedium() {
        return sftcnMedium;
    }

    public void setSftcnMedium(String sftcnMedium) {
        this.sftcnMedium = sftcnMedium;
    }

    public String getStfcnQstnTecnic() {
        return stfcnQstnTecnic;
    }

    public void setStfcnQstnTecnic(String stfcnQstnTecnic) {
        this.stfcnQstnTecnic = stfcnQstnTecnic;
    }

    public String getColorOTEmergency() {
        return colorOTEmergency;
    }

    public void setColorOTEmergency(String colorOTEmergency) {
        this.colorOTEmergency = colorOTEmergency;
    }

    public String getSftcnHigh() {
        return sftcnHigh;
    }

    public void setSftcnHigh(String sftcnHigh) {
        this.sftcnHigh = sftcnHigh;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getStfcnQstnManager() {
        return stfcnQstnManager;
    }

    public void setStfcnQstnManager(String stfcnQstnManager) {
        this.stfcnQstnManager = stfcnQstnManager;
    }

    public String getColorOTRutine() {
        return colorOTRutine;
    }

    public void setColorOTRutine(String colorOTRutine) {
        this.colorOTRutine = colorOTRutine;
    }

    public String getColorEOInProcess() {
        return colorEOInProcess;
    }

    public void setColorEOInProcess(String colorEOInProcess) {
        this.colorEOInProcess = colorEOInProcess;
    }

    public String getColorEOReject() {
        return colorEOReject;
    }

    public void setColorEOReject(String colorEOReject) {
        this.colorEOReject = colorEOReject;
    }

    public String getObligatory() {
        return obligatory;
    }

    public void setObligatory(String obligatory) {
        this.obligatory = obligatory;
    }

    public String getColorEOFinalized() {
        return colorEOFinalized;
    }

    public void setColorEOFinalized(String colorEOFinalized) {
        this.colorEOFinalized = colorEOFinalized;
    }

    public String getSftcnLow() {
        return sftcnLow;
    }

    public void setSftcnLow(String sftcnLow) {
        this.sftcnLow = sftcnLow;
    }

    public String getColorOTOther() {
        return colorOTOther;
    }

    public void setColorOTOther(String colorOTOther) {
        this.colorOTOther = colorOTOther;
    }

    public String getAutomatic() {
        return automatic;
    }

    public void setAutomatic(String automatic) {
        this.automatic = automatic;
    }
}
