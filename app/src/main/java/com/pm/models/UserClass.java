package com.pm.models;

public class UserClass {
	public int id;
	public String user;
	public String password;
	public String code; //No utilizo
	public String name;
	public String lastname;
	public String email;
	public String type; //GER (Gerente), SUP (Supervisor), TEC (Tecnico)
	public String lastConnection;
	public String center;
	public String active;

	public UserClass(int _id, String _user, String _password, String _code, String _name, String _lastname, String _email,
	 	String _type, String _dateConnection, String _center, String _active) {
		this.id = _id;
		this.user = _user;
		this.password = _password;
		this.code = _code;
		this.name = _name;
		this.lastname = _lastname;
		this.email = _email;
		this.type = _type;
		this.lastConnection = _dateConnection;
		this.center = _center;
		this.active = _active;
	}

	public UserClass() { }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getType() {
		return type;
	}

	public String getCenter() {
		return center;
	}

	public void setCenter(String center) {
		this.center = center;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLastConnection() {
		return lastConnection;
	}

	public void setLastConnection(String dateConnection) {
		this.lastConnection = dateConnection;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String _active) {
		this.active = _active;
	}
}
