package com.pm.models;

public class OTComponentsClass {
    public int id;
    public String numberOrder;
    public String numberOperation;
    public String position;
    public String objID;
    public String itemCode;
    public String itemDescription;
    public String umISO;
    public String unitMeasure;
    public String quantity;
    public String objUser;
    public String status;
    public String observation;
    public String amountUsed;

    public OTComponentsClass(){ }

    public OTComponentsClass(int id, String _numberOrder, String _numberOperation, String _position, String _objID, String _itemCode, String _itemDescription, String _umISO,
        String _unitMeasure, String _quantity, String _objUser, String _status, String _observation, String _amountUsed) {
        this.id = id;
        this.numberOrder = _numberOrder;
        this.numberOperation = _numberOperation;
        this.position = _position;
        this.objID = _objID;
        this.itemCode = _itemCode;
        this.itemDescription = _itemDescription;
        this.umISO = _umISO;
        this.unitMeasure = _unitMeasure;
        this.quantity = _quantity;
        this.objUser = _objUser;
        this.status = _status;
        this.observation = _observation;
        this.amountUsed = _amountUsed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumberOrder() {
        return numberOrder;
    }

    public void setNumberOrder(String numberOrder) {
        this.numberOrder = numberOrder;
    }

    public String getNumberOperation() {
        return numberOperation;
    }

    public void setNumberOperation(String numberOperation) {
        this.numberOperation = numberOperation;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getObjID() {
        return objID;
    }

    public void setObjID(String objID) {
        this.objID = objID;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getUmISO() {
        return umISO;
    }

    public void setUmISO(String umISO) {
        this.umISO = umISO;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getObjUser() {
        return objUser;
    }

    public void setObjUser(String objUser) {
        this.objUser = objUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getAmountUsed() {
        return amountUsed;
    }

    public void setAmountUsed(String amountUsed) {
        this.amountUsed = amountUsed;
    }
}
