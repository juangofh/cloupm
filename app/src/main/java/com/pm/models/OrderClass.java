package com.pm.models;

public class OrderClass {
    public int id;
    public String classCode;
    public String description;

    public OrderClass(){ }

    public OrderClass(int _id, String _classCode, String _description){
        this.id = _id;
        this.classCode = _classCode;
        this.description = _description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
