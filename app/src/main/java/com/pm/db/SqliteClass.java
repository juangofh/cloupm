package com.pm.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.File;
import java.util.ArrayList;

import com.pm.config.ConstValue;
import com.pm.models.*;

public class SqliteClass {
	public DatabaseHelper databasehelp;
	private static SqliteClass SqliteInstance = null;

	private SqliteClass(Context context) {
		databasehelp = new DatabaseHelper(context);
	}

	public static SqliteClass getInstance(Context context){
		if(SqliteInstance == null){
			SqliteInstance = new SqliteClass(context);
		}
		return SqliteInstance;
	}

	public class DatabaseHelper extends SQLiteOpenHelper {
		private static final int DATABASE_VERSION = 1;
		private static final String DATABASE_NAME = "app_pm_intur.db";

		/* @TABLE_APP_USER */
		public static final String TABLE_APP_USER = "user";
		private static final String KEY_USEID = "id";
		private static final String KEY_USEUSE = "username";
		private static final String KEY_USEPAS = "password";
		private static final String KEY_USECOD = "code";
		private static final String KEY_USENAM = "first_name";
		private static final String KEY_USELAS = "last_name";
		private static final String KEY_USEEMA = "email";
		private static final String KEY_USETYP = "type_user";
		private static final String KEY_USEDAT = "last_login";
		private static final String KEY_USECEN = "center";
		private static final String KEY_USEACT = "active";
		/* @TABLE_APP_CENTER*/
		private static final String TABLE_APP_CENTER = "app_center";
		private static final String KEY_CENID = "id";
		private static final String KEY_CENCENCOD = "WERKS";
		private static final String KEY_CENDESCEN = "NAME1";
		private static final String KEY_CENSOC = "BUKRS";
		private static final String KEY_CENLAT = "LAT";
		private static final String KEY_CENLON = "LON";
		/* @TABLE_APP_EQUIPMENT*/
		private static final String TABLE_APP_EQUIPMENT = "app_equipment";
		private static final String KEY_EQUID = "id";
		private static final String KEY_EQUCOD = "EQUNR";
		private static final String KEY_EQUOBJNUM = "OBJNR";
		private static final String KEY_EQUINDABC = "ABCKZ";
		private static final String KEY_EQUCATPRO = "RBNR";
		private static final String KEY_EQUTECLOCCOD = "TPLNR";
		private static final String KEY_EQUSITCEN = "SWERK";
		private static final String KEY_EQUSIT = "STORT";
		private static final String KEY_EQUDES = "EQKTX";
		/* @TABLE_APP_JOB_POSITION*/
		private static final String TABLE_APP_JOB_POSITION = "app_job_position";
		private static final String KEY_JPID = "id";
		private static final String KEY_JPOBJID = "OBJID";
		private static final String KEY_JPDES = "KTEXT";
		/* @TABLE_APP_MEASUREMENT_POINT*/
		private static final String TABLE_APP_MEASUREMENT_POINT = "app_measurement_point";
		private static final String KEY_MPID = "id";
		private static final String KEY_MPMEAPOICOD = "POINT";
		private static final String KEY_MPMEAPOIOBJ = "MPOBJ";
		private static final String KEY_MPINNFEA = "ATINN";
		private static final String KEY_MPMEAPOIDES = "PTTXT";
		/* @TABLE_AP_NOTICE_TYPE*/
		private static final String TABLE_AP_NOTICE_TYPE = "app_notice_type";
		private static final String KEY_NOTID ="id";
		private static final String KEY_NOTCOD="code";
		private static final String KEY_NOTDES="description";
		private static final String KEY_NOTSTA= "status";
		/* @TABLE_APP_ORDER_CLASS*/
		private static final String TABLE_APP_ORDER_CLASS = "app_order_class";
		private static final String KEY_ORDCLAID = "id";
		private static final String KEY_ORDCLACOD = "AUART";
		private static final String KEY_ORDCLADES = "TXT";
		/* @TABLE_APP_PLANNING_GROUP_CENTER*/
		private static final String TABLE_APP_PLANNING_GROUP_CENTER = "app_planning_group_center";
		private static final String KEY_PGPCID = "apgc_id";
		private static final String KEY_PGPCPLAGROCOD = "apgc_pla_gro_cod";
		private static final String KEY_PGPCPLACENCOD = "apgc_pla_cen_cod";
		/* @TABLE_APP_PLANNING_GROUP*/
		private static final String TABLE_APP_PLANNING_GROUP = "app_planning_group";
		private static final String KEY_PGID = "id";
		private static final String KEY_PGPLAGRO = "INGRP";
		private static final String KEY_PGDES = "INNAM";
		/* @TABLE_APP_PRIORITY*/
		private static final String TABLE_APP_PRIORITY = "app_priority";
		private static final String KEY_PRIOID = "id";
		private static final String KEY_PRIOPRICOD= "PRIOK";
		private static final String KEY_PRIODES = "PRIOKX";
		/* @TABLE_APP_SYMPTOM*/
		private static final String TABLE_APP_SYMPTOM = "app_symptom";
		private static final String KEY_SYMID = "id";
		private static final String KEY_SYMCATPRO = "RBNR";
		private static final String KEY_SYMSYMCOD = "CODE";
		private static final String KEY_SYMGROCOD = "CODEGRUPPE";
		private static final String KEY_SYMVER = "VERSION";
		private static final String KEY_SYMINA = "INAKTIV";
		private static final String KEY_SYMDES = "KURZTEXT";
		/* @TABLE_APP_TECHNICAL_LOCATIONS*/
		private static final String TABLE_APP_TECHNICAL_LOCATIONS = "app_technical_location";
		private static final String KEY_TLID = "id";
		private static final String KEY_TLTECLOCCOD = "TPLNR";
		private static final String KEY_TLDESLOC = "PLTXT";
		private static final String KEY_TLSITCEN = "SWERK";
		/* -- OPERATIONAL TABLE -- */
		/* @TABLE_APP_TICKET */
		private static final String TABLE_APP_TICKET = "app_ticket";
		private static final String KEY_TCKID = "id";
		private static final String KEY_TCKTICNUM = "TICNR";
		private static final String KEY_TCKNOTTYP = "QMART";
		private static final String KEY_TCKNNOTTYP = "NQMART";
		private static final String KEY_TCKCEN = "WERKS";
		private static final String KEY_TCKLOC = "TPLNR";

		/* @TABLE_APP_SUPPORT_TICKET */
		private static final String TABLE_APP_SUPPORT_TICKET = "app_suport_ticket";
		private static final String KEY_TICID = "id";
		private static final String KEY_TICTICNUM = "TICNR";
		private static final String KEY_TICNOTTYP = "QMART";
		private static final String KEY_TICNNOTTYP = "NQMART";
		private static final String KEY_TICCEN = "WERKS";
		private static final String KEY_TICLOC = "TPLNR";
		private static final String KEY_TICNLOC = "NTPLNR";
		private static final String KEY_TICTEA = "EQUNR";
		private static final String KEY_TICNTEA = "NEQUNR";
		private static final String KEY_TICNOTDAT = "NDATE";
		private static final String KEY_TICNOTHOU = "NTIME";
		private static final String KEY_TICSYMGROCOD = "CODEGRUPPE";
		private static final String KEY_TICSYMGROSYM = "CODE";
		private static final String KEY_TICNSYMGROSYM = "NCODE";
		private static final String KEY_TICCAUTEX = "REASON";
		private static final String KEY_TICSTOEQU = "STOPPED";
		private static final String KEY_TICAPP = "APPROVAL";
		private static final String KEY_TICLOA = "LOAD";
		/* @TABLE_APP_OT */
		public static final String TABLE_APP_OT = "app_ot";
		public static final String KEY_ORDID = "id";
		public static final String KEY_ORDNUM = "AUFNR"; //numero de orden
		public static final String KEY_ORDCLAS = "AUART";
		public static final String KEY_ORDCLASNAM = "NAUART";
		public static final String KEY_ORDPLAGRO = "INGRP";
		public static final String KEY_ORDPLAGRONAM = "NINGRP";
		public static final String KEY_ORDPLACEN = "IWERK";
		public static final String KEY_ORDPLACENNAM = "NIWERK";
		public static final String KEY_ORDJOBTIT = "VAPLZ";
		public static final String KEY_ORDJOBTITNAM = "NVAPLZ";
		public static final String KEY_ORDJOBCEN = "WAWRK";
		public static final String KEY_ORDJOBCENNAM = "NWAWRK";
		public static final String KEY_ORDOBJID = "OBJID";  //id del objeto
		public static final String KEY_ORDOBJID2 = "OBJID2";
		public static final String KEY_ORDSITCEN = "SOWRK";
		public static final String KEY_ORDSITCENNAM = "NSOWRK";
		public static final String KEY_ORDTEX = "KTEXT";
		public static final String KEY_ORDEXTSTADAT = "GSTRP";
		public static final String KEY_ORDEXTENDDAT = "GLTRP";
		public static final String KEY_ORDSTATIM = "BEGTIN";
		public static final String KEY_ORDENDTIM = "ENDTIN";
		public static final String KEY_ORDORDREF = "NOTNR";
		public static final String KEY_ORDPRIO = "PRIOK";
		public static final String KEY_ORDPRIONAM = "PRION";
		public static final String KEY_ORDTECLOC = "TPLNR";
		public static final String KEY_ORDTECLOCNAM = "NTPLNR";
		public static final String KEY_ORDEQU = "EQUNR";
		public static final String KEY_ORDEQUNAM = "NEQUNR";
		public static final String KEY_ORDOBJUSE = "USE";
		public static final String KEY_ORDTECCLO = "TECHAPP";
		public static final String KEY_ORDCLICLO = "CUSAPP";
		public static final String KEY_ORDOBS = "OBSH";
		public static final String KEY_ORDOBSC = "OBSHC";
		public static final String KEY_ORDWORSTADAT = "BEGDA";
		public static final String KEY_ORDWORSTATIM = "BEGTI";
		public static final String KEY_ORDWORENDDAT = "ENDDA";
		public static final String KEY_ORDENDTIMWOR = "ENDTI";
		public static final String KEY_ORDTOTTIMEHOU = "TOTALTHRS";

		public static final String KEY_ORDRESPSUP = "RESPSUP";
		public static final String KEY_ORDVALSUP = "VALSUP";
		public static final String KEY_ORDRESPTEC = "RESPTEC";
		public static final String KEY_ORDVALTEC = "VALTEC";

		public static final String KEY_ORDSTA = "STATUS";
		public static final String KEY_ORDLOA = "LOAD";
		public static final String KEY_ORDCRE = "CREATED";
		public static final String KEY_ORDOBSI = "OBSI";


		/* @TABLE_APP_OT_OPERATIONS*/
		private static final String TABLE_APP_OT_OPERATIONS = "app_ot_operations";
		private static final String KEY_OTOPEID = "id";
		private static final String KEY_OTOPEORDNUM = "AUFNR";
		private static final String KEY_OTOPEORDOPE = "VORNR";
		private static final String KEY_OTOPEJOB = "ARBPL";
		private static final String KEY_OTOPEJOBNAM = "ARBPLNAM";
		private static final String KEY_OTOPECEN = "WERKS";
		private static final String KEY_OTOPECENNAM = "WERKSNAM";
		private static final String KEY_OTOPEOBJID = "OBJID";
		private static final String KEY_OTOPECONKEY = "STEUS";
		private static final String KEY_OTOPESHOTEX = "LTXA1";
		private static final String KEY_OTOPESTADAT = "BEGDAOP";
		private static final String KEY_OTOPESTAHOU = "BEGTIOP";
		private static final String KEY_OTOPEENDDAT = "ENDDAOP";
		private static final String KEY_OTOPEENDHOU = "ENDTIOP";
		private static final String KEY_OTOPEUSE = "USE";
		private static final String KEY_OTOPESTA = "DONE";
		private static final String KEY_OTOPEOBS = "OPOBS";
		private static final String KEY_OTOPETIM = "OPTIME";
		/* @TABLE_APP_OT_COMPONENTS*/
		private static final String TABLE_APP_OT_COMPONENTS = "app_ot_components";
		private static final String KEY_OTCOMID = "id";
		private static final String KEY_OTCOMORDNUM = "AUFNR";
		private static final String KEY_OTCOMOPENUM = "VORNR";
		private static final String KEY_OTCOMPOS = "POSNR";
		private static final String KEY_OTCOMOBJID = "OBJID";
		private static final String KEY_OTCOMNUMMAT = "MATNR";
		private static final String KEY_OTCOMDESMAT = "MAKTX";
		private static final String KEY_OTCOMUMISO = "MEINSISO";
		private static final String KEY_OTCOMUNIMEA = "MEINS";
		private static final String KEY_OTCOMQUA = "MENGE";
		private static final String KEY_OTCOMUSE = "USE";
		private static final String KEY_OTCOMSTA = "DONE";
		private static final String KEY_OTCOMOBS = "CPOBS";
		private static final String KEY_OTCOMAMOUSE = "QUANTITY";
		/* @TABLE_APP_OTMEASUREMENT_POINTS*/
		private static final String TABLE_APP_OT_MEASUREMENT_POINTS = "app_ot_measurement_points";
		private static final String KEY_OTMID = "id";
		private static final String KEY_OTMORDNUM = "AUFNR";
		private static final String KEY_OTMPOINUM = "POINT";
		private static final String KEY_OTMTEACOD = "EQUNR";
		private static final String KEY_OTMTEANAM = "EQUNRNAM";
		private static final String KEY_OTMOBJID = "OBJID";
		private static final String KEY_OTMDAT = "VALUE_DATE";
		private static final String KEY_OTMHOU = "VALUE_TIME";
		private static final String KEY_OTMSTA = "TAKEN";
		private static final String KEY_OTMOBS = "PMOBS";
		private static final String KEY_OTMMEAVAL = "VALUE";
		/*@TABLE_APP_CONFIGURATION*/
		private static final String TABLE_APP_CONFIGURATION = "app_ot_configuration";
		private static final String KEY_CONCOLEOSTAR = "colorEONotStarting";
		private static final String KEY_CONGEODIS = "geoDistance";
		private static final String KEY_CONSATVALMED = "sftcnMedium";
		private static final String KEY_CONSATQUETEC = "stfcnQstnTecnic";
		private static final String KEY_CONCOLEMER = "colorOTEmergency";
		private static final String KEY_CONSATVALHIG = "sftcnHigh";
		private static final String KEY_CONNOT= "notification";
		private static final String KEY_CONSATQUESUP = "stfcnQstnManager";
		private static final String KEY_CONCOLOTRUT = "colorOTRutine";
		private static final String KEY_CONCOLEOPROC = "colorEOInProcess";
		private static final String KEY_CONCOLEOREJ = "colorEOReject";
		private static final String KEY_CONOBL = "obligatory";
		private static final String KEY_CONCOLOEFIN = "colorEOFinalized";
		private static final String KEY_CONSATVALLOW = "sftcnLow";
		private static final String KEY_CONCOLOTOTH = "colorOTOther";
		private static final String KEY_CONAUT = "automatic";
		private static final String KEY_CONID = "id";
		/*@TABLE_APP_NOTIFICATION*/
		private static final String TABLE_APP_NOTIFICATION = "app_ot_notification";
		private static final String KEY_NOTIFID = "id";
		private static final String KEY_NOTIFDAT = "DATE";
		private static final String KEY_NOTIFORDNUM = "AUFNR";
		private static final String KEY_NOTIFOBJID = "OBJID";
		private static final String KEY_NOTIFTIM = "TIME";

		/* @TABLE_APP_SYNCHRONIZATION */
		public static final String TABLE_APP_SYNCHRONIZATION = "app_synchronization";
		public static final String KEY_SYNID = "as_id";
		public static final String KEY_SYNCLANAM = "as_cla_nam";
		public static final String KEY_SYNDATHOUSTA = "as_dat_hou_sta";
		public static final String KEY_SYNDATHOUEND = "as_dat_hou_end";
		public static final String KEY_SYNREGNUM = "as_reg_num";
		public static final String KEY_SYNFAINUM = "as_fai_num";
		public static final String KEY_SYNSTA = "as_sta";

		/* @SQL */
		public AppUserSql usersql;
		public AppCenterSql centerSql;
		public AppEquipmentSql equipmentSql;
		public AppJobPositionSql jobPositionSql;
		public AppMeasurementPointSql measurementPointSql;
		public AppNoticeTypeSql noticeTypeSql;
		public AppOrderClassSql orderClassSql;
		public AppPlanningGroupCenterSql planningGroupCenterSql;
		public AppPlanningGroupSql planningGroupSql;
		public AppPrioritySql priorityClassSql;
		public AppSymptomSql symptomSql;
		public AppTechnicalLocationsSql technicalLocationsSql;
		public AppSupportTicketSql supportTicketSql;
		public AppOTSql otsql;
		public AppOTOperationsSql otOperationsSql;
		public AppOTMeasurementPointsSql otMeasurementPointsSql;
		public AppOTComponentsSql otComponentsSql;
		public AppConfigurationSql configurationSql;
		public AppNotificationSql notificationSql;
		public AppSynchronizationSql synchronizationsql;

		public Context context;

		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			usersql = new AppUserSql();
			centerSql = new AppCenterSql();
			equipmentSql = new AppEquipmentSql();
			jobPositionSql = new AppJobPositionSql();
			measurementPointSql = new AppMeasurementPointSql();
			noticeTypeSql = new AppNoticeTypeSql();
			orderClassSql = new AppOrderClassSql();
			planningGroupCenterSql = new AppPlanningGroupCenterSql();
			planningGroupSql = new AppPlanningGroupSql();
			priorityClassSql = new AppPrioritySql();
			symptomSql = new AppSymptomSql();
			technicalLocationsSql = new AppTechnicalLocationsSql();
			supportTicketSql = new AppSupportTicketSql();
			otsql = new AppOTSql();
			otOperationsSql = new AppOTOperationsSql();
			otComponentsSql = new AppOTComponentsSql();
			otMeasurementPointsSql = new AppOTMeasurementPointsSql();
			configurationSql =  new AppConfigurationSql();
			notificationSql = new AppNotificationSql();
			synchronizationsql = new AppSynchronizationSql();

			this.context = context;
		}
		public void onCreate(SQLiteDatabase db) {
			/* @TABLE_USER */
			String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_APP_USER + "(" + KEY_USEID + " INTEGER PRIMARY KEY," + KEY_USEUSE + " TEXT," + KEY_USEPAS + " TEXT,"
				+ KEY_USECOD + " TEXT," + KEY_USENAM + " TEXT," + KEY_USELAS + " TEXT," + KEY_USEEMA + " TEXT," + KEY_USETYP + " TEXT," + KEY_USEDAT + " TEXT," + KEY_USECEN + " TEXT," + KEY_USEACT + " TEXT)";
			/* @TABLE_CENTER */
			String CREATE_TABLE_CENTER = "CREATE TABLE " + TABLE_APP_CENTER + "(" + KEY_CENID + " INTEGER PRIMARY KEY," + KEY_CENCENCOD + " TEXT," + KEY_CENDESCEN + " TEXT,"
				+ KEY_CENSOC + " TEXT," + KEY_CENLAT + " TEXT," + KEY_CENLON + " TEXT)";
			/* @TABLE_EQUIPMENT */
			String CREATE_TABLE_EQUIPMENT = "CREATE TABLE " + TABLE_APP_EQUIPMENT + "(" + KEY_EQUID + " INTEGER PRIMARY KEY," + KEY_EQUCOD + " TEXT," + KEY_EQUOBJNUM + " TEXT,"
				+ KEY_EQUINDABC + " TEXT," + KEY_EQUCATPRO + " TEXT," + KEY_EQUTECLOCCOD + " TEXT," + KEY_EQUSITCEN + " TEXT," + KEY_EQUSIT + " TEXT," + KEY_EQUDES + " TEXT)";
			/* @TABLE_JOB_POSITION */
			String CREATE_TABLE_JOB_POSITION = "CREATE TABLE "+ TABLE_APP_JOB_POSITION + "(" + KEY_JPID + " INTEGER PRIMARY KEY," + KEY_JPOBJID + " TEXT," + KEY_JPDES + " TEXT)";
			/* @TABLE_MEASUREMENT_POINT */
			String CREATE_TABLE_MEASUREMENT_POINT = "CREATE TABLE "+ TABLE_APP_MEASUREMENT_POINT + "(" + KEY_MPID + " INTEGER PRIMARY KEY," + KEY_MPMEAPOICOD + " TEXT,"
				+ KEY_MPMEAPOIOBJ + " TEXT," + KEY_MPINNFEA + " TEXT," + KEY_MPMEAPOIDES + " TEXT)";
			/* @TABLE_NOTICE_TYPE */
			String CREATE_TABLE_TYPE_NOTICE = " CREATE TABLE " + TABLE_AP_NOTICE_TYPE + "(" + KEY_NOTID + " INTEGER PRIMARY KEY, " + KEY_NOTCOD + " TEXT, " + KEY_NOTDES + " TEXT, "
				+ KEY_NOTSTA + " TEXT) ";
			/* @TABLE_ORDER_CLASS */
			String CREATE_TABLE_ORDER_CLASS = "CREATE TABLE " + TABLE_APP_ORDER_CLASS + "(" + KEY_ORDCLAID + " INTEGER PRIMARY KEY," + KEY_ORDCLACOD + " TEXT," + KEY_ORDCLADES + " TEXT)";
			/* @TABLE_PLANNING_GROUP_CENTER */
			String CREATE_TABLE_PLANNING_GROUP_CENTER = "CREATE TABLE " + TABLE_APP_PLANNING_GROUP_CENTER + "(" + KEY_PGPCID + " INTEGER PRIMARY KEY," + KEY_PGPCPLAGROCOD + " TEXT," + KEY_PGPCPLACENCOD + " TEXT)";
			/* @TABLE_PLANNING_GROUP */
			String CREATE_TABLE_PLANNING_GROUP = "CREATE TABLE " + TABLE_APP_PLANNING_GROUP + "(" + KEY_PGID + " INTEGER PRIMARY KEY," + KEY_PGPLAGRO + " TEXT," + KEY_PGDES + " TEXT)";
			/* @TABLE_PRIORITY */
			String CREATE_TABLE_PRIORITY = "CREATE TABLE " + TABLE_APP_PRIORITY + "(" + KEY_PRIOID + " INTEGER PRIMARY KEY," + KEY_PRIOPRICOD + " TEXT," + KEY_PRIODES + " TEXT)";
			/* @TABLE_SYMPTOM */
			String CREATE_TABLE_SYMPTOM = "CREATE TABLE " + TABLE_APP_SYMPTOM + "(" + KEY_SYMID + " INTEGER PRIMARY KEY," + KEY_SYMCATPRO + " TEXT," + KEY_SYMSYMCOD + " TEXT," + KEY_SYMGROCOD + " TEXT,"
				+ KEY_SYMVER + " TEXT," + KEY_SYMINA + " TEXT," + KEY_SYMDES + " TEXT)";
			/* @TABLE_TECHNICAL_LOCATIONS */
			String CREATE_TABLE_TECHNICAL_LOCATIONS = "CREATE TABLE " + TABLE_APP_TECHNICAL_LOCATIONS + "(" + KEY_TLID + " INTEGER PRIMARY KEY," + KEY_TLTECLOCCOD + " TEXT," + KEY_TLDESLOC + " TEXT,"
				+ KEY_TLSITCEN + " TEXT)";
			/* @TABLE_SUPPORT_TICKET */
			String CREATE_TABLE_SUPPORT_TICKET = " CREATE TABLE " + TABLE_APP_SUPPORT_TICKET + "(" + KEY_TICID + " INTEGER PRIMARY KEY, " + KEY_TICTICNUM + " TEXT, " + KEY_TICNOTTYP + " TEXT, " + KEY_TICNNOTTYP + " TEXT, "
				+ KEY_TICCEN + " TEXT, " + KEY_TICLOC + " TEXT, " + KEY_TICNLOC + " TEXT, "  + KEY_TICTEA + " TEXT, " + KEY_TICNTEA + " TEXT, " + KEY_TICNOTDAT + " TEXT, " + KEY_TICNOTHOU + " TEXT, " + KEY_TICSYMGROCOD + " TEXT, " + KEY_TICSYMGROSYM + " TEXT, " + KEY_TICNSYMGROSYM + " TEXT, "
				+ KEY_TICCAUTEX + " TEXT, " + KEY_TICSTOEQU + " TEXT," + KEY_TICAPP + " TEXT," + KEY_TICLOA + " TEXT) ";
			/* @TABLE_OT */
			String CREATE_TABLE_OT = "CREATE TABLE " + TABLE_APP_OT + "(" + KEY_ORDID + " INTEGER, " + KEY_ORDNUM + " TEXT," + KEY_ORDCLAS + " TEXT," + KEY_ORDCLASNAM + " TEXT," + KEY_ORDPLAGRO + " TEXT,"
					+ KEY_ORDPLAGRONAM + " TEXT," + KEY_ORDPLACEN + " TEXT," + KEY_ORDPLACENNAM + " TEXT," + KEY_ORDJOBTIT + " TEXT," + KEY_ORDJOBTITNAM + " TEXT," + KEY_ORDJOBCEN + " TEXT," + KEY_ORDJOBCENNAM + " TEXT,"
					+ KEY_ORDOBJID + " TEXT," + KEY_ORDOBJID2 + " TEXT," + KEY_ORDSITCEN + " TEXT," + KEY_ORDSITCENNAM + " TEXT," + KEY_ORDTEX + " TEXT," + KEY_ORDEXTSTADAT + " TEXT," + KEY_ORDEXTENDDAT + " TEXT," + KEY_ORDSTATIM + " TEXT,"
					+ KEY_ORDENDTIM + " TEXT," + KEY_ORDORDREF + " TEXT," + KEY_ORDPRIO + " TEXT," + KEY_ORDPRIONAM + " TEXT," + KEY_ORDTECLOC + " TEXT," + KEY_ORDTECLOCNAM + " TEXT," + KEY_ORDEQU + " TEXT," + KEY_ORDEQUNAM + " TEXT,"
					+ KEY_ORDOBJUSE + " TEXT," + KEY_ORDTECCLO + " TEXT," + KEY_ORDCLICLO + " TEXT," + KEY_ORDOBS + " TEXT," + KEY_ORDOBSC + " TEXT," + KEY_ORDWORSTADAT + " TEXT," + KEY_ORDWORSTATIM + " TEXT," + KEY_ORDWORENDDAT + " TEXT,"
					+ KEY_ORDENDTIMWOR + " TEXT," + KEY_ORDTOTTIMEHOU + " TEXT," + KEY_ORDRESPTEC + " TEXT, " + KEY_ORDVALTEC + " TECT, " + KEY_ORDRESPSUP + " TEXT, " + KEY_ORDVALSUP + " TEXT, "
					+ KEY_ORDSTA + " TEXT," + KEY_ORDLOA + " TEXT," + KEY_ORDCRE + " TEXT," + KEY_ORDOBSI+ " TEXT)";
			/* @TABLE_OT_OPERATIONS*/
			String CREATE_TABLE_OT_OPERATIONS = "CREATE TABLE " + TABLE_APP_OT_OPERATIONS + "(" + KEY_OTOPEID + " INTEGER," + KEY_OTOPEORDNUM + " TEXT," + KEY_OTOPEORDOPE + " TEXT,"
					+ KEY_OTOPEJOB + " TEXT," + KEY_OTOPEJOBNAM + " TEXT," + KEY_OTOPECEN + " TEXT," + KEY_OTOPECENNAM + " TEXT," + KEY_OTOPEOBJID + " TEXT," + KEY_OTOPECONKEY + " TEXT,"
					+ KEY_OTOPESHOTEX + " TEXT," + KEY_OTOPESTADAT + " TEXT," + KEY_OTOPESTAHOU + " TEXT," + KEY_OTOPEENDDAT + " TEXT," + KEY_OTOPEENDHOU + " TEXT," + KEY_OTOPEUSE + " TEXT,"
					+ KEY_OTOPESTA + " TEXT," + KEY_OTOPEOBS + " TEXT," + KEY_OTOPETIM + " TEXT)";
			/* @TABLE_OT_COMPONENTS*/
			String CREATE_TABLE_OT_COMPONENTS = "CREATE TABLE " + TABLE_APP_OT_COMPONENTS + "(" + KEY_OTCOMID + " INTEGER," + KEY_OTCOMORDNUM + " TEXT," + KEY_OTCOMOPENUM + " TEXT,"
				+ KEY_OTCOMPOS + " TEXT," + KEY_OTCOMOBJID + " TEXT," + KEY_OTCOMNUMMAT + " TEXT," + KEY_OTCOMDESMAT + " TEXT," + KEY_OTCOMUMISO + " TEXT," + KEY_OTCOMUNIMEA + " TEXT,"
				+ KEY_OTCOMQUA + " TEXT," + KEY_OTCOMUSE + " TEXT," + KEY_OTCOMSTA + " TEXT," + KEY_OTCOMOBS + " TEXT,"  + KEY_OTCOMAMOUSE + " TEXT)";
			/* @TABLE_OT_MEASUREMENT_POINTS*/
			String CREATE_TABLE_OT_MEASUREMENT_POINTS = "CREATE TABLE " + TABLE_APP_OT_MEASUREMENT_POINTS + "(" + KEY_OTMID + " INTEGER," + KEY_OTMORDNUM + " TEXT," + KEY_OTMPOINUM + " TEXT,"
				+ KEY_OTMTEACOD + " TEXT," + KEY_OTMTEANAM + " TEXT," + KEY_OTMOBJID + " TEXT," + KEY_OTMDAT + " TEXT," + KEY_OTMHOU + " TEXT," + KEY_OTMSTA + " TEXT," + KEY_OTMOBS + " TEXT," + KEY_OTMMEAVAL + " TEXT)";
			/*@TABLE_CONFIGURATION*/
			String CREATE_TABLE_CONFIGURATION = "CREATE TABLE " + TABLE_APP_CONFIGURATION + "(" + KEY_CONCOLEOSTAR + " TEXT, " + KEY_CONGEODIS + " TEXT, "
					+ KEY_CONSATVALMED + " TEXT, " + KEY_CONSATQUETEC + " TEXT, " + KEY_CONCOLEMER + " TEXT, " + KEY_CONSATVALHIG + " TEXT, "
					+ KEY_CONNOT + " TEXT, " + KEY_CONSATQUESUP + " TEXT, " + KEY_CONCOLOTRUT + " TEXT, " + KEY_CONCOLEOPROC + " TEXT, "
					+ KEY_CONCOLEOREJ + " TEXT, " + KEY_CONOBL + " TEXT, " + KEY_CONCOLOEFIN + " TEXT, " + KEY_CONSATVALLOW + " TEXT, "
					+ KEY_CONCOLOTOTH + " TEXT, " + KEY_CONAUT + " TEXT, " + KEY_CONID + " TEXT) ";
			/* @TABLE_NOTIFICATION*/
			String CREATE_TABLE_NOTIFICATION = "CREATE TABLE " + TABLE_APP_NOTIFICATION + "("
					+ KEY_NOTIFID + " TEXT, " + KEY_NOTIFDAT + " TEXT , " + KEY_NOTIFORDNUM + " TEXT, " + KEY_NOTIFOBJID + " TEXT, " + KEY_NOTIFTIM + " TEXT) ";

			/* @TABLE_SYNCHRONIZATION */
			String CREATE_TABLE_SYNCHRONIZATION = "CREATE TABLE " + TABLE_APP_SYNCHRONIZATION + "(" + KEY_SYNID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ KEY_SYNCLANAM + " TEXT," + KEY_SYNDATHOUSTA + " TEXT,"  + KEY_SYNDATHOUEND + " TEXT," + KEY_SYNREGNUM + " TEXT," + KEY_SYNFAINUM
					+ " TEXT," + KEY_SYNSTA + " TEXT )";

			/* @EXECSQL_CREATE */
			db.execSQL(CREATE_TABLE_USER);
			db.execSQL(CREATE_TABLE_CENTER);
			db.execSQL(CREATE_TABLE_EQUIPMENT);
			db.execSQL(CREATE_TABLE_JOB_POSITION);
			db.execSQL(CREATE_TABLE_MEASUREMENT_POINT);
			db.execSQL(CREATE_TABLE_TYPE_NOTICE);
			db.execSQL(CREATE_TABLE_ORDER_CLASS);
			db.execSQL(CREATE_TABLE_PLANNING_GROUP_CENTER);
			db.execSQL(CREATE_TABLE_PLANNING_GROUP);
			db.execSQL(CREATE_TABLE_PRIORITY);
			db.execSQL(CREATE_TABLE_SYMPTOM);
			db.execSQL(CREATE_TABLE_TECHNICAL_LOCATIONS);
			db.execSQL(CREATE_TABLE_SUPPORT_TICKET);
			db.execSQL(CREATE_TABLE_OT);
			db.execSQL(CREATE_TABLE_OT_OPERATIONS);
			db.execSQL(CREATE_TABLE_OT_COMPONENTS);
			db.execSQL(CREATE_TABLE_OT_MEASUREMENT_POINTS);
			db.execSQL(CREATE_TABLE_CONFIGURATION);
			db.execSQL(CREATE_TABLE_NOTIFICATION);
			db.execSQL(CREATE_TABLE_SYNCHRONIZATION);

		}
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			/* @EXECSQL_DROP */
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_USER);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_CENTER);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_EQUIPMENT);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_AP_NOTICE_TYPE);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_SYMPTOM);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_TECHNICAL_LOCATIONS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_OT);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_OT_OPERATIONS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_OT_COMPONENTS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_OT_MEASUREMENT_POINTS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_SUPPORT_TICKET);

			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_PLANNING_GROUP);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_PRIORITY);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_JOB_POSITION);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_MEASUREMENT_POINT);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_ORDER_CLASS);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_PLANNING_GROUP_CENTER);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_CONFIGURATION);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_NOTIFICATION);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_SYNCHRONIZATION);

			onCreate(db);
		}
		public boolean checkDataBase(){
			File dbFile = new File(context.getDatabasePath(DATABASE_NAME).toString());
			return dbFile.exists();
		}
		public void deleteDataBase(){
			context.deleteDatabase(DATABASE_NAME);
		}

		/* @CLASS_USERSQL */
		public class AppUserSql {
			public AppUserSql() {	}
			public void deleteUser(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_USER,null,null);
				db.close();
			}
			public void addUser(UserClass user) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_USEID, user.getId());
				values.put(KEY_USEUSE, user.getUser());
				values.put(KEY_USEPAS, user.getPassword());
				values.put(KEY_USECOD, user.getCode());
				values.put(KEY_USENAM, user.getName());
				values.put(KEY_USELAS, user.getLastname());
				values.put(KEY_USEEMA, user.getEmail());
				values.put(KEY_USETYP, user.getType());
				values.put(KEY_USEDAT, user.getLastConnection());
				values.put(KEY_USECEN, user.getCenter());
				values.put(KEY_USEACT, user.getActive());
				db.insert(TABLE_APP_USER, null, values);
				db.close();
			}
			public boolean isRegisterUser(String sName, String sPassword) {
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_USER, new String[] { KEY_USEUSE, KEY_USEPAS, KEY_USECOD, KEY_USENAM, KEY_USELAS, KEY_USEEMA, KEY_USETYP,
								KEY_USEDAT, KEY_USECEN, KEY_USEACT}, KEY_USEUSE + "=?" + " and "+ KEY_USEPAS + "=?",
						new String[] {sName, sPassword}, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				if(cursor.getCount() == 1){
					db.close();
					return true;
				}else{
					db.close();
					return false;
				}
			}
			public ArrayList<UserClass> getUserData() {
				ArrayList<UserClass> userList = new ArrayList<UserClass>();
				String selectQuery = "SELECT * FROM " + TABLE_APP_USER + " LIMIT 1";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						UserClass user = new UserClass();
						user.setId(cursor.getInt(cursor.getColumnIndex(KEY_USEID)));
						user.setUser(cursor.getString(cursor.getColumnIndex(KEY_USEUSE)));
						user.setPassword(cursor.getString(cursor.getColumnIndex(KEY_USEPAS)));
						user.setCode(cursor.getString(cursor.getColumnIndex(KEY_USECOD)));
						user.setName(cursor.getString(cursor.getColumnIndex(KEY_USENAM)));
						user.setLastname(cursor.getString(cursor.getColumnIndex(KEY_USELAS)));
						user.setEmail(cursor.getString(cursor.getColumnIndex(KEY_USEEMA)));
						user.setType(cursor.getString(cursor.getColumnIndex(KEY_USETYP)));
						user.setLastConnection(cursor.getString(cursor.getColumnIndex(KEY_USEDAT)));
						user.setCenter(cursor.getString(cursor.getColumnIndex(KEY_USECEN)));
						user.setActive(cursor.getString(cursor.getColumnIndex(KEY_USEACT)));
						userList.add(user);
					} while (cursor.moveToNext());
				}
				db.close();
				return userList;
			}
			public int getId(String sName, String sPassword) {
				int _id=0;
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_USER, new String[] { KEY_USEID, KEY_USEUSE,
								KEY_USEPAS, KEY_USECOD, KEY_USENAM, KEY_USELAS, KEY_USEEMA, KEY_USETYP, KEY_USEDAT, KEY_USECEN, KEY_USEACT}, KEY_USEUSE + "=?" + " and "+ KEY_USEPAS + "=?",
						new String[] {sName, sPassword}, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				_id = cursor.getInt(0);
				db.close();
				return _id;
			}
			public String getData(int nField, String sName) {
				String _data = null;
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_USER, new String[] { KEY_USEID, KEY_USEUSE,
								KEY_USEPAS, KEY_USECOD, KEY_USENAM, KEY_USELAS, KEY_USEEMA, KEY_USETYP, KEY_USEDAT, KEY_USECEN, KEY_USEACT}, KEY_USEUSE + "=?",
						new String[] {sName}, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				_data = cursor.getString(nField);
				db.close();
				return _data;
			}
			public String getUser(){
				String result = "";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT username FROM " + TABLE_APP_USER + " LIMIT 1",null);
				mCount.moveToFirst();
				result= mCount.getString(0);
				db.close();
				return result;
			}
			public String getActive(){
				String result = "";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT active FROM " + TABLE_APP_USER + " LIMIT 1",null);
				mCount.moveToFirst();
				result= mCount.getString(0);
				db.close();
				return result;
			}
			public void update(String value) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put("active", value);
				db.update(TABLE_APP_USER, values,"",new String[]{});
				db.close();
			}
			public  void updateUserPass(String _pass){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_USEPAS,_pass);
				db.update(TABLE_APP_USER, values,"",new String[]{});
				db.close();
			}
		}
		/* @CLASS_CENTER */
		public class AppCenterSql{
			public AppCenterSql(){ }
			public void deleteCenter(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_CENTER,null,null);
				db.close();
			}
			public void addCenter(CenterClass centerClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_CENID , centerClass.getId());
				values.put(KEY_CENCENCOD , centerClass.getCenterCode());
				values.put(KEY_CENDESCEN , centerClass.getCenterName());
				values.put(KEY_CENSOC , centerClass.getSociety());
				values.put(KEY_CENLAT , centerClass.getLatitude());
				values.put(KEY_CENLON , centerClass.getLongitude());
				db.insert(TABLE_APP_CENTER, null, values);
				db.close();
			}
			public ArrayList<CenterClass> getAllItem() {
				ArrayList<CenterClass> centerClassList = new ArrayList<CenterClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_CENTER ;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						CenterClass item = new CenterClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_CENID))));
						item.setCenterCode(cursor.getString(cursor.getColumnIndex(KEY_CENCENCOD)));
						item.setCenterName(cursor.getString(cursor.getColumnIndex(KEY_CENDESCEN)));
						item.setSociety(cursor.getString(cursor.getColumnIndex(KEY_CENSOC)));
						item.setSociety(cursor.getString(cursor.getColumnIndex(KEY_CENLAT)));
						item.setSociety(cursor.getString(cursor.getColumnIndex(KEY_CENLON)));
						centerClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return centerClassList;
			}
			public CenterClass getCenter(int nId) {
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_CENTER , new String[] { KEY_CENID , KEY_CENCENCOD ,
								KEY_CENDESCEN, KEY_CENSOC, KEY_CENLAT, KEY_CENLON }, KEY_CENID + "=?",
						new String[] { String.valueOf(nId) }, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				CenterClass centerClass = new CenterClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
						cursor.getString(3), cursor.getString(4), cursor.getString(5));
				db.close();
				return centerClass;
			}
		}
		/* @CLASS_EQUIPMENT */
		public class AppEquipmentSql{
			public AppEquipmentSql(){ }
			public void deleteEquipment(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_EQUIPMENT ,null,null);
				db.close();
			}

			public boolean checkIfExists(String id){
				int result = 0;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_EQUIPMENT + " WHERE EQUNR='" + id + "'",null);
				mCount.moveToFirst();
				result= mCount.getInt(0);
				db.close();

				return result > 0;
			}

			public void addEquipment(EquipmentClass equipmentClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_EQUID, equipmentClass.getId());
				values.put(KEY_EQUCOD, equipmentClass.getCode());
				values.put(KEY_EQUOBJNUM, equipmentClass.getObjectNumber());
				values.put(KEY_EQUINDABC, equipmentClass.getIndicatorABC());
				values.put(KEY_EQUCATPRO, equipmentClass.getCatalogProfile());
				values.put(KEY_EQUTECLOCCOD, equipmentClass.getTechnicalLocationCode());
				values.put(KEY_EQUSITCEN, equipmentClass.getSiteCenter());
				values.put(KEY_EQUSIT, equipmentClass.getSite());
				values.put(KEY_EQUDES, equipmentClass.getDescription());
				db.insert(TABLE_APP_EQUIPMENT, null, values);
				db.close();
			}

			public void updateEquipment(String id, EquipmentClass equipmentClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				//No id
				values.put(KEY_EQUCOD, equipmentClass.getCode());
				values.put(KEY_EQUOBJNUM, equipmentClass.getObjectNumber());
				values.put(KEY_EQUINDABC, equipmentClass.getIndicatorABC());
				values.put(KEY_EQUCATPRO, equipmentClass.getCatalogProfile());
				values.put(KEY_EQUTECLOCCOD, equipmentClass.getTechnicalLocationCode());
				values.put(KEY_EQUSITCEN, equipmentClass.getSiteCenter());
				values.put(KEY_EQUSIT, equipmentClass.getSite());
				values.put(KEY_EQUDES, equipmentClass.getDescription());
				db.update(TABLE_APP_EQUIPMENT, values, KEY_EQUCOD + " = ?",new String[] { id});
				db.close();
			}

			public ArrayList<EquipmentClass> getAllItem() {
				ArrayList<EquipmentClass> equipmentClassList = new ArrayList<EquipmentClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_EQUIPMENT;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						EquipmentClass item = new EquipmentClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_EQUID))));
						item.setCode(cursor.getString(cursor.getColumnIndex(KEY_EQUCOD)));
						item.setObjectNumber(cursor.getString(cursor.getColumnIndex(KEY_EQUOBJNUM)));
						item.setIndicatorABC(cursor.getString(cursor.getColumnIndex(KEY_EQUINDABC)));
						item.setCatalogProfile(cursor.getString(cursor.getColumnIndex(KEY_EQUCATPRO)));
						item.setTechnicalLocationCode(cursor.getString(cursor.getColumnIndex(KEY_EQUTECLOCCOD)));
						item.setSiteCenter(cursor.getString(cursor.getColumnIndex(KEY_EQUSITCEN)));
						item.setSite(cursor.getString(cursor.getColumnIndex(KEY_EQUSIT)));
						item.setDescription(cursor.getString(cursor.getColumnIndex(KEY_EQUDES)));
						equipmentClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return equipmentClassList;
			}
			public ArrayList<EquipmentClass> getEquipmentLocation(String location) {
				ArrayList<EquipmentClass> equipmentClassList = new ArrayList<EquipmentClass>();
				String selectQuery = "SELECT * FROM " + TABLE_APP_EQUIPMENT + " WHERE TPLNR ='"+location+"' GROUP BY "+KEY_EQUDES;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						EquipmentClass item = new EquipmentClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_EQUID))));
						item.setCode(cursor.getString(cursor.getColumnIndex(KEY_EQUCOD)));
						item.setObjectNumber(cursor.getString(cursor.getColumnIndex(KEY_EQUOBJNUM)));
						item.setIndicatorABC(cursor.getString(cursor.getColumnIndex(KEY_EQUINDABC)));
						item.setCatalogProfile(cursor.getString(cursor.getColumnIndex(KEY_EQUCATPRO)));
						item.setTechnicalLocationCode(cursor.getString(cursor.getColumnIndex(KEY_EQUTECLOCCOD)));
						item.setSiteCenter(cursor.getString(cursor.getColumnIndex(KEY_EQUSITCEN)));
						item.setSite(cursor.getString(cursor.getColumnIndex(KEY_EQUSIT)));
						item.setDescription(cursor.getString(cursor.getColumnIndex(KEY_EQUDES)));
						equipmentClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return equipmentClassList;
			}
		}
		/* @CLASS_JOB_POSITION */
		public class AppJobPositionSql {
			public AppJobPositionSql(){ }
			public void deleteJobPosition(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_JOB_POSITION,null,null);
				db.close();
			}
			public void addJobPosition(JobPositionClass jobPositionClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_JPID, jobPositionClass.getId());
				values.put(KEY_JPOBJID, jobPositionClass.getObjectId());
				values.put(KEY_JPDES, jobPositionClass.getDescription());
				db.insert(TABLE_APP_JOB_POSITION, null, values);
				db.close();
			}
			public String getJobPosition(String objid) {
				String result="";
				String selectQuery = "SELECT KTEXT FROM " + TABLE_APP_JOB_POSITION + " WHERE OBJID='"+objid+"' LIMIT 1";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						result = cursor.getString(cursor.getColumnIndex(KEY_JPDES));
					} while (cursor.moveToNext());
				}
				db.close();
				return result;
			}
		}
		/* @CLASS_MEASUREMENT_POINT */
		public class AppMeasurementPointSql {
			public AppMeasurementPointSql(){ }
			public void deleteMeasurementPoint(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_MEASUREMENT_POINT,null,null);
				db.close();
			}
			public void addMeasurementPoint(MeasurementPointClass measurementPointClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_MPID, measurementPointClass.getId());
				values.put(KEY_MPMEAPOICOD, measurementPointClass.getMeasurementPointCode());
				values.put(KEY_MPMEAPOIOBJ, measurementPointClass.getMeasurementPointObject());
				values.put(KEY_MPINNFEA, measurementPointClass.getInnerFeature());
				values.put(KEY_MPMEAPOIDES, measurementPointClass.getMeasurementPointDescription());
				db.insert(TABLE_APP_MEASUREMENT_POINT, null, values);
				db.close();
			}
			public ArrayList<MeasurementPointClass> getAllItem() {
				ArrayList<MeasurementPointClass> measurementPointList = new ArrayList<MeasurementPointClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_MEASUREMENT_POINT;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						MeasurementPointClass item = new MeasurementPointClass();
						item.setId(cursor.getInt(cursor.getColumnIndex(KEY_MPID)));
						item.setMeasurementPointCode(cursor.getString(cursor.getColumnIndex(KEY_MPMEAPOICOD)));
						item.setMeasurementPointObject(cursor.getString(cursor.getColumnIndex(KEY_MPMEAPOIOBJ)));
						item.setInnerFeature(cursor.getString(cursor.getColumnIndex(KEY_MPINNFEA)));
						item.setMeasurementPointDescription(cursor.getString(cursor.getColumnIndex(KEY_MPMEAPOIDES)));
						measurementPointList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return measurementPointList;
			}
			public MeasurementPointClass getMeasurementPoint(int nId) {
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_MEASUREMENT_POINT, new String[] {
								KEY_MPID, KEY_MPMEAPOICOD, KEY_MPMEAPOIOBJ,
								KEY_MPINNFEA, KEY_MPMEAPOIDES}, KEY_MPID + "=?",
						new String[] { String.valueOf(nId) }, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				MeasurementPointClass measurementPointClass = new MeasurementPointClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
				db.close();
				return measurementPointClass;
			}
		}
		/* @CLASS_NOTICE_TYPE */
		public class AppNoticeTypeSql{
			public AppNoticeTypeSql(){ }
			public void deleteNoticeType(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_AP_NOTICE_TYPE,null,null);
				db.close();
			}

			public boolean checkIfExists(String id){

				int result = 0;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_AP_NOTICE_TYPE + " WHERE CODE='" + id + "'",null);
				mCount.moveToFirst();
				result= mCount.getInt(0);
				db.close();

				return result > 0;
			}

			public void updateNoticeType(String id, NoticeTypeClass noticeTypeClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				//No id
				values.put(KEY_NOTCOD , noticeTypeClass.getCode());
				values.put(KEY_NOTDES , noticeTypeClass.getDescription());
				values.put(KEY_NOTSTA  , noticeTypeClass.getStatus());
				db.update(TABLE_AP_NOTICE_TYPE, values, KEY_NOTCOD + " = ?",new String[] { id});
				db.close();
			}

			public void addNoticeType(NoticeTypeClass noticeTypeClass){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_NOTID, noticeTypeClass.getId());
				values.put(KEY_NOTCOD , noticeTypeClass.getCode());
				values.put(KEY_NOTDES , noticeTypeClass.getDescription());
				values.put(KEY_NOTSTA  , noticeTypeClass.getStatus());
				db.insert(TABLE_AP_NOTICE_TYPE, null, values);
				db.close();
			}
			public ArrayList<NoticeTypeClass> getAllItem(){
				ArrayList<NoticeTypeClass> noticeTypeClass = new ArrayList<NoticeTypeClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_AP_NOTICE_TYPE;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						NoticeTypeClass item = new NoticeTypeClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_NOTID))));
						item.setCode(cursor.getString(cursor.getColumnIndex(KEY_NOTCOD)));
						item.setDescription(cursor.getString(cursor.getColumnIndex(KEY_NOTDES)));
						item.setStatus(cursor.getString(cursor.getColumnIndex(KEY_NOTSTA)));
						noticeTypeClass.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return noticeTypeClass;
			}
		}
		/* @CLASS_ORDER_CLASS */
		public class AppOrderClassSql{
			public AppOrderClassSql(){ }
			public void deleteOrderClass(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_ORDER_CLASS,null,null);
				db.close();
			}
			public void addOrderClass(OrderClass orderClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_ORDCLAID, orderClass.getId());
				values.put(KEY_ORDCLACOD, orderClass.getClassCode());
				values.put(KEY_ORDCLADES, orderClass.getDescription());
				db.insert(TABLE_APP_ORDER_CLASS, null, values);
				db.close();
			}
			public ArrayList<OrderClass> getAllItem() {
				ArrayList<OrderClass> orderClassList = new ArrayList<OrderClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_ORDER_CLASS;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						OrderClass item = new OrderClass();
						item.setId(cursor.getInt(cursor.getColumnIndex(KEY_ORDCLAID)));
						item.setClassCode(cursor.getString(cursor.getColumnIndex(KEY_ORDCLACOD)));
						item.setDescription(cursor.getString(cursor.getColumnIndex(KEY_ORDCLADES)));
						orderClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return orderClassList;
			}
			public OrderClass getOrderClass(int nId) {
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_ORDER_CLASS, new String[] {
								KEY_ORDCLAID, KEY_ORDCLACOD, KEY_ORDCLADES}, KEY_ORDCLAID + "=?",
						new String[] { String.valueOf(nId) }, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				OrderClass orderClass = new OrderClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
				db.close();
				return orderClass;
			}
		}
		/* @CLASS_PLANNING_GROUP_CENTER */
		public class AppPlanningGroupCenterSql {
			public AppPlanningGroupCenterSql() {}
			public void deletePlanningGroupCenter(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_PLANNING_GROUP_CENTER,null,null);
				db.close();
			}
			public void addPlanningGroupCenter(PlanningGroupCenterClass planningGroupCenter) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_PGPCID, planningGroupCenter.getId());
				values.put(KEY_PGPCPLAGROCOD, planningGroupCenter.getPlanningGroupCode());
				values.put(KEY_PGPCPLACENCOD, planningGroupCenter.getPlanningCenterCode());
				db.insert(TABLE_APP_PLANNING_GROUP_CENTER, null, values);
				db.close();
			}
			public ArrayList<PlanningGroupCenterClass> getAllItem() {
				ArrayList<PlanningGroupCenterClass> planningGroupCenterList = new ArrayList<PlanningGroupCenterClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_PLANNING_GROUP_CENTER;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						PlanningGroupCenterClass item = new PlanningGroupCenterClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_PGPCID))));
						item.setPlanningGroupCode(cursor.getString(cursor.getColumnIndex(KEY_PGPCPLAGROCOD)));
						item.setPlanningCenterCode(cursor.getString(cursor.getColumnIndex(KEY_PGPCPLACENCOD)));
						planningGroupCenterList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return planningGroupCenterList;
			}
			public PlanningGroupCenterClass getPlanningGroupCenter(int nId) {
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_PLANNING_GROUP_CENTER, new String[] { KEY_PGPCID, KEY_PGPCPLAGROCOD,
								KEY_PGPCPLACENCOD}, KEY_PGPCID + "=?",
						new String[] { String.valueOf(nId) }, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				PlanningGroupCenterClass planningGroupCenter = new PlanningGroupCenterClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
				db.close();
				return planningGroupCenter;
			}
		}
		/* @CLASS_PLANNING_GROUPSQL */
		public class AppPlanningGroupSql {
			public AppPlanningGroupSql() {}
			public void deletePlanningGroup(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_PLANNING_GROUP,null,null);
				db.close();
			}
			public void addPlanningGroup(PlanningGroupClass planningGroup) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_PGID, planningGroup.getId());
				values.put(KEY_PGPLAGRO, planningGroup.getPlanningGroup());
				values.put(KEY_PGDES, planningGroup.getDescription());
				db.insert(TABLE_APP_PLANNING_GROUP, null, values);
				db.close();
			}
			public ArrayList<PlanningGroupClass> getAllItem() {
				ArrayList<PlanningGroupClass> planningGroupList = new ArrayList<PlanningGroupClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_PLANNING_GROUP;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						PlanningGroupClass item = new PlanningGroupClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_PGID))));
						item.setPlanningGroup(cursor.getString(cursor.getColumnIndex(KEY_PGPLAGRO)));
						item.setDescription(cursor.getString(cursor.getColumnIndex(KEY_PGDES)));
						planningGroupList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return planningGroupList;
			}
			public PlanningGroupClass getPlanningGroup(int nId) {
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_PLANNING_GROUP, new String[] { KEY_PGID, KEY_PGPLAGRO,
								KEY_PGDES}, KEY_PGID + "=?",
						new String[] { String.valueOf(nId) }, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				PlanningGroupClass planningGroup = new PlanningGroupClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
				db.close();
				return planningGroup;
			}
		}
		/* @CLASS_PRIORITY_CLASSSQL */
		public class AppPrioritySql {
			public AppPrioritySql() { }
			public void deletePriority(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_PRIORITY,null,null);
				db.close();
			}
			public void addPriority(PriorityClass priorityClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_PRIOID, priorityClass.getId());
				values.put(KEY_PRIOPRICOD, priorityClass.getPriorityCode());
				values.put(KEY_PRIODES, priorityClass.getDescription());
				db.insert(TABLE_APP_PRIORITY, null, values);
				db.close();
			}
			public ArrayList<PriorityClass> getAllItem() {
				ArrayList<PriorityClass> priorityClassList = new ArrayList<PriorityClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_PRIORITY;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						PriorityClass item = new PriorityClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_PRIOID))));
						item.setPriorityCode(cursor.getString(cursor.getColumnIndex(KEY_PRIOPRICOD)));
						item.setDescription(cursor.getString(cursor.getColumnIndex(KEY_PRIODES)));
						priorityClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return priorityClassList;
			}
			public PriorityClass getPriority(int nId) {
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_PRIORITY, new String[] { KEY_PRIOID, KEY_PRIOPRICOD,
								KEY_PGDES}, KEY_PRIOID + "=?",
						new String[] { String.valueOf(nId) }, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				PriorityClass priorityClass = new PriorityClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
				db.close();
				return priorityClass;
			}
		}
		/* @CLASS_SYMPTOM */
		public class AppSymptomSql{
			public AppSymptomSql(){ }
			public void deleteSymptom(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_SYMPTOM,null,null);
				db.close();
			}
			public boolean checkIfExists(String id){

				int result = 0;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_SYMPTOM + " WHERE CODE='" + id + "'",null);
				mCount.moveToFirst();
				result= mCount.getInt(0);
				db.close();

				return result > 0;
			}
			public void addSymptom(SymptomClass symptomClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_SYMID, symptomClass.getId());
				values.put(KEY_SYMCATPRO, symptomClass.getCatalogProfile());
				values.put(KEY_SYMSYMCOD, symptomClass.getSymptomCode());
				values.put(KEY_SYMGROCOD, symptomClass.getGroupCode());
				values.put(KEY_SYMVER, symptomClass.getVersion());
				values.put(KEY_SYMINA, symptomClass.getInactive());
				values.put(KEY_SYMDES, symptomClass.getDescription());
				db.insert(TABLE_APP_SYMPTOM, null, values);
				db.close();
			}

			public void updateSymptom(String id, SymptomClass symptomClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				//No id
				values.put(KEY_SYMCATPRO, symptomClass.getCatalogProfile());
				values.put(KEY_SYMSYMCOD, symptomClass.getSymptomCode());
				values.put(KEY_SYMGROCOD, symptomClass.getGroupCode());
				values.put(KEY_SYMVER, symptomClass.getVersion());
				values.put(KEY_SYMINA, symptomClass.getInactive());
				values.put(KEY_SYMDES, symptomClass.getDescription());
				db.update(TABLE_APP_SYMPTOM, values, KEY_SYMSYMCOD + " = ?",new String[] { id});
				db.close();
			}

			public ArrayList<SymptomClass> getAllItem() {
				ArrayList<SymptomClass> symptomClassList = new ArrayList<SymptomClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_SYMPTOM;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						SymptomClass item = new SymptomClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_SYMID))));
						item.setCatalogProfile(cursor.getString(cursor.getColumnIndex(KEY_SYMCATPRO)));
						item.setSymptomCode(cursor.getString(cursor.getColumnIndex(KEY_SYMSYMCOD)));
						item.setGroupCode(cursor.getString(cursor.getColumnIndex(KEY_SYMGROCOD)));
						item.setVersion(cursor.getString(cursor.getColumnIndex(KEY_SYMVER)));
						item.setInactive(cursor.getString(cursor.getColumnIndex(KEY_SYMINA)));
						item.setDescription(cursor.getString(cursor.getColumnIndex(KEY_SYMDES)));
						symptomClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return symptomClassList;
			}
			public ArrayList<SymptomClass> getSymptomProfile(String profile) {
				ArrayList<SymptomClass> symptomClassList = new ArrayList<SymptomClass>();
				String selectQuery = "SELECT * FROM " + TABLE_APP_SYMPTOM + " WHERE RBNR='"+profile+"' GROUP BY "+KEY_SYMSYMCOD;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						SymptomClass item = new SymptomClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_SYMID))));
						item.setCatalogProfile(cursor.getString(cursor.getColumnIndex(KEY_SYMCATPRO)));
						item.setSymptomCode(cursor.getString(cursor.getColumnIndex(KEY_SYMSYMCOD)));
						item.setGroupCode(cursor.getString(cursor.getColumnIndex(KEY_SYMGROCOD)));
						item.setVersion(cursor.getString(cursor.getColumnIndex(KEY_SYMVER)));
						item.setInactive(cursor.getString(cursor.getColumnIndex(KEY_SYMINA)));
						item.setDescription(cursor.getString(cursor.getColumnIndex(KEY_SYMDES)));
						symptomClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return symptomClassList;
			}
		}
		/* @CLASS_TECHNICAL_LOCATIONS */
		public class AppTechnicalLocationsSql{
			public AppTechnicalLocationsSql(){ }
			public void deleteTechnicalLocations(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_TECHNICAL_LOCATIONS ,null,null);
				db.close();
			}

			public boolean checkIfExists(String id){
				int result = 0;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_TECHNICAL_LOCATIONS + " WHERE " + KEY_TLTECLOCCOD + "='" + id + "'",null);
				mCount.moveToFirst();
				result= mCount.getInt(0);
				db.close();
				return result > 0;
			}

			public void addTechnicalLocations(TechnicalLocationsClass technicalLocationsClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_TLID , technicalLocationsClass.getId());
				values.put(KEY_TLTECLOCCOD , technicalLocationsClass.getTechnicalLocationCode());
				values.put(KEY_TLDESLOC , technicalLocationsClass.getDescriptionLocation());
				values.put(KEY_TLSITCEN , technicalLocationsClass.getSiteCenter());
				db.insert(TABLE_APP_TECHNICAL_LOCATIONS, null, values);
				db.close();
			}

			public void updateTechnicalLocations(String id, TechnicalLocationsClass technicalLocationsClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				//No id
				values.put(KEY_TLTECLOCCOD , technicalLocationsClass.getTechnicalLocationCode());
				values.put(KEY_TLDESLOC , technicalLocationsClass.getDescriptionLocation());
				values.put(KEY_TLSITCEN , technicalLocationsClass.getSiteCenter());
				db.update(TABLE_APP_TECHNICAL_LOCATIONS, values, KEY_TLTECLOCCOD + " = ?",new String[] { id});
				db.close();
			}

			public ArrayList<TechnicalLocationsClass> getAllItem() {
				ArrayList<TechnicalLocationsClass> technicalLocationsClassList = new ArrayList<TechnicalLocationsClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_TECHNICAL_LOCATIONS;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						TechnicalLocationsClass item = new TechnicalLocationsClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_TLID))));
						item.setTechnicalLocationCode(cursor.getString(cursor.getColumnIndex(KEY_TLTECLOCCOD)));
						item.setDescriptionLocation(cursor.getString(cursor.getColumnIndex(KEY_TLDESLOC)));
						item.setSiteCenter(cursor.getString(cursor.getColumnIndex(KEY_TLSITCEN)));
						technicalLocationsClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return technicalLocationsClassList;
			}
			public TechnicalLocationsClass getTechnicalLocations(int nId) {
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_TECHNICAL_LOCATIONS , new String[] { KEY_TLID , KEY_TLTECLOCCOD ,
								KEY_TLDESLOC, KEY_TLSITCEN }, KEY_TLID + "=?",
						new String[] { String.valueOf(nId) }, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				TechnicalLocationsClass technicalLocationsClass = new TechnicalLocationsClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),cursor.getString(3));
				db.close();
				return technicalLocationsClass;
			}
		}
		/* @CLASS_SUPPORT_TICKET */
		public class AppSupportTicketSql{
			public AppSupportTicketSql(){ }
			public void deleteSupportTicket(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_SUPPORT_TICKET,null,null);
				db.close();
			}
			public boolean checkIfExistsTicketPending(String equipmentCode){
				int result = 0;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_SUPPORT_TICKET + " WHERE "+ KEY_TICTEA + "='" + equipmentCode + "' AND " + KEY_TICAPP + "='0'" ,null);
				mCount.moveToFirst();
				result= mCount.getInt(0);
				db.close();
				return result > 0;
			}
			public String checkIfExistsInOT(String equipmentCode){
				String result = "";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT * FROM " + TABLE_APP_OT + " WHERE "+ KEY_ORDEQU + "='" + equipmentCode + "' AND " + KEY_ORDCLAS + "!= 'ZPM2'",null);
				if(mCount.moveToFirst()){
					result= mCount.getString(mCount.getColumnIndex(KEY_ORDNUM));
				}
				db.close();
				return result;
			}
			public void addSupportTicket(SupportTicketClass supportTicketClass){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_TICID, supportTicketClass.getId());
				values.put(KEY_TICTICNUM, supportTicketClass.getTicketNumber());
				values.put(KEY_TICNOTTYP, supportTicketClass.getNoticeType());
				values.put(KEY_TICNNOTTYP, supportTicketClass.getNnoticeType());
				values.put(KEY_TICCEN, supportTicketClass.getCenter());
				values.put(KEY_TICLOC  , supportTicketClass.getLocation());
				values.put(KEY_TICNLOC  , supportTicketClass.getNlocation());
				values.put(KEY_TICTEA  , supportTicketClass.getEquipment());
				values.put(KEY_TICNTEA  , supportTicketClass.getNequipment());
				values.put(KEY_TICNOTDAT  , supportTicketClass.getNoticeDate());
				values.put(KEY_TICNOTHOU  , supportTicketClass.getNoticeHour());
				values.put(KEY_TICSYMGROCOD  , supportTicketClass.getSymptomGroupCode());
				values.put(KEY_TICSYMGROSYM  , supportTicketClass.getSymptomGroupSymptom());
				values.put(KEY_TICNSYMGROSYM  , supportTicketClass.getNsymptomGroupSymptom());
				values.put(KEY_TICCAUTEX  , supportTicketClass.getCauseText());
				values.put(KEY_TICSTOEQU  , supportTicketClass.getStoppedEquipment());
				values.put(KEY_TICAPP, supportTicketClass.getApproval()); //
				values.put(KEY_TICLOA  , supportTicketClass.getLoad());
				db.insert(TABLE_APP_SUPPORT_TICKET, null, values);
				db.close();
			}
			public ArrayList<SupportTicketClass> getAllItem(){
				ArrayList<SupportTicketClass> supportTicketClassList = new ArrayList<SupportTicketClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_SUPPORT_TICKET;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						SupportTicketClass item = new SupportTicketClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_TICID))));
						item.setTicketNumber(cursor.getString(cursor.getColumnIndex(KEY_TICTICNUM )));
						item.setNoticeType(cursor.getString(cursor.getColumnIndex(KEY_TICNOTTYP )));
						item.setNnoticeType(cursor.getString(cursor.getColumnIndex(KEY_TICNNOTTYP )));
						item.setCenter(cursor.getString(cursor.getColumnIndex(KEY_TICCEN )));
						item.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TICLOC )));
						item.setNlocation(cursor.getString(cursor.getColumnIndex(KEY_TICNLOC )));
						item.setEquipment(cursor.getString(cursor.getColumnIndex(KEY_TICTEA )));
						item.setNequipment(cursor.getString(cursor.getColumnIndex(KEY_TICNTEA )));
						item.setNoticeDate(cursor.getString(cursor.getColumnIndex(KEY_TICNOTDAT )));
						item.setNoticeHour(cursor.getString(cursor.getColumnIndex(KEY_TICNOTHOU )));
						item.setSymptomGroupCode(cursor.getString(cursor.getColumnIndex(KEY_TICSYMGROCOD )));
						item.setSymptomGroupSymptom(cursor.getString(cursor.getColumnIndex(KEY_TICSYMGROSYM )));
						item.setNsymptomGroupSymptom(cursor.getString(cursor.getColumnIndex(KEY_TICNSYMGROSYM )));
						item.setCauseText(cursor.getString(cursor.getColumnIndex(KEY_TICCAUTEX )));
						item.setStoppedEquipment(cursor.getString(cursor.getColumnIndex(KEY_TICSTOEQU )));
						item.setApproval(cursor.getString(cursor.getColumnIndex(KEY_TICAPP)));
						item.setLoad(cursor.getString(cursor.getColumnIndex(KEY_TICLOA )));
						supportTicketClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return supportTicketClassList;
			}
			public ArrayList<SupportTicketClass> getLoadPendingItem(){
				ArrayList<SupportTicketClass> supportTicketClassList = new ArrayList<SupportTicketClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_SUPPORT_TICKET + " WHERE LOAD='0'";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						SupportTicketClass item = new SupportTicketClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_TICID))));
						item.setTicketNumber(cursor.getString(cursor.getColumnIndex(KEY_TICTICNUM)));
						item.setNoticeType(cursor.getString(cursor.getColumnIndex(KEY_TICNOTTYP)));
						item.setNnoticeType(cursor.getString(cursor.getColumnIndex(KEY_TICNNOTTYP)));
						item.setCenter(cursor.getString(cursor.getColumnIndex(KEY_TICCEN)));
						item.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TICLOC)));
						item.setNlocation(cursor.getString(cursor.getColumnIndex(KEY_TICNLOC)));
						item.setEquipment(cursor.getString(cursor.getColumnIndex(KEY_TICTEA)));
						item.setNequipment(cursor.getString(cursor.getColumnIndex(KEY_TICNTEA)));
						item.setNoticeDate(cursor.getString(cursor.getColumnIndex(KEY_TICNOTDAT)));
						item.setNoticeHour(cursor.getString(cursor.getColumnIndex(KEY_TICNOTHOU)));
						item.setSymptomGroupCode(cursor.getString(cursor.getColumnIndex(KEY_TICSYMGROCOD)));
						item.setSymptomGroupSymptom(cursor.getString(cursor.getColumnIndex(KEY_TICSYMGROSYM)));
						item.setNsymptomGroupSymptom(cursor.getString(cursor.getColumnIndex(KEY_TICNSYMGROSYM)));
						item.setCauseText(cursor.getString(cursor.getColumnIndex(KEY_TICCAUTEX)));
						item.setStoppedEquipment(cursor.getString(cursor.getColumnIndex(KEY_TICSTOEQU)));
						item.setApproval(cursor.getString(cursor.getColumnIndex(KEY_TICAPP)));
						item.setLoad(cursor.getString(cursor.getColumnIndex(KEY_TICLOA)));
						supportTicketClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return supportTicketClassList;
			}
			public void updateLoadSupportTicket(int nId, String nLoad) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_TICLOA, nLoad);
				db.update(TABLE_APP_SUPPORT_TICKET, values, KEY_TICID + " = ?",new String[] { String.valueOf(nId)});
				db.close();
			}
			public void deleteTicket(){
				String whereClause;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				whereClause = "LOAD = '1'";
				db.delete(TABLE_APP_SUPPORT_TICKET, whereClause,null);
				db.close();
			}
		}
		/* @CLASS_ORDERSQL */
		public class AppOTSql {
			public AppOTSql() {}
			public void deleteOT(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_OT,null,null);
				db.close();
			}
			public int checkIfExists(String orderNumber){
				int result = 0;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_OT + " WHERE AUFNR='" + orderNumber + "' AND STATUS<>'0'",null);
				mCount.moveToFirst();
				result= mCount.getInt(0);
				db.close();
				return result;
			}
			public String getStatusTechnical(String orderNumber){
				String result = "";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT TECHAPP FROM " + TABLE_APP_OT + " WHERE AUFNR='" + orderNumber + "'",null);
				mCount.moveToFirst();
				result= mCount.getString(0);
				db.close();
				return result;
			}
			public String getStatusManager(String orderNumber){
				String result = "";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT CUSAPP FROM " + TABLE_APP_OT + " WHERE AUFNR='" + orderNumber + "'",null);
				mCount.moveToFirst();
				result= mCount.getString(0);
				db.close();
				return result;
			}
			public void addOT(OTClass item) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_ORDID, item.getId());
				values.put(KEY_ORDNUM, item.getNumberOrder());
				values.put(KEY_ORDCLAS, item.getClassOrder());
				values.put(KEY_ORDCLASNAM, item.getClassOrderName());
				values.put(KEY_ORDPLAGRO, item.getPlanningGroup());
				values.put(KEY_ORDPLAGRONAM, item.getPlanningGroupName());
				values.put(KEY_ORDPLACEN, item.getPlanningCenter());
				values.put(KEY_ORDPLACENNAM, item.getPlanningCenterName());
				values.put(KEY_ORDJOBTIT, item.getJobTitle());
				values.put(KEY_ORDJOBTITNAM, item.getJobTitleName());
				values.put(KEY_ORDJOBCEN, item.getJobCenter());
				values.put(KEY_ORDJOBCENNAM, item.getJobCenterName());
				values.put(KEY_ORDOBJID, item.getObjID());
				values.put(KEY_ORDOBJID2, item.getObjID2());
				values.put(KEY_ORDSITCEN, item.getSiteCenter());
				values.put(KEY_ORDSITCENNAM, item.getSiteCenterName());
				values.put(KEY_ORDTEX, item.getText());
				values.put(KEY_ORDEXTSTADAT, item.getExtremeStartDate());
				values.put(KEY_ORDEXTENDDAT, item.getExtremeEndDate());
				values.put(KEY_ORDSTATIM, item.getStartTime());
				values.put(KEY_ORDENDTIM, item.getEndTime());
				values.put(KEY_ORDORDREF, item.getOrderReference());
				values.put(KEY_ORDPRIO, item.getPriority());
				values.put(KEY_ORDPRIONAM, item.getPriorityName());
				values.put(KEY_ORDTECLOC, item.getTechnicalLocation());
				values.put(KEY_ORDTECLOCNAM, item.getTechnicalLocationName());
				values.put(KEY_ORDEQU, item.getEquipment());
				values.put(KEY_ORDEQUNAM, item.getEquipmentName());
				values.put(KEY_ORDOBJUSE, item.getObjUser());
				values.put(KEY_ORDTECCLO, item.getTechnicianClosing());
				values.put(KEY_ORDCLICLO, item.getClientClosure());
				values.put(KEY_ORDOBS, item.getObservation());
				values.put(KEY_ORDOBSC, item.getObservationc());
				values.put(KEY_ORDWORSTADAT, item.getWorkStartDate());
				values.put(KEY_ORDWORSTATIM, item.getWorkStartTime());
				values.put(KEY_ORDWORENDDAT, item.getWorkEndDate());
				values.put(KEY_ORDENDTIMWOR, item.getEndTimeWork());
				values.put(KEY_ORDTOTTIMEHOU, item.getTotalTimeHours());
				values.put(KEY_ORDRESPTEC, item.getTechnicianObservation());
				values.put(KEY_ORDVALTEC, item.getTechnicianValue());
				values.put(KEY_ORDRESPSUP, item.getSupervisorObservation());
				values.put(KEY_ORDVALSUP, item.getSupervisorValue());
				values.put(KEY_ORDSTA, item.getStatus());
				values.put(KEY_ORDLOA, item.getLoad());
				values.put(KEY_ORDCRE, item.getCreated());
				values.put(KEY_ORDOBSI, item.getObsi());

				db.insert(TABLE_APP_OT, null, values);
				db.close();
			}
            public int countOrderStatus(String orderNumber, String type) {
			    String AND = ""; int result = 0;
			    if(type.equals("ALL")){
			        AND = " ";
                }else{
			        AND = " AND TECHAPP='1'";
                }
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_OT + " WHERE AUFNR='" + orderNumber + "' "+AND ,null);
                mCount.moveToFirst();
                result= mCount.getInt(0);
                db.close();
                return result;
            }
			public ArrayList<String> getDeleteList(){
				ArrayList<String> list = new ArrayList<String>();
				String selectQuery = "SELECT * FROM " + TABLE_APP_OT + " WHERE STATUS='0' OR (LOAD = '1' AND (ENDDA < datetime('now', '-2 days')))";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						list.add(cursor.getString(cursor.getColumnIndex(KEY_ORDNUM)));
					} while (cursor.moveToNext());
				}
				db.close();
				return list;
			}
			public ArrayList<OTClass> getOT() {
				ArrayList<OTClass> customerList = new ArrayList<OTClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_OT + " GROUP BY AUFNR ORDER BY AUART, GSTRP, PRIOK ASC";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						OTClass item = new OTClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_ORDID ))));
						item.setNumberOrder(cursor.getString((cursor.getColumnIndex(KEY_ORDNUM))));
						item.setClassOrder(cursor.getString((cursor.getColumnIndex(KEY_ORDCLAS))));
						item.setClassOrderName(cursor.getString((cursor.getColumnIndex(KEY_ORDCLASNAM))));
						item.setPlanningGroup(cursor.getString((cursor.getColumnIndex(KEY_ORDPLAGRO))));
						item.setPlanningGroupName(cursor.getString((cursor.getColumnIndex(KEY_ORDPLAGRONAM))));
						item.setPlanningCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDPLACEN))));
						item.setPlanningCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDPLACENNAM))));
						item.setJobTitle(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBTIT))));
						item.setJobTitleName(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBTITNAM))));
						item.setJobCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBCEN))));
						item.setJobCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBCENNAM))));
						item.setObjID(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJID))));
						item.setObjID2(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJID2))));
						item.setSiteCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDSITCEN))));
						item.setSiteCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDSITCENNAM))));
						item.setText(cursor.getString((cursor.getColumnIndex(KEY_ORDTEX))));
						item.setExtremeStartDate(cursor.getString((cursor.getColumnIndex(KEY_ORDEXTSTADAT))));
						item.setExtremeEndDate(cursor.getString((cursor.getColumnIndex(KEY_ORDEXTENDDAT))));
						item.setStartTime(cursor.getString((cursor.getColumnIndex(KEY_ORDSTATIM))));
						item.setEndTime(cursor.getString((cursor.getColumnIndex(KEY_ORDENDTIM))));
						item.setOrderReference(cursor.getString((cursor.getColumnIndex(KEY_ORDORDREF))));
						item.setPriority(cursor.getString((cursor.getColumnIndex(KEY_ORDPRIO))));
						item.setPriorityName(cursor.getString((cursor.getColumnIndex(KEY_ORDPRIONAM))));
						item.setTechnicalLocation(cursor.getString((cursor.getColumnIndex(KEY_ORDTECLOC))));
						item.setTechnicalLocationName(cursor.getString((cursor.getColumnIndex(KEY_ORDTECLOCNAM))));
						item.setEquipment(cursor.getString((cursor.getColumnIndex(KEY_ORDEQU))));
						item.setEquipmentName(cursor.getString((cursor.getColumnIndex(KEY_ORDEQUNAM))));
						item.setObjUser(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJUSE))));
						item.setTechnicianClosing(cursor.getString((cursor.getColumnIndex(KEY_ORDTECCLO))));
						item.setClientClosure(cursor.getString((cursor.getColumnIndex(KEY_ORDCLICLO))));
						item.setObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDOBS))));
						item.setObservationc(cursor.getString((cursor.getColumnIndex(KEY_ORDOBSC))));
						item.setWorkStartDate(cursor.getString((cursor.getColumnIndex(KEY_ORDWORSTADAT))));
						item.setWorkStartTime(cursor.getString((cursor.getColumnIndex(KEY_ORDWORSTATIM))));
						item.setWorkEndDate(cursor.getString((cursor.getColumnIndex(KEY_ORDWORENDDAT))));
						item.setEndTimeWork(cursor.getString((cursor.getColumnIndex(KEY_ORDENDTIMWOR))));
						item.setTotalTimeHours(cursor.getString((cursor.getColumnIndex(KEY_ORDTOTTIMEHOU))));

						item.setTechnicianObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDRESPTEC))));
						item.setTechnicianValue(cursor.getString((cursor.getColumnIndex(KEY_ORDVALTEC))));
						item.setSupervisorObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDRESPSUP))));
						item.setSupervisorValue(cursor.getString((cursor.getColumnIndex(KEY_ORDVALSUP))));

						item.setStatus(cursor.getString((cursor.getColumnIndex(KEY_ORDSTA))));
						item.setLoad(cursor.getString((cursor.getColumnIndex(KEY_ORDLOA))));
						item.setCreated(cursor.getString((cursor.getColumnIndex(KEY_ORDCRE))));
						item.setObsi(cursor.getString((cursor.getColumnIndex(KEY_ORDOBSI))));
						customerList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return customerList;
			}
			public ArrayList<OTClass> getOTReady() {
				ArrayList<OTClass> customerList = new ArrayList<OTClass>();
				String selectQuery = "SELECT * FROM " + TABLE_APP_OT + " WHERE STATUS='2' OR STATUS='3'";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						OTClass item = new OTClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_ORDID ))));
						item.setNumberOrder(cursor.getString((cursor.getColumnIndex(KEY_ORDNUM))));
						item.setClassOrder(cursor.getString((cursor.getColumnIndex(KEY_ORDCLAS))));
						item.setClassOrderName(cursor.getString((cursor.getColumnIndex(KEY_ORDCLASNAM))));
						item.setPlanningGroup(cursor.getString((cursor.getColumnIndex(KEY_ORDPLAGRO))));
						item.setPlanningGroupName(cursor.getString((cursor.getColumnIndex(KEY_ORDPLAGRONAM))));
						item.setPlanningCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDPLACEN))));
						item.setPlanningCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDPLACENNAM))));
						item.setJobTitle(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBTIT))));
						item.setJobTitleName(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBTITNAM))));
						item.setJobCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBCEN))));
						item.setJobCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBCENNAM))));
						item.setObjID(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJID))));
						item.setObjID2(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJID2))));
						item.setSiteCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDSITCEN))));
						item.setSiteCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDSITCENNAM))));
						item.setText(cursor.getString((cursor.getColumnIndex(KEY_ORDTEX))));
						item.setExtremeStartDate(cursor.getString((cursor.getColumnIndex(KEY_ORDEXTSTADAT))));
						item.setExtremeEndDate(cursor.getString((cursor.getColumnIndex(KEY_ORDEXTENDDAT))));
						item.setStartTime(cursor.getString((cursor.getColumnIndex(KEY_ORDSTATIM))));
						item.setEndTime(cursor.getString((cursor.getColumnIndex(KEY_ORDENDTIM))));
						item.setOrderReference(cursor.getString((cursor.getColumnIndex(KEY_ORDORDREF))));
						item.setPriority(cursor.getString((cursor.getColumnIndex(KEY_ORDPRIO))));
						item.setPriorityName(cursor.getString((cursor.getColumnIndex(KEY_ORDPRIONAM))));
						item.setTechnicalLocation(cursor.getString((cursor.getColumnIndex(KEY_ORDTECLOC))));
						item.setTechnicalLocationName(cursor.getString((cursor.getColumnIndex(KEY_ORDTECLOCNAM))));
						item.setEquipment(cursor.getString((cursor.getColumnIndex(KEY_ORDEQU))));
						item.setEquipmentName(cursor.getString((cursor.getColumnIndex(KEY_ORDEQUNAM))));
						item.setObjUser(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJUSE))));
						item.setTechnicianClosing(cursor.getString((cursor.getColumnIndex(KEY_ORDTECCLO))));
						item.setClientClosure(cursor.getString((cursor.getColumnIndex(KEY_ORDCLICLO))));
						item.setObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDOBS))));
						item.setObservationc(cursor.getString((cursor.getColumnIndex(KEY_ORDOBSC))));
						item.setWorkStartDate(cursor.getString((cursor.getColumnIndex(KEY_ORDWORSTADAT))));
						item.setWorkStartTime(cursor.getString((cursor.getColumnIndex(KEY_ORDWORSTATIM))));
						item.setWorkEndDate(cursor.getString((cursor.getColumnIndex(KEY_ORDWORENDDAT))));
						item.setEndTimeWork(cursor.getString((cursor.getColumnIndex(KEY_ORDENDTIMWOR))));
						item.setTotalTimeHours(cursor.getString((cursor.getColumnIndex(KEY_ORDTOTTIMEHOU))));
						item.setTechnicianObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDRESPTEC))));
						item.setTechnicianValue(cursor.getString((cursor.getColumnIndex(KEY_ORDVALTEC))));
						item.setSupervisorObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDRESPSUP))));
						item.setSupervisorValue(cursor.getString((cursor.getColumnIndex(KEY_ORDVALSUP))));
						item.setStatus(cursor.getString((cursor.getColumnIndex(KEY_ORDSTA))));
						item.setLoad(cursor.getString((cursor.getColumnIndex(KEY_ORDLOA))));
						item.setCreated(cursor.getString((cursor.getColumnIndex(KEY_ORDCRE))));
						item.setObsi(cursor.getString((cursor.getColumnIndex(KEY_ORDOBSI))));
						customerList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return customerList;
			}
            public ArrayList<OTClass> getOrderNumber(String numberOrder) {
                ArrayList<OTClass> customerList = new ArrayList<OTClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_OT + " WHERE AUFNR='"+numberOrder+"' ORDER BY TECHAPP DESC LIMIT 1";
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        OTClass item = new OTClass();
                        item.setId(cursor.getInt((cursor.getColumnIndex(KEY_ORDID ))));
                        item.setNumberOrder(cursor.getString((cursor.getColumnIndex(KEY_ORDNUM))));
                        item.setClassOrder(cursor.getString((cursor.getColumnIndex(KEY_ORDCLAS))));
                        item.setClassOrderName(cursor.getString((cursor.getColumnIndex(KEY_ORDCLASNAM))));
                        item.setPlanningGroup(cursor.getString((cursor.getColumnIndex(KEY_ORDPLAGRO))));
                        item.setPlanningGroupName(cursor.getString((cursor.getColumnIndex(KEY_ORDPLAGRONAM))));
                        item.setPlanningCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDPLACEN))));
                        item.setPlanningCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDPLACENNAM))));
                        item.setJobTitle(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBTIT))));
                        item.setJobTitleName(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBTITNAM))));
                        item.setJobCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBCEN))));
                        item.setJobCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBCENNAM))));
                        item.setObjID(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJID))));
						item.setObjID2(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJID2))));
                        item.setSiteCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDSITCEN))));
                        item.setSiteCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDSITCENNAM))));
                        item.setText(cursor.getString((cursor.getColumnIndex(KEY_ORDTEX))));
                        item.setExtremeStartDate(cursor.getString((cursor.getColumnIndex(KEY_ORDEXTSTADAT))));
                        item.setExtremeEndDate(cursor.getString((cursor.getColumnIndex(KEY_ORDEXTENDDAT))));
                        item.setStartTime(cursor.getString((cursor.getColumnIndex(KEY_ORDSTATIM))));
                        item.setEndTime(cursor.getString((cursor.getColumnIndex(KEY_ORDENDTIM))));
                        item.setOrderReference(cursor.getString((cursor.getColumnIndex(KEY_ORDORDREF))));
						item.setPriority(cursor.getString((cursor.getColumnIndex(KEY_ORDPRIO))));
						item.setPriorityName(cursor.getString((cursor.getColumnIndex(KEY_ORDPRIONAM))));
                        item.setTechnicalLocation(cursor.getString((cursor.getColumnIndex(KEY_ORDTECLOC))));
                        item.setTechnicalLocationName(cursor.getString((cursor.getColumnIndex(KEY_ORDTECLOCNAM))));
                        item.setEquipment(cursor.getString((cursor.getColumnIndex(KEY_ORDEQU))));
                        item.setEquipmentName(cursor.getString((cursor.getColumnIndex(KEY_ORDEQUNAM))));
                        item.setObjUser(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJUSE))));
                        item.setTechnicianClosing(cursor.getString((cursor.getColumnIndex(KEY_ORDTECCLO))));
                        item.setClientClosure(cursor.getString((cursor.getColumnIndex(KEY_ORDCLICLO))));
                        item.setObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDOBS))));
						item.setObservationc(cursor.getString((cursor.getColumnIndex(KEY_ORDOBSC))));
                        item.setWorkStartDate(cursor.getString((cursor.getColumnIndex(KEY_ORDWORSTADAT))));
                        item.setWorkStartTime(cursor.getString((cursor.getColumnIndex(KEY_ORDWORSTATIM))));
                        item.setWorkEndDate(cursor.getString((cursor.getColumnIndex(KEY_ORDWORENDDAT))));
                        item.setEndTimeWork(cursor.getString((cursor.getColumnIndex(KEY_ORDENDTIMWOR))));
                        item.setTotalTimeHours(cursor.getString((cursor.getColumnIndex(KEY_ORDTOTTIMEHOU))));
						item.setTechnicianObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDRESPTEC))));
						item.setTechnicianValue(cursor.getString((cursor.getColumnIndex(KEY_ORDVALTEC))));
						item.setSupervisorObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDRESPSUP))));
						item.setSupervisorValue(cursor.getString((cursor.getColumnIndex(KEY_ORDVALSUP))));
                        item.setStatus(cursor.getString((cursor.getColumnIndex(KEY_ORDSTA))));
						item.setLoad(cursor.getString((cursor.getColumnIndex(KEY_ORDLOA))));
						item.setCreated(cursor.getString((cursor.getColumnIndex(KEY_ORDCRE))));
						item.setObsi(cursor.getString((cursor.getColumnIndex(KEY_ORDOBSI))));
                        customerList.add(item);
                    } while (cursor.moveToNext());
                }
                db.close();
                return customerList;
            }
			public ArrayList<OTClass> getPendingOT() {
				ArrayList<OTClass> customerList = new ArrayList<OTClass>();
				String selectQuery = "SELECT * FROM " + TABLE_APP_OT + " WHERE LOAD='0' AND (STATUS='2' OR STATUS='3')";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						OTClass item = new OTClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_ORDID ))));
						item.setNumberOrder(cursor.getString((cursor.getColumnIndex(KEY_ORDNUM))));
						item.setClassOrder(cursor.getString((cursor.getColumnIndex(KEY_ORDCLAS))));
						item.setClassOrderName(cursor.getString((cursor.getColumnIndex(KEY_ORDCLASNAM))));
						item.setPlanningGroup(cursor.getString((cursor.getColumnIndex(KEY_ORDPLAGRO))));
						item.setPlanningGroupName(cursor.getString((cursor.getColumnIndex(KEY_ORDPLAGRONAM))));
						item.setPlanningCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDPLACEN))));
						item.setPlanningCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDPLACENNAM))));
						item.setJobTitle(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBTIT))));
						item.setJobTitleName(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBTITNAM))));
						item.setJobCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBCEN))));
						item.setJobCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDJOBCENNAM))));
						item.setObjID(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJID))));
						item.setSiteCenter(cursor.getString((cursor.getColumnIndex(KEY_ORDSITCEN))));
						item.setSiteCenterName(cursor.getString((cursor.getColumnIndex(KEY_ORDSITCENNAM))));
						item.setText(cursor.getString((cursor.getColumnIndex(KEY_ORDTEX))));
						item.setExtremeStartDate(cursor.getString((cursor.getColumnIndex(KEY_ORDEXTSTADAT))));
						item.setExtremeEndDate(cursor.getString((cursor.getColumnIndex(KEY_ORDEXTENDDAT))));
						item.setStartTime(cursor.getString((cursor.getColumnIndex(KEY_ORDSTATIM))));
						item.setEndTime(cursor.getString((cursor.getColumnIndex(KEY_ORDENDTIM))));
						item.setOrderReference(cursor.getString((cursor.getColumnIndex(KEY_ORDORDREF))));
						item.setPriority(cursor.getString((cursor.getColumnIndex(KEY_ORDPRIO))));
						item.setPriorityName(cursor.getString((cursor.getColumnIndex(KEY_ORDPRIONAM))));
						item.setTechnicalLocation(cursor.getString((cursor.getColumnIndex(KEY_ORDTECLOC))));
						item.setTechnicalLocationName(cursor.getString((cursor.getColumnIndex(KEY_ORDTECLOCNAM))));
						item.setEquipment(cursor.getString((cursor.getColumnIndex(KEY_ORDEQU))));
						item.setEquipmentName(cursor.getString((cursor.getColumnIndex(KEY_ORDEQUNAM))));
						item.setObjUser(cursor.getString((cursor.getColumnIndex(KEY_ORDOBJUSE))));
						item.setTechnicianClosing(cursor.getString((cursor.getColumnIndex(KEY_ORDTECCLO))));
						item.setClientClosure(cursor.getString((cursor.getColumnIndex(KEY_ORDCLICLO))));
						item.setObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDOBS))));
						item.setObservationc(cursor.getString((cursor.getColumnIndex(KEY_ORDOBSC))));
						item.setWorkStartDate(cursor.getString((cursor.getColumnIndex(KEY_ORDWORSTADAT))));
						item.setWorkStartTime(cursor.getString((cursor.getColumnIndex(KEY_ORDWORSTATIM))));
						item.setWorkEndDate(cursor.getString((cursor.getColumnIndex(KEY_ORDWORENDDAT))));
						item.setEndTimeWork(cursor.getString((cursor.getColumnIndex(KEY_ORDENDTIMWOR))));
						item.setTotalTimeHours(cursor.getString((cursor.getColumnIndex(KEY_ORDTOTTIMEHOU))));

						item.setTechnicianObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDRESPTEC))));
						item.setTechnicianValue(cursor.getString((cursor.getColumnIndex(KEY_ORDVALTEC))));
						item.setSupervisorObservation(cursor.getString((cursor.getColumnIndex(KEY_ORDRESPSUP))));
						item.setSupervisorValue(cursor.getString((cursor.getColumnIndex(KEY_ORDVALSUP))));

						item.setStatus(cursor.getString((cursor.getColumnIndex(KEY_ORDSTA))));
						item.setLoad(cursor.getString((cursor.getColumnIndex(KEY_ORDLOA))));
						item.setCreated(cursor.getString((cursor.getColumnIndex(KEY_ORDCRE))));
						item.setObsi(cursor.getString((cursor.getColumnIndex(KEY_ORDOBSI))));
						customerList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return customerList;
			}
			public String getOTInfo(String orderNumber, String field) {
                String result = "";
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor mCount= db.rawQuery("SELECT " + field + " FROM " + TABLE_APP_OT + " WHERE AUFNR = '" + orderNumber + "'",null);
                mCount.moveToFirst();
                result= mCount.getString(0);
                db.close();
                return result;
            }
            public void updateOT(String orderNumber, String field, String value) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(field, value);
                db.update(TABLE_APP_OT, values, KEY_ORDNUM + " = ?",new String[] { String.valueOf(orderNumber)});
                db.close();
            }
            public String getData(String orderNumber) {
                String _data = null;
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_OT, new String[] { KEY_ORDSTA }, KEY_ORDNUM + "=?",
                        new String[] {orderNumber}, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                _data = cursor.getString(0);
                db.close();
                return _data;
            }
            public void deleteOT(String orderNumber){
                String whereClause;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                whereClause = "AUFNR = '" + orderNumber + "'";
                db.delete(TABLE_APP_OT, whereClause,null);
                db.close();
            }
		}
		/* @CLASS_OT_OPERATIONS_CLASS */
		public class AppOTOperationsSql{
			public AppOTOperationsSql(){ }
			public void deleteOTOperation(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_OT_OPERATIONS,null,null);
				db.close();
			}
			public void addOTOperation(OTOperationsClass otOperationsClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_OTOPEID, otOperationsClass.getId());
				values.put(KEY_OTOPEORDNUM, otOperationsClass.getNumberOrder());
				values.put(KEY_OTOPEORDOPE, otOperationsClass.getNumberOperation());
				values.put(KEY_OTOPEJOB, otOperationsClass.getJobTitle());
				values.put(KEY_OTOPEJOBNAM, otOperationsClass.getJobTitleName());
				values.put(KEY_OTOPECEN, otOperationsClass.getCenter());
				values.put(KEY_OTOPECENNAM, otOperationsClass.getCenterName());
				values.put(KEY_OTOPEOBJID, otOperationsClass.getObjID());
				values.put(KEY_OTOPECONKEY, otOperationsClass.getControlKey());
				values.put(KEY_OTOPESHOTEX, otOperationsClass.getShortText());
				values.put(KEY_OTOPESTADAT, otOperationsClass.getStartDate());
				values.put(KEY_OTOPESTAHOU, otOperationsClass.getStartTime());
				values.put(KEY_OTOPEENDDAT, otOperationsClass.getEndDate());
				values.put(KEY_OTOPEENDHOU, otOperationsClass.getEndTime());
				values.put(KEY_OTOPEUSE, otOperationsClass.getObjUser());
				values.put(KEY_OTOPESTA, otOperationsClass.getStatus());
				values.put(KEY_OTOPEOBS, otOperationsClass.getObservation());
				values.put(KEY_OTOPETIM, otOperationsClass.getTimeRequired());
				db.insert(TABLE_APP_OT_OPERATIONS, null, values);
				db.close();
			}
			public ArrayList<OTOperationsClass> getOrderNumber(String numberOrder)  {
				ArrayList<OTOperationsClass> otOperationsClassList = new ArrayList<OTOperationsClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_OT_OPERATIONS + " WHERE AUFNR='"+numberOrder+"'";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						OTOperationsClass item = new OTOperationsClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_OTOPEID))));
						item.setNumberOrder(cursor.getString(cursor.getColumnIndex(KEY_OTOPEORDNUM)));
						item.setNumberOperation(cursor.getString(cursor.getColumnIndex(KEY_OTOPEORDOPE)));
						item.setJobTitle(cursor.getString(cursor.getColumnIndex(KEY_OTOPEJOB)));
						item.setJobTitleName(cursor.getString(cursor.getColumnIndex(KEY_OTOPEJOBNAM)));
						item.setCenter(cursor.getString(cursor.getColumnIndex(KEY_OTOPECEN)));
						item.setCenterName(cursor.getString(cursor.getColumnIndex(KEY_OTOPECENNAM)));
						item.setObjID(cursor.getString(cursor.getColumnIndex(KEY_OTOPEOBJID)));
						item.setControlKey(cursor.getString(cursor.getColumnIndex(KEY_OTOPECONKEY)));
						item.setShortText(cursor.getString(cursor.getColumnIndex(KEY_OTOPESHOTEX)));
						item.setStartDate(cursor.getString(cursor.getColumnIndex(KEY_OTOPESTADAT)));
						item.setStartTime(cursor.getString(cursor.getColumnIndex(KEY_OTOPESTAHOU)));
						item.setEndDate(cursor.getString(cursor.getColumnIndex(KEY_OTOPEENDDAT)));
						item.setEndTime(cursor.getString(cursor.getColumnIndex(KEY_OTOPEENDHOU)));
						item.setObjUser(cursor.getString(cursor.getColumnIndex(KEY_OTOPEUSE)));
						item.setStatus(cursor.getString(cursor.getColumnIndex(KEY_OTOPESTA)));
						item.setObservation(cursor.getString(cursor.getColumnIndex(KEY_OTOPEOBS)));
						item.setTimeRequired(cursor.getString(cursor.getColumnIndex(KEY_OTOPETIM)));
						otOperationsClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return otOperationsClassList;
			}

			public int countElement(String orderNumber) {
				int result = 0;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_OT_OPERATIONS + " WHERE AUFNR='" + orderNumber + "' AND DONE='0'"  ,null);
				mCount.moveToFirst();
				result= mCount.getInt(0);
				db.close();
				return result;
			}
            public void updateOTOperation(int id, String field, String value) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(field, value);
                db.update(TABLE_APP_OT_OPERATIONS, values, KEY_OTOPEID + " = ?",new String[] { String.valueOf(id)});
                db.close();
            }
			public String getOTOperation(int id, String field) {
				String result = "";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT " + field + " FROM " + TABLE_APP_OT_OPERATIONS + " WHERE id = '" + id + "'",null);
				mCount.moveToFirst();
				result= mCount.getString(0);
				db.close();
				return result;
			}
            public void deleteOTOperation(String orderNumber){
                String whereClause;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                whereClause = "AUFNR = '" + orderNumber + "'";
                db.delete(TABLE_APP_OT_OPERATIONS, whereClause,null);
                db.close();
            }
		}
		/* @CLASS_OT_COMPONENTS_CLASS */
		public class AppOTComponentsSql{
			public AppOTComponentsSql(){ }
			public void deleteOTComponent(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_OT_COMPONENTS,null,null);
				db.close();
			}
			public void addOTComponent(OTComponentsClass otComponentsClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_OTCOMID, otComponentsClass.getId());
				values.put(KEY_OTCOMORDNUM, otComponentsClass.getNumberOrder());
				values.put(KEY_OTCOMOPENUM, otComponentsClass.getNumberOperation());
				values.put(KEY_OTCOMPOS, otComponentsClass.getPosition());
				values.put(KEY_OTCOMOBJID, otComponentsClass.getObjID());
				values.put(KEY_OTCOMNUMMAT, otComponentsClass.getItemCode());
				values.put(KEY_OTCOMDESMAT, otComponentsClass.getItemDescription());
				values.put(KEY_OTCOMUMISO, otComponentsClass.getUmISO());
				values.put(KEY_OTCOMUNIMEA, otComponentsClass.getUnitMeasure());
				values.put(KEY_OTCOMQUA, otComponentsClass.getQuantity());
				values.put(KEY_OTCOMUSE, otComponentsClass.getObjUser());
				values.put(KEY_OTCOMSTA, otComponentsClass.getStatus());
				values.put(KEY_OTCOMOBS, otComponentsClass.getObservation());
				values.put(KEY_OTCOMAMOUSE, otComponentsClass.getAmountUsed());
				db.insert(TABLE_APP_OT_COMPONENTS, null, values);
				db.close();
			}
			public ArrayList<OTComponentsClass> getOrderNumber(String numberOrder) {
				ArrayList<OTComponentsClass> otComponentsClassList = new ArrayList<OTComponentsClass>();
				String selectQuery = "SELECT * FROM " + TABLE_APP_OT_COMPONENTS + " WHERE AUFNR='"+ numberOrder+"'";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						OTComponentsClass item = new OTComponentsClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_OTCOMID))));
						item.setNumberOrder(cursor.getString(cursor.getColumnIndex(KEY_OTCOMORDNUM)));
						item.setNumberOperation(cursor.getString(cursor.getColumnIndex(KEY_OTCOMOPENUM)));
						item.setPosition(cursor.getString(cursor.getColumnIndex(KEY_OTCOMPOS)));
						item.setObjID(cursor.getString(cursor.getColumnIndex(KEY_OTCOMOBJID)));
						item.setItemCode(cursor.getString(cursor.getColumnIndex(KEY_OTCOMNUMMAT)));
						item.setItemDescription(cursor.getString(cursor.getColumnIndex(KEY_OTCOMDESMAT)));
						item.setUmISO(cursor.getString(cursor.getColumnIndex(KEY_OTCOMUMISO)));
						item.setUnitMeasure(cursor.getString(cursor.getColumnIndex(KEY_OTCOMUNIMEA)));
						item.setQuantity(cursor.getString(cursor.getColumnIndex(KEY_OTCOMQUA)));
						item.setObjUser(cursor.getString(cursor.getColumnIndex(KEY_OTCOMUSE)));
						item.setStatus(cursor.getString(cursor.getColumnIndex(KEY_OTCOMSTA)));
						item.setObservation(cursor.getString(cursor.getColumnIndex(KEY_OTCOMOBS)));
						item.setAmountUsed(cursor.getString(cursor.getColumnIndex(KEY_OTCOMAMOUSE)));
						otComponentsClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return otComponentsClassList;
			}
            public void updateOTComponent(int id, String field, String value) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(field, value);
                db.update(TABLE_APP_OT_COMPONENTS, values, KEY_OTCOMID + " = ?",new String[] { String.valueOf(id)});
                db.close();
            }
			public String getOTComponent(int id, String field) {
				String result = "";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT " + field + " FROM " + TABLE_APP_OT_COMPONENTS + " WHERE id = '" + id + "'",null);
				mCount.moveToFirst();
				result= mCount.getString(0);
				db.close();
				return result;
			}
			public int countElement(String orderNumber) {
				int result = 0;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_OT_COMPONENTS + " WHERE AUFNR='" + orderNumber +
						"' AND DONE = '0'"  ,null);
				mCount.moveToFirst();
				result= mCount.getInt(0);
				db.close();
				return result;
			}
			public void deleteOTComponents(String orderNumber){
				String whereClause;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
                whereClause = "AUFNR = '" + orderNumber + "'";
    			db.delete(TABLE_APP_OT_COMPONENTS, whereClause,null);
				db.close();
			}
		}
		/* @CLASS_OT_MEASUREMENT_POINTS */
		public class AppOTMeasurementPointsSql{
			public AppOTMeasurementPointsSql(){	}
			public void deleteMeasurementPoints(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_OT_MEASUREMENT_POINTS,null,null);
				db.close();
			}
			public void addMeasurementPoints(OTMeasurementPointClass otMeasurementPointClass) {
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_OTMID, otMeasurementPointClass.getId());
				values.put(KEY_OTMORDNUM, otMeasurementPointClass.getNumberOrder());
				values.put(KEY_OTMPOINUM, otMeasurementPointClass.getNumberPoint());
				values.put(KEY_OTMTEACOD, otMeasurementPointClass.getEquipment());
				values.put(KEY_OTMTEANAM, otMeasurementPointClass.getEquipmentName());
				values.put(KEY_OTMOBJID, otMeasurementPointClass.getObjUser());
				values.put(KEY_OTMDAT, otMeasurementPointClass.get_date());
				values.put(KEY_OTMHOU, otMeasurementPointClass.getHour());
				values.put(KEY_OTMSTA, otMeasurementPointClass.getStatus());
				values.put(KEY_OTMOBS, otMeasurementPointClass.getObservation());
				values.put(KEY_OTMMEAVAL, otMeasurementPointClass.getMeasuredValue());
				db.insert(TABLE_APP_OT_MEASUREMENT_POINTS, null, values);
				db.close();
			}
			public ArrayList<OTMeasurementPointClass> getOrderNumber(String numberOrder, String objid) {
				ArrayList<OTMeasurementPointClass> otMeasurementPointClassList = new ArrayList<OTMeasurementPointClass>();
				String selectQuery = "";
				if(ConstValue.getUserTypeString().equals("T")){
					selectQuery = "SELECT * FROM " + TABLE_APP_OT_MEASUREMENT_POINTS + " WHERE AUFNR='"+numberOrder+"' AND OBJID='"+objid+"'";
				}else{
					selectQuery = "SELECT * FROM " + TABLE_APP_OT_MEASUREMENT_POINTS + " WHERE AUFNR='"+numberOrder+"'";
				}
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						OTMeasurementPointClass item = new OTMeasurementPointClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_OTMID ))));
						item.setNumberOrder(cursor.getString((cursor.getColumnIndex(KEY_OTMORDNUM))));
						item.setNumberPoint(cursor.getString((cursor.getColumnIndex(KEY_OTMPOINUM))));
						item.setEquipment(cursor.getString((cursor.getColumnIndex(KEY_OTMTEACOD))));
						item.setEquipmentName(cursor.getString((cursor.getColumnIndex(KEY_OTMTEANAM))));
						item.setObjUser(cursor.getString((cursor.getColumnIndex(KEY_OTMOBJID))));
						item.set_date(cursor.getString((cursor.getColumnIndex(KEY_OTMDAT))));
						item.setHour(cursor.getString(cursor.getColumnIndex(KEY_OTMHOU)));
						item.setStatus(cursor.getString((cursor.getColumnIndex(KEY_OTMSTA))));
						item.setObservation(cursor.getString((cursor.getColumnIndex(KEY_OTMOBS))));
						item.setMeasuredValue(cursor.getString((cursor.getColumnIndex(KEY_OTMMEAVAL))));
						otMeasurementPointClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return otMeasurementPointClassList;
			}
			public int countElement(String orderNumber) {
				int result = 0;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_OT_MEASUREMENT_POINTS + " WHERE AUFNR='" + orderNumber +
						"' AND TAKEN = '0'"  ,null);
				mCount.moveToFirst();
				result= mCount.getInt(0);
				db.close();
				return result;
			}
            public void updateOTMeasurementPoint(int id, String field, String value) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(field, value);
                db.update(TABLE_APP_OT_MEASUREMENT_POINTS, values, KEY_OTMID + " = ?",new String[] { String.valueOf(id)});
                db.close();
            }

			public String getOTMeasurementPoint(int id, String field) {
				String result = "";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT " + field + " FROM " + TABLE_APP_OT_MEASUREMENT_POINTS + " WHERE id = '" + id + "'",null);
				mCount.moveToFirst();
				result= mCount.getString(0);
				db.close();
				return result;
			}
            public void deleteOTMeasurementPoint(String orderNumber){
                String whereClause;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                whereClause = "AUFNR = '" + orderNumber + "'";
                db.delete(TABLE_APP_OT_MEASUREMENT_POINTS, whereClause,null);
                db.close();
            }
		}

		public class AppConfigurationSql {
			public AppConfigurationSql(){}
			public void addConfiguration(ConfigurationClass configurationClass){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_CONID  , configurationClass.getId());
				values.put(KEY_CONCOLEOSTAR , configurationClass.getColorEONotStarting());
				values.put(KEY_CONGEODIS  , configurationClass.getGeoDistance());
				values.put(KEY_CONSATVALMED  , configurationClass.getSftcnMedium());
				values.put(KEY_CONSATQUETEC  , configurationClass.getStfcnQstnTecnic());
				values.put(KEY_CONCOLEMER  , configurationClass.getColorOTEmergency());
				values.put(KEY_CONSATVALHIG  , configurationClass.getSftcnHigh());
				values.put(KEY_CONNOT , configurationClass.getNotification());
				values.put(KEY_CONSATQUESUP  , configurationClass.getStfcnQstnManager());
				values.put(KEY_CONCOLOTRUT  , configurationClass.getColorOTRutine());
				values.put(KEY_CONCOLEOPROC  , configurationClass.getColorEOInProcess());
				values.put(KEY_CONCOLEOREJ  , configurationClass.getColorEOReject());
				values.put(KEY_CONOBL  , configurationClass.getObligatory());
				values.put(KEY_CONCOLOEFIN  , configurationClass.getColorEOFinalized());
				values.put(KEY_CONSATVALLOW  , configurationClass.getSftcnLow());
				values.put(KEY_CONCOLOTOTH  , configurationClass.getColorOTOther());
				values.put(KEY_CONAUT  , configurationClass.getAutomatic());
				db.insert(TABLE_APP_CONFIGURATION, null, values);
				db.close();
			}
			public String getVal(String val) {
				String result = "";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor mCount= db.rawQuery("SELECT "+ val +" FROM " + TABLE_APP_CONFIGURATION + " LIMIT 1",null);
				mCount.moveToFirst();
				result= mCount.getString(0);
				db.close();
				return result;
			}
			public ArrayList<ConfigurationClass> getConfigurationData() {
				ArrayList<ConfigurationClass> userList = new ArrayList<ConfigurationClass>();
				String selectQuery = "SELECT * FROM " + TABLE_APP_CONFIGURATION + " LIMIT 1";
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						ConfigurationClass config = new ConfigurationClass();
						config.setId(cursor.getInt(cursor.getColumnIndex(KEY_CONID)));
						config.setColorEONotStarting(cursor.getString(cursor.getColumnIndex(KEY_CONCOLEOSTAR )));
						config.setGeoDistance(cursor.getString(cursor.getColumnIndex(KEY_CONGEODIS )));
						config.setColorOTEmergency(cursor.getString(cursor.getColumnIndex(KEY_CONCOLEMER )));
						config.setSftcnHigh(cursor.getString(cursor.getColumnIndex(KEY_CONSATVALHIG )));
						config.setNotification(cursor.getString(cursor.getColumnIndex(KEY_CONNOT )));
						config.setStfcnQstnManager(cursor.getString(cursor.getColumnIndex(KEY_CONSATQUESUP )));
						config.setColorOTRutine(cursor.getString(cursor.getColumnIndex(KEY_CONCOLOTRUT )));
						config.setColorEOInProcess(cursor.getString(cursor.getColumnIndex(KEY_CONCOLEOPROC )));
						config.setColorEOReject(cursor.getString(cursor.getColumnIndex(KEY_CONCOLEOREJ )));
						config.setObligatory(cursor.getString(cursor.getColumnIndex(KEY_CONOBL )));
						config.setColorEOFinalized(cursor.getString(cursor.getColumnIndex(KEY_CONCOLOEFIN )));
						config.setSftcnLow(cursor.getString(cursor.getColumnIndex(KEY_CONSATVALLOW )));						config.setId(cursor.getInt(cursor.getColumnIndex(KEY_CONSATVALLOW )));
						config.setColorOTOther(cursor.getString(cursor.getColumnIndex(KEY_CONCOLOTOTH  )));
						config.setAutomatic(cursor.getString(cursor.getColumnIndex(KEY_CONAUT  )));
						userList.add(config);
					} while (cursor.moveToNext());
				}
				db.close();
				return userList;
			}

		}
		public class AppNotificationSql{
			public AppNotificationSql(){}
			public void addNotification(NotificationClass notificationClass){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				values.put(KEY_NOTIFID , notificationClass.getId());
				values.put(KEY_NOTIFDAT,notificationClass.getDATE());
				values.put(KEY_NOTIFORDNUM , notificationClass.getAUFNR());
				values.put(KEY_NOTIFOBJID , notificationClass.getOBJID());
				values.put(KEY_NOTIFTIM, notificationClass.getTIME());
				db.insert(TABLE_APP_NOTIFICATION, null, values);
				db.close();
			}
			public void deleteNotification (String orderNumber){
				String whereClause;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				whereClause = "AUFNR = '" + orderNumber + "'";
				db.delete(TABLE_APP_NOTIFICATION, whereClause,null);
				db.close();
			}
			public ArrayList<NotificationClass> getAllItem() {
				ArrayList<NotificationClass> notificationClassList = new ArrayList<NotificationClass>();
				String selectQuery = "SELECT  * FROM " + TABLE_APP_NOTIFICATION ;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						NotificationClass item = new NotificationClass();
						item.setId(cursor.getInt((cursor.getColumnIndex(KEY_NOTIFID))));
						item.setDATE(cursor.getString(cursor.getColumnIndex(KEY_NOTIFDAT)));
						item.setAUFNR(cursor.getString(cursor.getColumnIndex(KEY_NOTIFORDNUM)));
						item.setOBJID(cursor.getString(cursor.getColumnIndex(KEY_NOTIFOBJID)));
						item.setTIME(cursor.getString(cursor.getColumnIndex(KEY_NOTIFTIM)));
						notificationClassList.add(item);
					} while (cursor.moveToNext());
				}
				db.close();
				return notificationClassList;
			}
		}

		public class AppSynchronizationSql {
			public AppSynchronizationSql() {}

			public void deleteSynchronization(){
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				db.delete(TABLE_APP_SYNCHRONIZATION,null,null);
				db.close();
			}

			public void addSynchronization(SynchronizationClass synchronizationClass) {

				SQLiteDatabase db = databasehelp.getWritableDatabase();
				ContentValues values = new ContentValues();
				//values.put(KEY_SYNID, synchronizationClass.getId());
				values.put(KEY_SYNCLANAM, synchronizationClass.getClassName());
				values.put(KEY_SYNDATHOUSTA, synchronizationClass.getDateHourStart());
				values.put(KEY_SYNDATHOUEND, synchronizationClass.getDateHourEnd());
				values.put(KEY_SYNREGNUM, synchronizationClass.getRegisterNumber());
				values.put(KEY_SYNFAINUM, synchronizationClass.getFailureNumber());
				values.put(KEY_SYNSTA, synchronizationClass.getState());

				db.insert(TABLE_APP_SYNCHRONIZATION, null, values);
				db.close();
			}


			public ArrayList<SynchronizationClass> getSynchronizationData() {
				ArrayList<SynchronizationClass> synchronizationClassList = new ArrayList<SynchronizationClass>();
				String selectQuery = "SELECT * FROM " + TABLE_APP_SYNCHRONIZATION;
				SQLiteDatabase db = databasehelp.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						SynchronizationClass synchronizationClass = new SynchronizationClass();
						synchronizationClass.setId(cursor.getInt(cursor.getColumnIndex(KEY_SYNID)));
						synchronizationClass.setClassName(cursor.getString(cursor.getColumnIndex(KEY_SYNCLANAM)));
						synchronizationClass.setDateHourStart(cursor.getString(cursor.getColumnIndex(KEY_SYNDATHOUSTA)));
						synchronizationClass.setDateHourEnd(cursor.getString(cursor.getColumnIndex(KEY_SYNDATHOUEND)));
						synchronizationClass.setRegisterNumber(cursor.getString(cursor.getColumnIndex(KEY_SYNREGNUM)));
						synchronizationClass.setFailureNumber(cursor.getString(cursor.getColumnIndex(KEY_SYNFAINUM)));
						synchronizationClass.setState(cursor.getString(cursor.getColumnIndex(KEY_SYNSTA)));

						synchronizationClassList.add(synchronizationClass);
					} while (cursor.moveToNext());
				}
				db.close();
				return synchronizationClassList;
			}



			public SynchronizationClass getSynchronization(String nId) {
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_SYNCHRONIZATION, new String[] {KEY_SYNID, KEY_SYNCLANAM, KEY_SYNDATHOUSTA, KEY_SYNDATHOUEND,
								KEY_SYNREGNUM, KEY_SYNFAINUM, KEY_SYNSTA}, KEY_SYNID + "=?",
						new String[] { String.valueOf(nId) }, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}

				SynchronizationClass synchronizationClass = new SynchronizationClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
						cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
				db.close();
				return synchronizationClass;
			}

			public String getData(int nField, String sName) {
				String _data = null;
				SQLiteDatabase db = databasehelp.getReadableDatabase();
				Cursor cursor = db.query(TABLE_APP_SYNCHRONIZATION, new String[] {KEY_SYNID, KEY_SYNCLANAM, KEY_SYNDATHOUSTA, KEY_SYNDATHOUEND,
								KEY_SYNREGNUM, KEY_SYNFAINUM, KEY_SYNSTA}, KEY_SYNID + "=?",
						new String[] {sName}, null, null, null, null);
				if (cursor != null){
					cursor.moveToFirst();
				}
				_data = cursor.getString(nField);
				db.close();
				return _data;
			}

		}


	}
}
