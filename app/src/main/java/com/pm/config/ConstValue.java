package com.pm.config;

import java.util.Calendar;
import java.util.Date;


public class ConstValue {

    public static String SITE_URL = "http://52.41.46.242:443/api/main/mobility";//PRD
//    public static String SITE_URL = "http://34.216.148.9:443/api/main/mobility";//QAS
//    public static String SITE_URL = "http://34.216.148.9:8080/api/main/mobility";//QAS 23/03/2020


    public static String JSON_LOGIN = SITE_URL + "/get/user/";
    public static String JSON_UPDATE = SITE_URL+"/userapp/password/";
    /* MASTER TICKET */
    public static String JSON_EQUIPMENT = SITE_URL + "/get/equipment/";
    public static String JSON_NOTICETYPE = SITE_URL +"/get/notice_type/";
    public static String JSON_TECHNICALLOCATION = SITE_URL +"/get/location/";
    public static String JSON_SYMPTOM = SITE_URL +"/get/symptom/";
    /* MASTER OT */
    public static String JSON_OTHEAD = SITE_URL+"/get/othead/";
    public static String JSON_OTCOMPONENT = SITE_URL+"/get/otcomponent/";
    public static String JSON_OTOPERATIONS = SITE_URL+"/get/otoperations/";
    public static String JSON_OTMEASURES = SITE_URL+"/get/otmeasures/";
    public static String JSON_GTICKET = SITE_URL +"/get/ticket/";
    /* SEND DATA */
    public static String JSON_TICKET = SITE_URL +"/set/ticket/";
    public static String JSON_OT = SITE_URL +"/set/ot/";
    public static String JSON_OT_POST = SITE_URL +"/set/otupdate/";
    public static String JSON_GPS = SITE_URL + "/set/map/";
    public static String JSON_INCIDENCE = SITE_URL + "/set/incidence/";
    /* NOTIFICATION*/
    public static String JSON_NOTIFICATION_GET = SITE_URL + "/get/notification/";
    /* CONFIGURATION*/
    public static String JSON_CONFIGURATION = SITE_URL + "/get/setting/";

    //@Settings
    public static String fragmentView;
    public static void setFragmentView(String _fragmentView){ fragmentView = _fragmentView;  }
    public static String getFragmentView(){
        return fragmentView;
    }

    public static int userView;
    public static void setUserView(int _userView){ userView = _userView;  }
    public static int getUserView(){ return userView; }

    public static String colorEmergency;
    public static void setColorEmergencyString(String _colorEmergency){ colorEmergency = _colorEmergency;  }
    public static String getColorEmergencyString(){
        return colorEmergency;
    }

    public static String colorRoutine;
    public static void setColorRoutineString(String _colorRoutine){ colorRoutine = _colorRoutine; }
    public static String getColorRoutineString(){
        return colorRoutine;
    }

    public static String colorOther;
    public static void setColorOtherString(String _colorOther){ colorOther = _colorOther; }
    public static String getColorOtherString(){ return colorOther; }


    public static String colorPending;
    public static void setColorPendingString(String _colorPending){ colorPending = _colorPending;  }
    public static String getColorPendingString(){
        return colorPending;
    }

    public static String colorProcess;
    public static void setColorProcessString(String _colorProcess){ colorProcess = _colorProcess; }
    public static String getColorProcessString(){
        return colorProcess;
    }

    public static String colorFinish;
    public static void setColorFinishString(String _colorFinish){ colorFinish = _colorFinish; }
    public static String getColorFinishString(){
        return colorFinish;
    }

    public static String colorRefuse;
    public static void setColorRefuseString(String _colorRefuse){ colorRefuse = _colorRefuse; }
    public static String getColorRefuseString(){
        return colorRefuse;
    }

    //@GeneralUser
    public static String sendCode;
    public static void setSendCodeString(String _sendCode){ sendCode = _sendCode; }
    public static String getSendCodeString(){ return sendCode; }

    public static int userId;
    public static void setUserIdInt(int _userId){
        userId = _userId;
    }
    public static int getUserIdInt(){
        return userId;
    }

    public static String userUser;
    public static void setUserUserString(String _userUser){
        userUser = _userUser;
    }
    public static String getUserUserString(){
        return userUser;
    }

    public static String userCode;
    public static void setUserCodeString(String _userCode){
        userCode = _userCode;
    }
    public static String getUserCodeString(){
        return userCode;
    }

    public static String userType;
    public static void setUserTypeString(String _userType){
        userType = _userType;
    }
    public static String getUserTypeString(){
        return userType;
    }

    public static String userCenter;
    public static void setUserCenterString(String _userCenter){userCenter = _userCenter;}
    public static String getUserCenterString(){ return userCenter; }

    //@GeneralOrder
    public static int orderId;
    public static void setOrderIdInt(int _orderId){ orderId = _orderId; } public static int getOrderIdInt(){ return orderId; }

    public static String orderPriority;
    public static void setOrderPriority(String _orderPriority){ orderPriority = _orderPriority; }	public static String getOrderPriority(){ return orderPriority; }

    public static String orderNPriority;
    public static void setOrderNPriority(String _orderNPriority){ orderNPriority = _orderNPriority; }	public static String getOrderNPriority(){ return orderNPriority; }

    public static String orderNumber;
    public static void setOrderNumberString(String _orderNumber){ orderNumber = _orderNumber; }	public static String getOrderNumberString(){ return orderNumber; }

    public static String orderTechnical;
    public static void setOrderTechnicalString(String _orderTechnical){ orderTechnical = _orderTechnical; } public static String getOrderTechnicalString(){ return orderTechnical; }

    public static String orderCenter;
    public static void setOrderCenterString(String _orderCenter){ orderCenter = _orderCenter; } public static String getOrderCenterString(){ return orderCenter; }

    public static String orderText;
    public static void setOrderText(String _orderText) { orderText = _orderText; } public static String getOrderText() { return orderText; }

    public static String orderAdvance;
    public static void setOrderAdvance(String _orderAdvance) { orderAdvance = _orderAdvance; } public static String getOrderAdvance() { return orderAdvance; }

    //@Hour Date
    public static String hourStart;
    public static String getHourStart() { return hourStart; }
    public static void setHourStart(String hourStart) { ConstValue.hourStart = hourStart; }

    public static String hourEnd;
    public static String getHourEnd() { return hourEnd; }
    public static void setHourEnd(String hourEnd) { ConstValue.hourEnd = hourEnd; }

    public static String dateStart;
    public static String getDateStart() { return dateStart; }
    public static void setDateStart(String dateStart) { ConstValue.dateStart = dateStart; }

    public static String dateEnd;
    public static String getDateEnd() { return dateEnd; }
    public static void setDateEnd(String dateEnd) { ConstValue.dateEnd = dateEnd; }


    private Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    //@Ticket
    public static String rbnr;
    public static String getRbnr() { return rbnr; }public static void setRbnr(String rbnr) { ConstValue.rbnr = rbnr; }

    public static String numberTicket;
    public static String getNumberTicket() { return numberTicket; }public static void setNumberTicket(String numberTicket) { ConstValue.numberTicket = numberTicket; }

    public static String typeTicket;
    public static String getTypeTicket() { return typeTicket; }public static void setTypeTicket(String typeTicket) { ConstValue.typeTicket = typeTicket; }

    public static String tnotice;
    public static String getTnotice() { return tnotice; }public static void setTnotice(String tnotice) { ConstValue.tnotice = tnotice; }

    public static String locationTicket;
    public static String getLocationTicket() { return locationTicket; }public static void setLocationTicket(String locationTicket) { ConstValue.locationTicket = locationTicket; }

    public static String equipmentTicket;
    public static String getEquipmentTicket() { return equipmentTicket; }public static void setEquipmentTicket(String equipmentTicket) { ConstValue.equipmentTicket = equipmentTicket; }

    public static String dateTicket;
    public static String getDateTicket() { return dateTicket; }public static void setDateTicket(String dateTicket) { ConstValue.dateTicket = dateTicket; }

    public static String symptomTicket;
    public static String getSymptomTicket() { return symptomTicket; }public static void setSymptomTicket(String symptomTicket) { ConstValue.symptomTicket = symptomTicket; }

    public static String aproval;
    public static String getAproval() { return aproval; }public static void setAproval(String aproval) { ConstValue.aproval = aproval; }

    public static String reason;
    public static String getReason() { return reason; }public static void setReason(String reason) { ConstValue.reason = reason; }

    public static String LoadTicket;public static String getLoadTicket() { return LoadTicket; }public static void setLoadTicket(String loadTicket) { LoadTicket = loadTicket; }

    /** Sync Values */
    public static String checkedEquipments;
    public static String getCheckedEquipments() { return checkedEquipments; } public static void setCheckedEquipments(String checkedEquipments) {ConstValue.checkedEquipments = checkedEquipments; }

    public static String checkedNoticeType;
    public static String getCheckedNoticeType() { return checkedNoticeType; } public static void setCheckedNoticeType(String checkedNoticeType) { ConstValue.checkedNoticeType = checkedNoticeType; }

    public static String checkedSymptom;
    public static String getCheckedSymptom() { return checkedSymptom; } public static void setCheckedSymptom(String checkedSymptom) { ConstValue.checkedSymptom = checkedSymptom; }

    public static String checkedTechnicaLocation;
    public static String getCheckedTechnicaLocation() { return checkedTechnicaLocation; } public static void setCheckedTechnicaLocation(String checkedTechnicaLocation) { ConstValue.checkedTechnicaLocation = checkedTechnicaLocation; }

    public static String MAIN_PREF = "CloudPM";
}
