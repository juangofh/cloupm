package com.pm.views.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pm.R;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.models.EquipmentClass;
import com.pm.models.NoticeTypeClass;
import com.pm.models.SupportTicketClass;
import com.pm.models.SymptomClass;
import com.pm.models.TechnicalLocationsClass;
import com.pm.utils.Common;
import com.pm.utils.ConnectionDetector;
import com.pm.utils.Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TicketActivity extends AppCompatActivity {
    Context context;
    String noticeDate, noticeHour;
    ArrayList<String> noticeTypeList;
    ArrayList<NoticeTypeClass> notTypList;
    Spinner spinnerNoticeType; Boolean spinnerNoticeTypeInitialized = true; String QMART, NQMART;
    ArrayList<String> technicalLocationList;
    ArrayList<TechnicalLocationsClass> tecLocList;
    Spinner spinnerTechnicalLocation; Boolean spinnerTechnicalLocationInitialized = true; String TPLNR, NTPLNR;
    ArrayList<String> equipmentList;
    ArrayList<EquipmentClass> equList;
    Spinner spinnerEquipment; Boolean spinnerEquipmentInitialized = true; String EQUNR, NEQUNR, RBNR;
    ArrayList<String> symptomList;
    ArrayList<SymptomClass> symList;
    Spinner spinnerSymptom; Boolean spinnerSymptomInitialized = true; String CODEGRUPPE, CODE, NCODE;
    Boolean fin = false;

    private ArrayList<SupportTicketClass> loadTicket;
    ProgressDialog ticketDialog;
    SupportTicketClass sendTicket = new SupportTicketClass();
    Common common;
    public ConnectionDetector cd;

    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Generar Ticket");
        context = this;
        ConstValue.setFragmentView("Ticket");
        cd=new ConnectionDetector(TicketActivity.this);
        spinnerNoticeType = (Spinner) findViewById(R.id.sp_notice_type);
        spinnerTechnicalLocation = (Spinner) findViewById(R.id.sp_location); spinnerTechnicalLocation.setVisibility(View.GONE);
        spinnerEquipment = (Spinner) findViewById(R.id.sp_team); spinnerEquipment.setVisibility(View.GONE);
        spinnerSymptom = (Spinner) findViewById(R.id.sp_symptom_symptomt); spinnerSymptom.setVisibility(View.GONE);

        SimpleDateFormat dateFormatH = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        Date dateH = new Date(); noticeHour = dateFormatH.format(dateH);
        SimpleDateFormat dateFormatD = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date dateD = new Date(); noticeDate = dateFormatD.format(dateD);

        //NOTICE_TYPE
        notTypList = new ArrayList<NoticeTypeClass>();
        notTypList = SqliteClass.getInstance(context).databasehelp.noticeTypeSql.getAllItem();
        noticeTypeList = new ArrayList<String>();
        if (notTypList.size() > 0) {
            for (int j = 0; j < notTypList.size(); j++) {
                noticeTypeList.add(notTypList.get(j).getDescription());
            }
            ArrayAdapter<String> adapterNoticeType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, noticeTypeList);
            adapterNoticeType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerNoticeType.setAdapter(adapterNoticeType);
            spinnerNoticeType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> spn, android.view.View v, int position, long id) {
                    if (spinnerNoticeTypeInitialized) {
                        spinnerNoticeTypeInitialized = false;
                        fin = false;
                        return;
                    }else{
                        QMART= notTypList.get(position).getCode();
                        NQMART = notTypList.get(position).getDescription();
                        TPLNR= ""; NTPLNR = "";
                        EQUNR= ""; NEQUNR = ""; RBNR = "";
                        CODEGRUPPE = ""; CODE = ""; NCODE = "";
                        spinnerTechnicalLocation.setVisibility(View.VISIBLE);
                    }
                }
                public void onNothingSelected(AdapterView<?> spn) {
                    return;
                }
            });
            // LOCATION
            technicalLocationList = new ArrayList<String>();
            tecLocList = new ArrayList<TechnicalLocationsClass>();
            tecLocList = SqliteClass.getInstance(context).databasehelp.technicalLocationsSql.getAllItem();

                for (int j = 0; j < tecLocList.size(); j++) {
                    technicalLocationList.add(tecLocList.get(j).getDescriptionLocation());
                }
                ArrayAdapter<String> adapterTechnicalLocation = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, technicalLocationList);
                adapterTechnicalLocation.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerTechnicalLocation.setAdapter(adapterTechnicalLocation);
                spinnerTechnicalLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> spn, android.view.View v, int position, long id) {
                        if (spinnerTechnicalLocationInitialized) {
                            spinnerTechnicalLocationInitialized = false;
                            fin = false;
                            return;
                        }else{
                            TPLNR= tecLocList.get(position).getTechnicalLocationCode();
                            NTPLNR = tecLocList.get(position).getDescriptionLocation();
                            EQUNR= ""; NEQUNR = ""; RBNR = "";
                            CODEGRUPPE = ""; CODE = ""; NCODE = "";
                            spinnerEquipment.setVisibility(View.VISIBLE);
                            spEquiment();
                        }
                    }
                    public void onNothingSelected(AdapterView<?> spn) {
                        return;
                    }
                });

            }else{
                Toast.makeText(getApplicationContext(), "CloudPM: No se registró ubicación." , Toast.LENGTH_LONG).show();
                spinnerEquipment.setVisibility(View.GONE);
                spinnerSymptom.setVisibility(View.GONE);
            }
        if (tecLocList.size() > 0) {} else {
            Toast.makeText(getApplicationContext(), "CloudPM: No se registró tipos de aviso." , Toast.LENGTH_LONG).show();
            spinnerTechnicalLocation.setVisibility(View.GONE);
            spinnerEquipment.setVisibility(View.GONE);
            spinnerSymptom.setVisibility(View.GONE);
        }
    }

    public void spEquiment(){
        // EQUIPMENT
        equipmentList = new ArrayList<String>();
        equList = new ArrayList<EquipmentClass>();
        equList = SqliteClass.getInstance(context).databasehelp.equipmentSql.getEquipmentLocation(TPLNR);

            for (int j = 0; j < equList.size(); j++) {
                equipmentList.add(equList.get(j).getDescription());
            }
            ArrayAdapter<String> adapterEquipment = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, equipmentList);
            adapterEquipment.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerEquipment.setAdapter(adapterEquipment);
            spinnerEquipment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> spn, android.view.View v, int position, long id) {
                    if (spinnerEquipmentInitialized) {
                        spinnerEquipmentInitialized = false;
                        fin = false;
                        return;
                    }else{
                        try{
                            EQUNR= equList.get(position).getCode();
                            NEQUNR = equList.get(position).getDescription();
                            RBNR = equList.get(position).getCatalogProfile();
                            CODEGRUPPE = ""; CODE = ""; NCODE = "";
                            spinnerSymptom.setVisibility(View.VISIBLE);
                            spSymptom();
                        }catch (Exception e){

                        }
                    }
                }
                public void onNothingSelected(AdapterView<?> spn) {
                    return;
                }
            });
        if (equList.size() > 0) {}else{
            if (spinnerEquipmentInitialized) {}else{
                Toast.makeText(getApplicationContext(), "CloudPM: No se registró equipo." , Toast.LENGTH_LONG).show();
                spinnerSymptom.setVisibility(View.GONE);
            }
        }
    }

    public void spSymptom(){
        // SINTOMAS
        symptomList = new ArrayList<String>();
        symList = new ArrayList<SymptomClass>();
        symList = SqliteClass.getInstance(context).databasehelp.symptomSql.getSymptomProfile(RBNR);

        for (int j = 0; j < symList.size(); j++) {
            symptomList.add(symList.get(j).getDescription());
        }
        ArrayAdapter<String> adapterSymptom = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, symptomList);
        adapterSymptom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSymptom.setAdapter(adapterSymptom);
        spinnerSymptom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> spn, android.view.View v, int position, long id) {
                if (spinnerSymptomInitialized) {
                    spinnerSymptomInitialized = false;
                    fin = false;
                    return;
                }else{
                    fin = true;
                    CODEGRUPPE = symList.get(position).getGroupCode() ;
                    CODE = symList.get(position).getSymptomCode();
                    NCODE = symList.get(position).getDescription();
                }
            }
            public void onNothingSelected(AdapterView<?> spn) {
                return;
            }
        });
        if (symList.size() > 0) {}else{
            if (spinnerSymptomInitialized) {}else{
                Toast.makeText(getApplicationContext(), "CloudPM: No se registró sintomas." , Toast.LENGTH_LONG).show();
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ticket_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            Intent intent = new Intent(TicketActivity.this,MainActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_send_ticket_menu) {
            if(!fin){
                Toast.makeText(getApplicationContext(), "Por favor seleccione un síntoma.", Toast.LENGTH_LONG).show();
            }else {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_ticket);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_send);
                image.setImageResource(R.drawable.ic_alert_send);
                TextView head = (TextView) dialog.findViewById(R.id.alert_ticket_title);
                head.setText("CloudPM - TICKET");
                TextView content = (TextView) dialog.findViewById(R.id.alert_ticket_content);
                content.setText("¿Seguro de enviar el ticket?");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        sendTicket = new SupportTicketClass();
                        sendTicket.setId(SqliteClass.getInstance(context).databasehelp.supportTicketSql.getAllItem().size());
                        sendTicket.setTicketNumber(ConstValue.getUserUserString() + Util.getRandomNumberString());
                        sendTicket.setNoticeType(QMART);
                        sendTicket.setNnoticeType(NQMART);
                        sendTicket.setCenter(ConstValue.getUserCenterString());
                        sendTicket.setLocation(TPLNR);
                        sendTicket.setNlocation(NTPLNR);
                        sendTicket.setEquipment(EQUNR);
                        sendTicket.setNequipment(NEQUNR);
                        sendTicket.setNoticeDate(noticeDate);
                        sendTicket.setNoticeHour(noticeHour);
                        sendTicket.setSymptomGroupCode(CODEGRUPPE);
                        sendTicket.setSymptomGroupSymptom(CODE);
                        sendTicket.setNsymptomGroupSymptom(NCODE);
                        sendTicket.setCauseText("");
                        sendTicket.setStoppedEquipment("");
                        sendTicket.setApproval("0");
                        sendTicket.setLoad("0");

                        if(cd.isConnectingToInternet()){
                            new ticketTask().execute(true);
                        }else{
                            Toast.makeText(context, getString(R.string.app_no_action), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
                Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        Util.logout(TicketActivity.this, context, "1");
    }

    class ticketTask extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            ticketDialog = ProgressDialog.show(context, "CloudPM", "Procesando. Por favor espere..", true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (!result.equals("")) {
                Util.alertInfo(context, "CloudPM", result);
                //Toast.makeText(getApplicationContext(), "CloudPM: " + result, Toast.LENGTH_LONG).show();
            } else {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_info);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("CloudPM - SINCRONIZAR");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("El ticket se generó con éxito.");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(context, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                dialog.show();
            }
            // TODO Auto-generated method stub
            ticketDialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }


        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = "";
            try {
                //loadTicket = new ArrayList<SupportTicketClass>();
                //loadTicket = SqliteClass.getInstance(context).databasehelp.supportTicketSql.getLoadPendingItem();
                JSONObject jsonobj;
                //for (int z = 0; z < loadTicket.size(); z++) {
                    common = new Common();
                    jsonobj = new JSONObject();
                    jsonobj.put("TICNR", ConstValue.getUserUserString());
                    jsonobj.put("QMART", sendTicket.getNoticeType());
                    jsonobj.put("NQMART", sendTicket.getNnoticeType());
                    jsonobj.put("WERKS", sendTicket.getCenter());
                    jsonobj.put("TPLNR", sendTicket.getLocation());
                    jsonobj.put("NTPLNR", sendTicket.getNlocation());
                    jsonobj.put("EQUNR", sendTicket.getEquipment());
                    jsonobj.put("NEQUNR", sendTicket.getNequipment());
                    jsonobj.put("NDATE", sendTicket.getNoticeDate());
                    jsonobj.put("NTIME", sendTicket.getNoticeHour());
                    jsonobj.put("CODEGRUPPE", sendTicket.getSymptomGroupCode());
                    jsonobj.put("CODE", sendTicket.getSymptomGroupSymptom());
                    jsonobj.put("NCODE", sendTicket.getNsymptomGroupSymptom());
                    jsonobj.put("STOPPED", sendTicket.getStoppedEquipment());

                    List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                    nvps.add(new BasicNameValuePair("data", jsonobj.toString()));
                    JSONObject json = common.sendJsonData(ConstValue.JSON_TICKET, nvps);

                    if (json.getString("response").equalsIgnoreCase("success")) {
                        SqliteClass.getInstance(context).databasehelp.supportTicketSql.addSupportTicket(sendTicket); //add ticket if it was accepted
                        SqliteClass.getInstance(context).databasehelp.supportTicketSql.updateLoadSupportTicket(sendTicket.getId(), "1");
                    }
                    else {
                        responseString = json.getString("response");
                    }
                //}
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return responseString;
        }
    }
}