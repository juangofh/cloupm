package com.pm.views.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.pm.R;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.models.SupportTicketClass;
import com.pm.utils.Util;

public class TicketDActivity extends AppCompatActivity {
    Context context;
    TextView equipment,center,location,date,symptom,aproval,reason,ticketNumber,noticeType;
    SupportTicketClass ticket =  new SupportTicketClass();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_d);
        context=this;

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Información de Ticket");

        ticketNumber = (TextView) findViewById(R.id.tv_ticketNumber);
        ticketNumber.setText(ConstValue.getNumberTicket());

        noticeType = (TextView) findViewById(R.id.tv_typeNotice);
        noticeType.setText(ConstValue.getTnotice());

        equipment = (TextView) findViewById(R.id.tv_equipment_ticket);
        equipment.setText(ConstValue.getEquipmentTicket());

        center = (TextView) findViewById(R.id.tv_center_ticket);
        center.setText(ConstValue.getUserCenterString());

        location = (TextView) findViewById(R.id.tv_location_ticket);
        location.setText(ConstValue.getLocationTicket());

        date = (TextView) findViewById(R.id.tv_date_ticket);
        date.setText(ConstValue.getDateTicket());

        symptom = (TextView) findViewById(R.id.tv_symptom);
        symptom.setText(ConstValue.getSymptomTicket());

        reason = (TextView) findViewById(R.id.tv_reason);
        reason.setText(ConstValue.getReason());

        aproval = (TextView) findViewById(R.id.tv_aproval);
        if(ConstValue.getAproval().equals("0")){
            aproval.setText("Sin Aprobar");
        }else if(ConstValue.getAproval().equals("1")){
            aproval.setText("Aprobado");
        }else {
            aproval.setText("Rechazado");
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent intent = new Intent(TicketDActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }else if(id==R.id.action_logout){
            Util.logout(TicketDActivity.this, context, "0");
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        Util.logout(TicketDActivity.this, context, "1");
    }
}
