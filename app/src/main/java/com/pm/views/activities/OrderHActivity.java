package com.pm.views.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pm.R;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.models.OTClass;
import com.pm.models.OTComponentsClass;
import com.pm.models.OTMeasurementPointClass;
import com.pm.models.OTOperationsClass;
import com.pm.utils.Common;
import com.pm.utils.ConnectionDetector;
import com.pm.utils.GpsTracker;
import com.pm.utils.Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class OrderHActivity extends AppCompatActivity {

    FloatingActionButton fabOrderClose, fabOrderDetail, fabOrderObservation;
    TextView tvOrderClose, tvOrderDetail, tvOrderObservation;
    FrameLayout flOrderClose, flOrderDetail, flOrderObservation;
    TextView orderNumber,orderText;
    TextView notification, planificationGroup, responsable, tvdate, centerDistribution, ubication, equipment;
    ImageView iv_priority;
    View convertView;
    Context context;
    String hourStart, dateStart, hourEnd, dateEnd;
    List<OTClass> itemList;
    boolean isFABOpen = false;
    Common common;
    ArrayList<OTClass> otLoad; ArrayList<OTClass> otLoadClosing;
    ArrayList<OTComponentsClass>otcomponentLoad;
    ArrayList<OTMeasurementPointClass>otmeasurementLoad;
    ArrayList<OTOperationsClass>otoperationtLoad;
    ProgressDialog dialogOTClosing;
    ProgressDialog dialogOTClosingUpdate;
    ProgressDialog dialogLocation;
    public ConnectionDetector cd;
    private GpsTracker gpsTracker; public double latitude; public double longitude;
    public String valStfc ="";

    LocationManager mLocationManager; /* Change fot taking positions */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_h);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context= this;
        cd=new ConnectionDetector(OrderHActivity.this);
        orderNumber = (TextView) findViewById(R.id.orderNumber);
        orderNumber.setText("Nro. " + ConstValue.getOrderNumberString());
        orderText=(TextView) findViewById(R.id.tv_orderText);
        notification=(TextView) findViewById(R.id.tv_notification);
        planificationGroup=(TextView) findViewById(R.id.tv_planificationGroup);
        responsable=(TextView) findViewById(R.id.tv_responsable);
        tvdate=(TextView) findViewById(R.id.tv_date);
        centerDistribution=(TextView) findViewById(R.id.tv_centerDistribution);
        ubication=(TextView) findViewById(R.id.tv_ubication);
        equipment=(TextView) findViewById(R.id.tv_equipment);

        iv_priority = (ImageView) findViewById(R.id.iv_priority);
        itemList = new ArrayList<OTClass>();
        itemList = SqliteClass.getInstance(context).databasehelp.otsql.getOrderNumber(ConstValue.getOrderNumberString());
        gpsTracker = new GpsTracker(OrderHActivity.this);
        if(gpsTracker.canGetLocation()){
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
        }else{
            gpsTracker.showSettingsAlert();
        }
        for(int z=0; z < itemList.size(); z++){
            OTClass cc = itemList.get(z);
            ConstValue.setOrderIdInt(cc.getId());
            ConstValue.setOrderText(cc.getText());
            ConstValue.setOrderPriority(cc.getPriority());
            ConstValue.setOrderNPriority(cc.getPriorityName());
            ConstValue.setOrderTechnicalString(cc.getObjID());
            ConstValue.setOrderCenterString(cc.getJobCenter());
            notification.setText(cc.getOrderReference());
            planificationGroup.setText(cc.getPlanningGroup() + " - " + cc.getPlanningGroupName());
            responsable.setText(cc.getJobTitle() +" - "+ cc.getJobTitleName());
            tvdate.setText(cc.getExtremeStartDate() +" - "+ cc.getExtremeEndDate());
            centerDistribution.setText(cc.getJobCenter() + " - " + cc.getJobCenterName());
            ubication.setText(cc.getSiteCenter() + " - " + cc.getSiteCenterName());
            equipment.setText(cc.getEquipment() + " - " + cc.getEquipmentName());

            int total = SqliteClass.getInstance(context).databasehelp.otsql.countOrderStatus(cc.getNumberOrder(),"ALL");
            int finish = SqliteClass.getInstance(context).databasehelp.otsql.countOrderStatus(cc.getNumberOrder(),"F");
            if(finish==0){
                ConstValue.setOrderAdvance("0");
            }else if (total==finish){
                ConstValue.setOrderAdvance("100");
            }else{
                float result = (finish*100)/total;
                String _advance = String.format("%.2f", result);
                ConstValue.setOrderAdvance(_advance);
            }
            if(cc.getPriority().equals("1")){
                iv_priority.setImageResource(R.drawable.ic_priority_very_high);
                Toast.makeText(this,cc.getPriorityName(),Toast.LENGTH_SHORT).show();
            }else if(cc.getPriority().equals("2")){
                iv_priority.setImageResource(R.drawable.ic_priority_high);
                Toast.makeText(this,cc.getPriorityName(),Toast.LENGTH_SHORT).show();
            }else if(cc.getPriority().equals("3")){
                iv_priority.setImageResource(R.drawable.ic_priority_medium);
                Toast.makeText(this,cc.getPriorityName(),Toast.LENGTH_SHORT).show();
            }else{
                iv_priority.setImageResource(R.drawable.ic_priority_low);
                Toast.makeText(this,cc.getPriorityName(),Toast.LENGTH_SHORT).show();
            }
        }
        orderText.setText(ConstValue.getOrderText());
        iv_priority.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(OrderHActivity.this,ConstValue.getOrderNPriority(),Toast.LENGTH_SHORT).show();
            }
        });


        /****************************************************************/
        final FloatingActionButton fabEvent = (FloatingActionButton) findViewById(R.id.action_event);
        tvOrderClose = (TextView) findViewById(R.id.text_order_close); tvOrderClose.setVisibility(View.GONE);
        flOrderClose = (FrameLayout) findViewById(R.id.frame_order_close);
        tvOrderDetail = (TextView) findViewById(R.id.text_order_detail); tvOrderDetail.setVisibility(View.GONE);
        flOrderDetail = (FrameLayout) findViewById(R.id.frame_order_detail);
        tvOrderObservation = (TextView) findViewById(R.id.text_order_observation);
        tvOrderObservation.setVisibility(View.GONE);
        if(ConstValue.getUserTypeString().equals("C")){ tvOrderObservation.setText("Observación Técnico"); }
        flOrderObservation = (FrameLayout) findViewById(R.id.frame_order_observation);
        fabOrderClose = (FloatingActionButton) findViewById(R.id.action_order_close);
        fabOrderDetail = (FloatingActionButton) findViewById(R.id.action_order_detail);
        fabOrderObservation = (FloatingActionButton) findViewById(R.id.action_order_observation);
        fabEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }
        });

        //OBSERVACION
        fabOrderObservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_ot);
                TextView content = (TextView) dialog.findViewById(R.id.alert_rtn_content);
                content.setText("CloudPM - OT " + ConstValue.getOrderNumberString());
                final EditText _name = (EditText) dialog.findViewById(R.id.etObservation);
                Button dialogOk = (Button) dialog.findViewById(R.id.alert_ok);
                String _obs = SqliteClass.getInstance(context).databasehelp.otsql.getOTInfo(ConstValue.getOrderNumberString(), "OBSH");
                String _status = SqliteClass.getInstance(context).databasehelp.otsql.getOTInfo(ConstValue.getOrderNumberString(), "STATUS");
                if (!_obs.equals("") && !_status.equals("2")) {
                    content.setText("CloudPM - OT " + ConstValue.getOrderNumberString());
                    _name.setText(_obs);
                } else if (!_obs.equals("") && _status.equals("2")) {
                    content.setText("CloudPM - OT " + ConstValue.getOrderNumberString());
                    _name.setText(_obs);
                    _name.setEnabled(false);
                }
                if(ConstValue.getUserTypeString().equals("C")){
                    content.setText("CloudPM - OT " + ConstValue.getOrderNumberString());
                    _name.setText(_obs);
                    _name.setEnabled(false);
                }
                dialogOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(ConstValue.getUserTypeString().equals("C")){ dialog.dismiss(); }else{
                            String _nameString = _name.getText().toString();
                            if (_nameString.equals("")) {
                                Toast.makeText(context, "El campo observación no puede estar vacío.", Toast.LENGTH_SHORT).show();
                            } else {
                                if(ConstValue.getUserTypeString().equals("T")) {
                                    if (SqliteClass.getInstance(context).databasehelp.otsql.getStatusTechnical(ConstValue.getOrderNumberString()).equals("0")) {
                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "OBSH", _nameString);
                                        dialog.dismiss();
                                        Toast.makeText(context, "Observación guardada con éxito.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(context, "Actualmente la OT " + ConstValue.getOrderNumberString() + " ya ha sido cerrada.", Toast.LENGTH_SHORT).show();
                                    }
                                }else{
                                    if (SqliteClass.getInstance(context).databasehelp.otsql.getStatusManager(ConstValue.getOrderNumberString()).equals("0")) {
                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "OBSH", _nameString);
                                        dialog.dismiss();
                                        Toast.makeText(context, "Observación guardada con éxito.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(context, "Actualmente la OT " + ConstValue.getOrderNumberString() + " ya ha sido cerrada.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                });
                Button dialogCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dialogCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        /* DETALLE */
        fabOrderDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ConstValue.getUserTypeString().equals("T")) {
                    String _status = SqliteClass.getInstance(context).databasehelp.otsql.getData(ConstValue.getOrderNumberString());
                    if (_status.equals("0")) {
                        final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.alert_detail_ot);
                        ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                        image.setImageResource(R.drawable.ic_alert_info);
                        TextView title = (TextView) dialog.findViewById(R.id.alert_info_title);
                        title.setText("CloudPM");
                        TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                        String longMessage = "¿Que actividad desea realizar en la OT " + ConstValue.getOrderNumberString() + " ?";
                        content.setText(longMessage);
                        Button dialogOk = (Button) dialog.findViewById(R.id.alert_ok);
                        dialogOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (SqliteClass.getInstance(context).databasehelp.otOperationsSql.countElement(ConstValue.getOrderNumberString()) > 0) {
                                    SimpleDateFormat dateFormatHour = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                                    SimpleDateFormat dateFormatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                                    Date date = new Date();
                                    hourStart = dateFormatHour.format(date);
                                    ConstValue.setHourStart(hourStart);
                                    dateStart = dateFormatDate.format(date);
                                    ConstValue.setDateStart(dateStart);
                                    SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "BEGDA", dateStart);
                                    SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "BEGTI", hourStart);
                                    SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "STATUS", "1");
                                    ConstValue.setUserView(0);
                                    Intent intent = new Intent(OrderHActivity.this, OrderDActivity.class);
                                    startActivity(intent);
                                    dialog.dismiss();
                                } else {
                                    dialog.dismiss();
                                    Toast.makeText(context, getString(R.string.app_no_task), Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                        Button dialogCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                        dialogCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ConstValue.setUserView(1);
                                Intent intent = new Intent(OrderHActivity.this, OrderDActivity.class);
                                startActivity(intent);
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    } else if (_status.equals("1")) {
                        final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.alert_info);
                        ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                        image.setImageResource(R.drawable.ic_alert_info);
                        TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                        head.setText("CloudPM");
                        TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                        content.setText("Actualmente la OT " + ConstValue.getOrderNumberString() + " está en proceso. \n ¿Desea continuar ?");
                        Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                        dbOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                ConstValue.setUserView(0);
                                Intent intent = new Intent(OrderHActivity.this, OrderDActivity.class);
                                startActivity(intent);
                            }
                        });
                        Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                        dbCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    } else {
                        ConstValue.setUserView(1);
                        Intent intent = new Intent(OrderHActivity.this, OrderDActivity.class);
                        startActivity(intent);
                    }
                } else {
                    ConstValue.setUserView(1);
                    Intent intent = new Intent(OrderHActivity.this, OrderDActivity.class);
                    startActivity(intent);
                }
            }
        });
        /* CERRAR ORDEN */
        fabOrderClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ConstValue.getUserTypeString().equals("T"))
                {
                    if(SqliteClass.getInstance(context).databasehelp.otsql.getStatusTechnical(ConstValue.getOrderNumberString()).equals("0")){
                        int countOTOperations = SqliteClass.getInstance(context).databasehelp.otOperationsSql.countElement(ConstValue.getOrderNumberString());
                        int countOTComponents = SqliteClass.getInstance(context).databasehelp.otComponentsSql.countElement(ConstValue.getOrderNumberString());
                        final int countOTMeasurementPoints = SqliteClass.getInstance(context).databasehelp.otMeasurementPointsSql.countElement(ConstValue.getOrderNumberString());
                        String longMessage = "¿Esta seguro de cerrar la OT " + ConstValue.getOrderNumberString() + "? \n" +
                                "Actualmente tiene pendiente(s): \n" +
                                "(" + countOTOperations + ") Tarea(s) \n" +
                                "(" + countOTComponents + ") Componente(s) \n" +
                                "(" + countOTMeasurementPoints + ") Punto(s) de Medida \n";
                        final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.alert_info);

                        ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                        image.setImageResource(R.drawable.ic_alert_info);

                        TextView title = (TextView) dialog.findViewById(R.id.alert_info_title);
                        title.setText("CloudPM - CERRAR ORDEN");
                        final TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                        content.setText(longMessage);

                        Button dialogButton = (Button) dialog.findViewById(R.id.alert_ok);
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (countOTMeasurementPoints > 0) {
                                    dialog.dismiss();
                                    Util.alertInfoOtMeasure(context);
                                } else {
                                    dialog.dismiss();
                                    final Dialog dialog1 = new Dialog(context);
                                    dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    dialog1.setContentView(R.layout.alert_satisfaction);

                                    ImageView image = (ImageView) dialog1.findViewById(R.id.alert_info);
                                    image.setImageResource(R.drawable.ic_alert_info);

                                    TextView title = (TextView) dialog1.findViewById(R.id.alert_info_title);
                                    title.setText("CloudPM - NIVEL DE SATISFACCIÓN");
                                    final TextView content = (TextView) dialog1.findViewById(R.id.alert_info_content);
                                    content.setText(SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("stfcnQstnTecnic"));
                                    final EditText observationSatisfaction = (EditText) dialog1.findViewById(R.id.etObservation_stfc);

                                    final ImageView stfc_low = (ImageView) dialog1.findViewById(R.id.img_satisfaction_low);
                                    final ImageView stfc_medium = (ImageView) dialog1.findViewById(R.id.img_satisfaction_medium);
                                    final ImageView stfc_high = (ImageView) dialog1.findViewById(R.id.img_satisfaction_high);
                                    stfc_low.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            stfc_low.setColorFilter(Color.rgb(51, 133, 255));
                                            stfc_medium.setColorFilter(Color.GRAY);
                                            stfc_high.setColorFilter(Color.GRAY);
                                            valStfc="1";
                                            SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "VALTEC",
                                                    SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("sftcnLow"));
                                        }
                                    });
                                    stfc_medium.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            stfc_low.setColorFilter(Color.GRAY);
                                            stfc_medium.setColorFilter(Color.rgb(51, 133, 255));
                                            stfc_high.setColorFilter(Color.GRAY);
                                            valStfc="1";
                                            SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "VALTEC",
                                                    SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("sftcnMedium"));
                                        }
                                    });
                                    stfc_high.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            stfc_low.setColorFilter(Color.GRAY);
                                            stfc_medium.setColorFilter(Color.GRAY);
                                            stfc_high.setColorFilter(Color.rgb(51, 133, 255));
                                            valStfc="1";
                                            SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "VALTEC",
                                                    SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("sftcnHigh"));
                                        }
                                    });
                                    Button ok = (Button) dialog1.findViewById(R.id.alert_ok);
                                    ok.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (valStfc.equals("1")){
                                                dialog1.dismiss();
                                                SimpleDateFormat dateFormatHour = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                                                SimpleDateFormat dateFormatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                                                Date date = new Date();
                                                hourEnd = dateFormatHour.format(date);
                                                ConstValue.setHourEnd(hourEnd);
                                                dateEnd = dateFormatDate.format(date);
                                                ConstValue.setDateEnd(dateEnd);
                                                SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "ENDDA", dateEnd);
                                                SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "ENDTI", hourEnd);
                                                SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "STATUS", "2");
                                                SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "TECHAPP", "1");
                                                SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "RESPTEC",String.valueOf(observationSatisfaction.getText()));
                                                if (cd.isConnectingToInternet()) {
                                                    new OTClosingTask().execute(true);
                                                } else {
                                                    Toast.makeText(context, getString(R.string.app_no_action), Toast.LENGTH_SHORT).show();
                                                }
                                            }else {
                                                Toast.makeText(context,"Debe seleccionar un valor de satisfacción",Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                    });
                                    dialog1.show();
                                }

                            }
                        });
                        Button dialogCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                        dialogCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    } else {
                        Util.alertInfo(context, "CloudPM - CERRAR ORDEN", "Actualmente la OT " + ConstValue.getOrderNumberString() + " ya ha sido cerrada.");
                    }
                } else {
                    if (ConstValue.getOrderAdvance().equals("100") &&
                            SqliteClass.getInstance(context).databasehelp.otsql.getStatusManager(ConstValue.getOrderNumberString()).equals("0")) {
                        SimpleDateFormat dateFormatHour = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
                        SimpleDateFormat dateFormatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        Date date = new Date();
                        hourStart = dateFormatHour.format(date);
                        dateStart = dateFormatDate.format(date);
                        hourEnd = dateFormatHour.format(date);
                        dateEnd = dateFormatDate.format(date);
                        final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.alert_info);

                        ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                        image.setImageResource(R.drawable.ic_alert_info);

                        TextView title = (TextView) dialog.findViewById(R.id.alert_info_title);
                        title.setText("CloudPM - CERRAR ORDEN");
                        TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                        content.setText("¿ Qué acción desea tomar sobre la orden para cerrarla ?");

                        Button dialogButton = (Button) dialog.findViewById(R.id.alert_ok);
                        dialogButton.setText("APROBAR");
                        dialogButton.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                final Dialog dialog2 = new Dialog(context);
                                dialog2.setContentView(R.layout.alert_satisfaction);
                                ImageView image = (ImageView) dialog2.findViewById(R.id.alert_info);
                                image.setImageResource(R.drawable.ic_alert_info);
                                TextView title = (TextView) dialog2.findViewById(R.id.alert_info_title);
                                title.setText("CLOUD PM - NIVEL DE SATISFACCIÓN");
                                final TextView content = (TextView) dialog2.findViewById(R.id.alert_info_content);
                                content.setText(SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("stfcnQstnManager"));
                                final EditText observationSup = (EditText) dialog2.findViewById(R.id.etObservation_stfc);
                                final ImageView stfc_low = (ImageView) dialog2.findViewById(R.id.img_satisfaction_low);
                                final ImageView stfc_medium = (ImageView) dialog2.findViewById(R.id.img_satisfaction_medium);
                                final ImageView stfc_high = (ImageView) dialog2.findViewById(R.id.img_satisfaction_high);
                                stfc_low.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stfc_low.setColorFilter(Color.rgb(51, 133, 255));
                                        stfc_medium.setColorFilter(Color.GRAY);
                                        stfc_high.setColorFilter(Color.GRAY);
                                        valStfc="1";
                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "VALSUP",
                                                SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("sftcnLow"));
                                    }
                                });
                                stfc_medium.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stfc_low.setColorFilter(Color.GRAY);
                                        stfc_medium.setColorFilter(Color.rgb(51, 133, 255));
                                        stfc_high.setColorFilter(Color.GRAY);
                                        valStfc="1";
                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "VALSUP",
                                                SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("sftcnMedium"));
                                    }
                                });
                                stfc_high.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stfc_low.setColorFilter(Color.GRAY);
                                        stfc_medium.setColorFilter(Color.GRAY);
                                        stfc_high.setColorFilter(Color.rgb(51, 133, 255));
                                        valStfc="1";
                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "VALSUP",
                                                SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("sftcnHigh"));
                                    }
                                });
                                Button ok = (Button) dialog2.findViewById(R.id.alert_ok);
                                ok.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (valStfc.equals("1")){
                                            dialog2.dismiss();
                                            SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "BEGDA", dateStart);
                                            SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "BEGTI", hourStart);
                                            SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "ENDDA", dateEnd);
                                            SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "ENDTI", hourEnd);
                                            SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "STATUS", "2");
                                            SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "CUSAPP", "1");
                                            SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "RESPSUP",String.valueOf(observationSup.getText()));
                                            if (cd.isConnectingToInternet()) {
                                                new OTClosingUpdateTask().execute(true);
                                            } else {
                                                Toast.makeText(context, getString(R.string.app_no_action), Toast.LENGTH_SHORT).show();
                                            }
                                        }else {
                                            Toast.makeText(context,"Debe seleccionar almenos un valor de satisfacción",Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });
                                dialog2.show();

                            }
                        });
                        Button dialogCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                        dialogCancel.setText("RECHAZAR");
                        dialogCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                final Dialog dObs = new Dialog(context);
                                dObs.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dObs.setContentView(R.layout.alert_ot);
                                TextView content = (TextView) dObs.findViewById(R.id.alert_rtn_content);
                                content.setText("CloudPM - OT " + ConstValue.getOrderNumberString());
                                final EditText _namec = (EditText) dObs.findViewById(R.id.etObservation);
                                String _obsc = SqliteClass.getInstance(context).databasehelp.otsql.getOTInfo(ConstValue.getOrderNumberString(), "OBSHC");
                                content.setText("CloudPM - OT " + ConstValue.getOrderNumberString());
                                _namec.setText(_obsc);
                                Button dObsOk = (Button) dObs.findViewById(R.id.alert_ok);
                                dObsOk.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        final String _nameString = _namec.getText().toString();
                                        if (_nameString.equals("")) {
                                            Toast.makeText(context, "El campo observación no puede estar vacío.", Toast.LENGTH_SHORT).show();
                                        } else {
                                            dObs.dismiss();
                                            final Dialog dialog2 = new Dialog(context);
                                            //dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            dialog2.setContentView(R.layout.alert_satisfaction);

                                            ImageView image = (ImageView) dialog2.findViewById(R.id.alert_info);
                                            image.setImageResource(R.drawable.ic_alert_info);

                                            TextView title = (TextView) dialog2.findViewById(R.id.alert_info_title);
                                            title.setText("CLOUD PM - NIVEL DE SATISFACCIÓN");
                                            final TextView content = (TextView) dialog2.findViewById(R.id.alert_info_content);
                                            content.setText(SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("stfcnQstnManager"));
                                            final EditText observation = (EditText) dialog2.findViewById(R.id.etObservation_stfc);
                                            final ImageView stfc_low = (ImageView) dialog2.findViewById(R.id.img_satisfaction_low);
                                            final ImageView stfc_medium = (ImageView) dialog2.findViewById(R.id.img_satisfaction_medium);
                                            final ImageView stfc_high = (ImageView) dialog2.findViewById(R.id.img_satisfaction_high);
                                            stfc_low.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    stfc_low.setColorFilter(Color.rgb(51, 133, 255));
                                                    stfc_medium.setColorFilter(Color.GRAY);
                                                    stfc_high.setColorFilter(Color.GRAY);
                                                    valStfc="1";
                                                    SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "VALSUP",
                                                            SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("sftcnLow"));
                                                }
                                            });
                                            stfc_medium.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    stfc_low.setColorFilter(Color.GRAY);
                                                    stfc_medium.setColorFilter(Color.rgb(51, 133, 255));
                                                    stfc_high.setColorFilter(Color.GRAY);
                                                    valStfc="1";
                                                    SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "VALSUP",
                                                            SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("sftcnMedium"));
                                                }
                                            });
                                            stfc_high.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    stfc_low.setColorFilter(Color.GRAY);
                                                    stfc_medium.setColorFilter(Color.GRAY);
                                                    stfc_high.setColorFilter(Color.rgb(51, 133, 255));
                                                    valStfc="1";
                                                    SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "VALSUP",
                                                            SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("sftcnHigh"));
                                                }
                                            });
                                            Button ok = (Button) dialog2.findViewById(R.id.alert_ok);
                                            ok.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if (valStfc.equals("1")){
                                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "RESPSUP",String.valueOf(observation.getText()));
                                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "BEGDA", dateStart);
                                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "BEGTI", hourStart);
                                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "ENDDA", dateEnd);
                                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "ENDTI", hourEnd);
                                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "STATUS", "3");
                                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "CUSAPP", "2");
                                                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(ConstValue.getOrderNumberString(), "OBSHC", _nameString);
                                                        Toast.makeText(context, "Observación guardada con éxito.", Toast.LENGTH_SHORT).show();
                                                        dialog2.dismiss();
                                                        if (cd.isConnectingToInternet()) {
                                                            new OTClosingUpdateTask().execute(true);
                                                        } else {
                                                            Toast.makeText(context, getString(R.string.app_no_action), Toast.LENGTH_SHORT).show();
                                                        }
                                                    }else {
                                                        Toast.makeText(context,"Debe seleccionar almenos un valor de satisfacción",Toast.LENGTH_SHORT).show();
                                                    }

                                                }
                                            });
                                            dialog2.show();
                                        }
                                    }
                                });
                                Button dObsCancel = (Button) dObs.findViewById(R.id.alert_cancel);
                                dObsCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dObs.dismiss();
                                    }
                                });
                                dObs.show();
                            }
                        });
                        dialog.show();

                    }else{
                        if(!ConstValue.getOrderAdvance().equals("100")) {
                            Util.alertInfo(context, "CloudPM - CERRAR ORDEN", "Actualmente la OT " + ConstValue.getOrderNumberString() + " no ha sido cerrada totalmente por el/los técnicos.");
                        }else if(SqliteClass.getInstance(context).databasehelp.otsql.getStatusManager(ConstValue.getOrderNumberString()).equals("1")){
                            Util.alertInfo(context, "CloudPM - CERRAR ORDEN", "Actualmente la OT " + ConstValue.getOrderNumberString() + " ha sido cerrada.");
                        }
                    }
                }
            }
        });
    }
    private void showFABMenu(){
        isFABOpen=true;
        flOrderClose.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        tvOrderClose.setVisibility(View.VISIBLE);
        flOrderDetail.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
        tvOrderDetail.setVisibility(View.VISIBLE);
        flOrderObservation.animate().translationY(-getResources().getDimension(R.dimen.standard_155));
        tvOrderObservation.setVisibility(View.VISIBLE);
    }
    private void closeFABMenu(){
        isFABOpen=false;
        flOrderClose.animate().translationY(0);
        tvOrderClose.setVisibility(View.GONE);
        flOrderDetail.animate().translationY(0);
        tvOrderDetail.setVisibility(View.GONE);
        flOrderObservation.animate().translationY(0);
        tvOrderObservation.setVisibility(View.GONE);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            Intent intent = new Intent(OrderHActivity.this,MainActivity.class);
            startActivity(intent);
        }
        else if(id==R.id.action_logout){
            Util.logout(OrderHActivity.this, context, "0");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Util.logout(OrderHActivity.this, context, "1");
    }
    class OTClosingTask extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialogOTClosing = ProgressDialog.show(OrderHActivity.this, "CloudPM", getString(R.string.action_loading), true);
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String result) {
            //startService(new Intent(context, ServicePM.class));
            if (result != null) {
                Toast.makeText(getApplicationContext(), "CloudPM " + result, Toast.LENGTH_LONG).show();
            } else {
                /*final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_info);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("CloudPM - SINCRONIZAR");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("La OT " + ConstValue.getOrderNumberString() + " se envió con éxito. \n ¿Desea regresar a la lista de Ordenes ?");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(OrderHActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();*/
                //stopService(new Intent(context, ServicePM.class));
                new LocationTask().execute(true);
            }
            // TODO Auto-generated method stub
            dialogOTClosing.dismiss();
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }
        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }
        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = null;
            otLoad = new ArrayList<OTClass>();
            otLoad = SqliteClass.getInstance(context).databasehelp.otsql.getPendingOT();
            try {
                JSONObject jsonobj;
                for (int z = 0; z < otLoad.size(); z++) {
                    String total = Util.getDifferenceTime(otLoad.get(z).getWorkStartDate(),otLoad.get(z).getWorkStartTime(), otLoad.get(z).getWorkEndDate(), otLoad.get(z).getEndTimeWork());
                    common = new Common();
                    jsonobj = new JSONObject();
                    jsonobj.put("AUFNR", otLoad.get(z).getNumberOrder());
                    jsonobj.put("OBJID", ConstValue.getUserCodeString());
                    jsonobj.put("TECHAPP", "1");
                    jsonobj.put("CUSAPP", otLoad.get(z).getClientClosure());
                    jsonobj.put("OBSH", otLoad.get(z).getObservation());
                    jsonobj.put("BEGDA", otLoad.get(z).getWorkStartDate());
                    jsonobj.put("BEGTI", otLoad.get(z).getWorkStartTime());
                    jsonobj.put("ENDDA", otLoad.get(z).getWorkEndDate());
                    jsonobj.put("ENDTI", otLoad.get(z).getEndTimeWork());
                    jsonobj.put("RESPTEC", otLoad.get(z).getTechnicianObservation());
                    jsonobj.put("VALTEC", otLoad.get(z).getTechnicianValue());
                    jsonobj.put("TOTALHRS", total);
                    jsonobj.put("OBSUP", "");
                    jsonobj.put("ADVANCE", "");

                    JSONArray jsonArrayOtComp = new JSONArray();
                    JSONArray jsonArrayOtMesu = new JSONArray();
                    JSONArray jsonArrayOtOper = new JSONArray();
                    otcomponentLoad  = new ArrayList<OTComponentsClass>();
                    otcomponentLoad =  SqliteClass.getInstance(context).databasehelp.otComponentsSql.getOrderNumber(otLoad.get(z).getNumberOrder());
                    for (int i=0;i<otcomponentLoad.size();i++){
                        JSONObject detailOtComp = new JSONObject();
                        detailOtComp.put("AUFNR",otcomponentLoad.get(i).getNumberOrder());
                        detailOtComp.put("VORNR",otcomponentLoad.get(i).getNumberOperation());
                        detailOtComp.put("POSNR",otcomponentLoad.get(i).getPosition());
                        detailOtComp.put("OBJID",otcomponentLoad.get(i).getObjID());
                        detailOtComp.put("DONE",otcomponentLoad.get(i).getStatus());
                        detailOtComp.put("CPOBS",otcomponentLoad.get(i).getObservation());
                        detailOtComp.put("QUANTITY",otcomponentLoad.get(i).getAmountUsed());
                        detailOtComp.put("MAKTX",otcomponentLoad.get(i).getItemDescription());
                        detailOtComp.put("MATNR",otcomponentLoad.get(i).getItemCode());
                        detailOtComp.put("MENGE",otcomponentLoad.get(i).getQuantity());
                        detailOtComp.put("MEINS",otcomponentLoad.get(i).getUnitMeasure());
                        jsonArrayOtComp.put(detailOtComp);
                    }
                    jsonobj.put("OTcomponent",jsonArrayOtComp);
                    otmeasurementLoad = new ArrayList<OTMeasurementPointClass>();
                    otmeasurementLoad = SqliteClass.getInstance(context).databasehelp.otMeasurementPointsSql.getOrderNumber(otLoad.get(z).getNumberOrder(), ConstValue.getOrderTechnicalString());
                    for(int y=0; y<otmeasurementLoad.size(); y++){
                        JSONObject detailOtMes = new JSONObject();
                        detailOtMes.put("AUFNR", otmeasurementLoad.get(y).getNumberOrder());
                        detailOtMes.put("POINT", otmeasurementLoad.get(y).getNumberPoint());
                        detailOtMes.put("EQUNR", otmeasurementLoad.get(y).getEquipment());
                        detailOtMes.put("VALUE", otmeasurementLoad.get(y).getMeasuredValue());
                        detailOtMes.put("VALUE_DATE", otmeasurementLoad.get(y).get_date());
                        detailOtMes.put("VALUE_TIME", otmeasurementLoad.get(y).getHour());
                        detailOtMes.put("OBJID", ConstValue.getOrderTechnicalString());
                        detailOtMes.put("TAKEN", otmeasurementLoad.get(y).getStatus());
                        detailOtMes.put("NEQUNR", otmeasurementLoad.get(y).getEquipmentName());
                        detailOtMes.put("PMOBS", otmeasurementLoad.get(y).getObservation());
                        jsonArrayOtMesu.put(detailOtMes);
                    }
                    jsonobj.put("OTmeasure", jsonArrayOtMesu);
                    otoperationtLoad = new ArrayList<OTOperationsClass>();
                    otoperationtLoad = SqliteClass.getInstance(context).databasehelp.otOperationsSql.getOrderNumber(otLoad.get(z).getNumberOrder());
                    for(int y=0; y<otoperationtLoad.size(); y++){
                        JSONObject detailOtOpe = new JSONObject();
                        detailOtOpe.put("AUFNR", otoperationtLoad.get(y).getNumberOrder());
                        detailOtOpe.put("VORNR", otoperationtLoad.get(y).getNumberOperation());
                        detailOtOpe.put("OBJID", otoperationtLoad.get(y).getObjID());
                        detailOtOpe.put("DONE", otoperationtLoad.get(y).getStatus());
                        detailOtOpe.put("OPOBS", otoperationtLoad.get(y).getObservation());
                        detailOtOpe.put("OPTIME", otoperationtLoad.get(y).getTimeRequired());
                        detailOtOpe.put("LTXA1", otoperationtLoad.get(y).getShortText());
                        jsonArrayOtOper.put(detailOtOpe);
                    }
                    jsonobj.put("OToperation", jsonArrayOtOper);
                    List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                    nvps.add(new BasicNameValuePair("data", jsonobj.toString()));
                    JSONObject json = common.sendJsonData(ConstValue.JSON_OT, nvps);
                    if (json.getString("response").equalsIgnoreCase("success")) {
                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(otLoad.get(z).getNumberOrder(), "LOAD", "1");
                    } else {
                        responseString = json.getString("response");
                    }
                }
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return responseString;
        }

    }

    class OTClosingUpdateTask extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialogOTClosingUpdate = ProgressDialog.show(OrderHActivity.this, "CloudPM", getString(R.string.action_loading), true);
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Toast.makeText(getApplicationContext(), "CloudPM " + result, Toast.LENGTH_LONG).show();
            } else {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_info);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("CloudPM - SINCRONIZAR");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("La OT " + ConstValue.getOrderNumberString() + " se envió con éxito. \n ¿Desea regresar a la lista de Ordenes ?");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(OrderHActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
            // TODO Auto-generated method stub
            dialogOTClosingUpdate.dismiss();
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }
        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }
        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = null;
            otLoadClosing = new ArrayList<OTClass>();
            otLoadClosing = SqliteClass.getInstance(context).databasehelp.otsql.getPendingOT();
            try {
                JSONObject jsonobj;
                for (int z = 0; z < otLoadClosing.size(); z++) {
                    common = new Common();
                    jsonobj = new JSONObject();

                    jsonobj.put("AUFNR", otLoadClosing.get(z).getNumberOrder());
                    jsonobj.put("OBJID", otLoadClosing.get(z).getObjID());
                    jsonobj.put("CUSAPP", otLoadClosing.get(z).getClientClosure());
                    jsonobj.put("OBSUP", otLoadClosing.get(z).getObservationc());
                    jsonobj.put("ADVANCE", ConstValue.getOrderAdvance());

                    jsonobj.put("RESPSUP", otLoadClosing.get(z).getSupervisorObservation());
                    jsonobj.put("VALSUP", otLoadClosing.get(z).getSupervisorValue());
                    List<NameValuePair> nvps = new ArrayList<NameValuePair>();

                    nvps.add(new BasicNameValuePair("data", jsonobj.toString()));

                    String jsonString = jsonobj.toString();
                    JSONObject json = common.sendJsonData(ConstValue.JSON_OT_POST, nvps);
                    if (json.getString("response").equalsIgnoreCase("success")) {
                        SqliteClass.getInstance(context).databasehelp.otsql.updateOT(otLoadClosing.get(z).getNumberOrder(), "LOAD", "1");
                    } else {
                        responseString = json.getString("response");
                    }
                }
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return responseString;
        }

    }

    class LocationTask extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialogLocation = ProgressDialog.show(OrderHActivity.this, "CloudPM", getString(R.string.action_loading), true);
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Toast.makeText(getApplicationContext(), "CloudPM " + result, Toast.LENGTH_LONG).show();
            } else {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_info);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("CloudPM - SINCRONIZAR");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("La OT " + ConstValue.getOrderNumberString() + " se envió con éxito. \n ¿Desea regresar a la lista de Ordenes ?");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(OrderHActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
            // TODO Auto-generated method stub
            dialogLocation.dismiss();
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }
        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }
        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = null;
            try {
                double latitude = 0.0;
                double longitude = 0.0;
                if(cd.isConnectingToInternet()){
                    mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
                    List<String> providers = mLocationManager.getProviders(true);
                    Location bestLocation = null;
                    for (String provider : providers) {
                        @SuppressLint("MissingPermission") Location l = mLocationManager.getLastKnownLocation(provider);
                        if (l == null) { continue; }
                        if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                            bestLocation = l;
                        }
                    }
                    Location location = bestLocation;
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
                JSONObject jsonobj;
                common = new Common();
                jsonobj = new JSONObject();
                jsonobj.put("OBJID", ConstValue.getOrderTechnicalString());
                jsonobj.put("LAT", latitude);
                jsonobj.put("LON", longitude);
                jsonobj.put("CENTER", ConstValue.getOrderCenterString());
                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair("data", jsonobj.toString()));
                String jsonString = jsonobj.toString();
                Log.i("JSON GPS: ", jsonobj.toString());
                System.out.println("CloudPM "+ jsonString);
                JSONObject json = common.sendJsonData(ConstValue.JSON_GPS, nvps);
                if (json.getString("response").equalsIgnoreCase("success")) {
                    Log.e("CloudPM", "Enviado con éxito");
                } else {
                    responseString = json.getString("response");
                }
            } catch (JSONException e) {
                Log.e("JSON GPS Parser", "Error parsing data " + e.toString());
            }
            return responseString;
        }
    }
}
