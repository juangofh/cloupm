package com.pm.views.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pm.R;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.imgLoader.JSONParser;

import com.pm.models.ConfigurationClass;
import com.pm.models.SupportTicketClass;
import com.pm.models.SynchronizationClass;
import com.pm.models.UserClass;
import com.pm.models.EquipmentClass;
import com.pm.models.NoticeTypeClass;
import com.pm.models.SymptomClass;
import com.pm.models.TechnicalLocationsClass;
import com.pm.models.OTClass;
import com.pm.models.OTComponentsClass;
import com.pm.models.OTMeasurementPointClass;
import com.pm.models.OTOperationsClass;

import com.pm.utils.Common;
import com.pm.utils.ConnectionDetector;
import com.pm.utils.GpsTracker;
import com.pm.utils.Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    public SharedPreferences settings;
    public ConnectionDetector cd;
    EditText txtUser, txtPassword;
    Button  btnLogin;
    Common common;
    ProgressDialog dialog;
    Activity activity;
    AsyncTask<Void, Void, Void> mRegisterTask;

    private UserClass User; private ArrayList<UserClass> loadUser;
    private EquipmentClass EquipmentClass; private ArrayList<EquipmentClass> loadEquipmentClass;
    private NoticeTypeClass NoticeTypeClass; private ArrayList<NoticeTypeClass> loadNoticeTypeClass;
    private SymptomClass SymptomClass; private ArrayList<SymptomClass> loadSymptomClass;
    private TechnicalLocationsClass TechnicalLocationsClass; private ArrayList<TechnicalLocationsClass> loadTechnicalLocationsClass;
    private OTClass OT; private ArrayList<OTClass> loadOT;
    private OTComponentsClass OTComponent; private ArrayList<OTComponentsClass> loadOTComponent;
    private OTMeasurementPointClass OTMeasurementPoint; private ArrayList<OTMeasurementPointClass> loadOTMeasurementPoint;
    private OTOperationsClass OTOperation; private ArrayList<OTOperationsClass> loadOTOperation;
    private ConfigurationClass configuration; private ArrayList<ConfigurationClass> loadConfiguration;
    public Context context;
    public String _date;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    private GpsTracker gpsTracker; public double latitude; public double longitude;

    private SupportTicketClass Ticket; private List<SupportTicketClass> ticketList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        common = new Common();
        activity = this;
        gpsTracker = new GpsTracker(LoginActivity.this);
        if(gpsTracker.canGetLocation()){
            latitude = gpsTracker.getLatitude(); longitude = gpsTracker.getLongitude();
        }else{
            gpsTracker.showSettingsAlert();
        }
        //SqliteClass.getInstance(context).databasehelp.deleteDataBase();
        if (SqliteClass.getInstance(context).databasehelp.checkDataBase()) {
            if(SqliteClass.getInstance(context).databasehelp.usersql.getActive().equals("1")){
                String username = SqliteClass.getInstance(context).databasehelp.usersql.getUser();
                if(!username.equals("")) {
                    offLine(username);
                }
            }
        }
        _date = dateFormat.format(new Date());
        settings = getSharedPreferences(ConstValue.MAIN_PREF, 0);
        cd = new ConnectionDetector(this);
        txtUser = (EditText) findViewById(R.id.userUser);
        TextView server = (TextView) findViewById(R.id.textView);
        if(ConstValue.SITE_URL.equals("http://52.41.46.242:443/api/main/mobility")){
            server.setText("PRD 3.8");
        }else {
            server.setText("QAS 3.8");
        }
        txtPassword = (EditText) findViewById(R.id.userPassword);
        btnLogin = (Button) findViewById(R.id.userLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (txtUser.getText().toString().length() == 0) {
                    txtUser.setError("Porfavor, ingrese usuario.");
                } else if (txtPassword.getText().toString().length() == 0) {
                    txtPassword.setError("Porfavor, ingrese contraseña.");
                } else {
                    //SqliteClass.getInstance(context).databasehelp.deleteDataBase();
                    if (SqliteClass.getInstance(context).databasehelp.checkDataBase()) {
                        String user = txtUser.getText().toString();
                        String password = txtPassword.getText().toString();
                        if (SqliteClass.getInstance(context).databasehelp.usersql.isRegisterUser(user, password)) {
                            String _dateUser = SqliteClass.getInstance(context).databasehelp.usersql.getData(8, txtUser.getText().toString());
                            if (_date.equals(_dateUser)) {
                                if (SqliteClass.getInstance(context).databasehelp.usersql.isRegisterUser(txtUser.getText().toString(), txtPassword.getText().toString())) {
                                    offLine(user);
                                } else {
                                    Toast.makeText(getApplicationContext(), "CloudPM - Credenciales inválidas, intente nuevamente.", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                SqliteClass.getInstance(context).databasehelp.deleteDataBase();
                                if (cd.isConnectingToInternet()) {
                                    new loginTask().execute(true);
                                } else {
                                    Toast.makeText(getApplicationContext(), "CloudPM - Su dispositivo no cuenta con conexión a internet.", Toast.LENGTH_LONG).show();
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "CloudPM - Usuario y/o contraseña incorrecta, porfavor intente nuevamente.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        SqliteClass.getInstance(context).databasehelp.deleteDataBase();
                        if (cd.isConnectingToInternet()) {
                            new loginTask().execute(true);
                        } else {
                            Toast.makeText(getApplicationContext(), "CloudPM - Su dispositivo no cuenta con conexión a internet.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        activity.finish();
    }

    public void offLine(String user){
        ConstValue.setUserIdInt(1);
        ConstValue.setUserUserString(user);
        ConstValue.setUserTypeString(SqliteClass.getInstance(context).databasehelp.usersql.getData(7, user));
        ConstValue.setUserCodeString(SqliteClass.getInstance(context).databasehelp.usersql.getData(3, user));
        ConstValue.setUserCenterString(SqliteClass.getInstance(context).databasehelp.usersql.getData(9, user));
        ConstValue.setColorEmergencyString(SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("colorOTEmergency"));
        ConstValue.setColorRoutineString(SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("colorOTRutine"));
        ConstValue.setColorOtherString(SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("colorOTOther"));
        ConstValue.setColorRefuseString(SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("colorEOReject"));
        ConstValue.setColorFinishString(SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("colorEOFinalized"));
        ConstValue.setColorPendingString(SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("colorEONotStarting"));
        ConstValue.setColorProcessString(SqliteClass.getInstance(context).databasehelp.configurationSql.getVal("colorEOInProcess"));
        if(ConstValue.getUserTypeString().equals("T")){
            ConstValue.setSendCodeString(ConstValue.getUserCodeString().trim());
        }else if(ConstValue.getUserTypeString().equals("C")){
            ConstValue.setSendCodeString(ConstValue.getUserCenterString().trim());
        }
        ConstValue.setFragmentView("Order");
        SqliteClass.getInstance(context).databasehelp.usersql.update("1");
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }
    class loginTask extends AsyncTask<Boolean, Void, String> {
        String user, password;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            user = txtUser.getText().toString();
            password = txtPassword.getText().toString();
            dialog = ProgressDialog.show(LoginActivity.this, "CloudPM", getString(R.string.action_loading), true);
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Toast.makeText(getApplicationContext(), "CloudPM " + result, Toast.LENGTH_LONG).show();
                //Toast.makeText(getApplicationContext(), "CloudPM: Server OFFLINE " , Toast.LENGTH_LONG).show();
            } else {
                ConstValue.setFragmentView("Order");
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
            }
            // TODO Auto-generated method stub
            dialog.dismiss();
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }
        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }
        @Override
        protected String doInBackground(Boolean... params) {
            String responseString = null;
            try {
                loadUser = new ArrayList<UserClass>();
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("username", user));
                nameValuePairs.add(new BasicNameValuePair("password", password));
                nameValuePairs.add(new BasicNameValuePair("version", "3.8"));
                nameValuePairs.add(new BasicNameValuePair("typeAPP", "Native"));
                JSONObject jObj = common.sendJsonData(ConstValue.JSON_LOGIN, nameValuePairs);
                try {
                    if (jObj.getString("response").equalsIgnoreCase("success")) {
                        JSONObject data = jObj.getJSONObject("data");
                        if (data.getString("username").equalsIgnoreCase(user)) {
                            User = new UserClass(data.getInt("id"), user, password, data.getString("code"), data.getString("first_name"),
                                data.getString("last_name"), data.getString("email"), data.getString("type_user"), _date,
                                data.getString("center"), "1");
                            ConstValue.setUserIdInt(data.getInt("id"));
                            ConstValue.setUserUserString(user);
                            ConstValue.setUserTypeString(data.getString("type_user"));
                            ConstValue.setUserCodeString(data.getString("code"));
                            ConstValue.setUserCenterString(data.getString("center"));
                            if(ConstValue.getUserTypeString().equals("T")){
                                ConstValue.setSendCodeString(ConstValue.getUserCodeString().trim());
                            }else if(ConstValue.getUserTypeString().equals("C")){
                                ConstValue.setSendCodeString(ConstValue.getUserCenterString().trim());
                            }
                            String nameFile = "CloudPM";
                            String dataFile = ConstValue.JSON_NOTIFICATION_GET+ConstValue.getUserTypeString()+"/"+ConstValue.getSendCodeString()+"/";
                            FileOutputStream fileOutputStream = null;
                            try{
                                fileOutputStream = openFileOutput(nameFile, Context.MODE_PRIVATE);
                                fileOutputStream.write(dataFile.getBytes());
                                fileOutputStream.close();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            SqliteClass.getInstance(context).databasehelp.usersql.addUser(User);
                        } else {
                            responseString = "Login1 - " + jObj.getString("data");
                            return responseString;
                        }
                    }else{
                        responseString = "Login2 - " + jObj.getString("data");
                        return responseString;
                    }
                } catch (JSONException e) {
                    responseString = "Login3 - " + e.getMessage();
                    return responseString;
                }
                /* CONFIGURATION CLASS */
                JSONParser jParser;
                JSONObject json;
                String url = "";
                jParser = new JSONParser();
                url = ConstValue.JSON_CONFIGURATION;
                json = jParser.getJSONFromUrl(url);
                loadConfiguration =  new ArrayList<ConfigurationClass>();
                if(json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList =  json.getJSONArray("data");
                        for (int j=0; j<jsonList.length();j++){
                            JSONObject o =  jsonList.getJSONObject(j);
                            configuration = new ConfigurationClass(o.getString("outstart"),o.getString("distance"),o.getString("valor2"),
                                    o.getString("questtech"),o.getString("emergency"),o.getString("valor1"),o.getString("notification"),
                                    o.getString("questsup"),o.getString("routine"),o.getString("inprocess"),o.getString("rejected"),
                                    o.getString("obligatory"),o.getString("finished"),o.getString("valor3"),o.getString("others"),o.getString("automatic"),j);
                            loadConfiguration.add(configuration);
                            ConstValue.setColorEmergencyString(o.getString("emergency"));
                            ConstValue.setColorRoutineString(o.getString("routine"));
                            ConstValue.setColorOtherString(o.getString("others"));
                            ConstValue.setColorRefuseString(o.getString("rejected"));
                            ConstValue.setColorFinishString(o.getString("finished"));
                            ConstValue.setColorPendingString(o.getString("outstart"));
                            ConstValue.setColorProcessString(o.getString("inprocess"));
                            SqliteClass.getInstance(context).databasehelp.configurationSql.addConfiguration(configuration);
                        }
                    }
                }jParser = null; json = null;
                /* OT CLASS */
                loadOT = new ArrayList<OTClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_OTHEAD+ConstValue.getUserTypeString()+"/"+ConstValue.getSendCodeString()+"/";
                json = jParser.getJSONFromUrl(url);
                if(json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList =  json.getJSONArray("data");
                        System.out.println("Cloud PM - OTHEAD "+jsonList.length());
                        for (int j=0; j<jsonList.length();j++){
                            JSONObject o =  jsonList.getJSONObject(j);
                            ConstValue.setOrderTechnicalString(o.getString("OBJID"));
                            OT =  new OTClass(j,o.getString("AUFNR"),o.getString("AUART"),o.getString("NAUART"), o.getString("INGRP"), o.getString("NINGRP"),
                                    o.getString("WERKS"), o.getString("NWERKS"), o.getString("ARBPL"), o.getString("NARBPL"), o.getString("SOWRK"), o.getString("NSOWRK"),
                                    o.getString("OBJID"), o.getString("OBJID2"), o.getString("TPLNR"), o.getString("NTPLNR"), o.getString("KTEXT"), o.getString("GSTRP"), o.getString("GLTRP"),
                                    o.getString("BEGTI"), o.getString("ENDTI"), o.getString("NOTNR"), o.getString("PRIOK"), o.getString("NPRIOK"), o.getString("TPLNR"), o.getString("NTPLNR"),
                                    o.getString("EQUNR"), o.getString("NEQUNR"), ConstValue.getUserCodeString(), o.getString("TECHAPP"), "0", o.getString("OBSH"), "",
                                    "", "", "", "", "0","","","","" ,"0", "0", o.getString("CREATED"), o.getString("OBSI"));
                            loadOT.add(OT);
                            SqliteClass.getInstance(context).databasehelp.otsql.addOT(OT);
                        }
                    }
                }jParser = null; json = null;
                /* OT_OPERATION CLASS */
                loadOTOperation = new ArrayList<OTOperationsClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_OTOPERATIONS+ConstValue.getUserTypeString()+"/"+ConstValue.getSendCodeString()+"/";
                json = jParser.getJSONFromUrl(url);
                if(json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList =  json.getJSONArray("data");
                        System.out.println("Cloud PM - OTOPERATIONS "+jsonList.length());
                        for (int j=0; j<jsonList.length();j++){
                            JSONObject o =  jsonList.getJSONObject(j);
                            String status = "";
                            if(o.getString("DONE").equals("")){ status="0";}else{status=o.getString("DONE");}
                            OTOperation =  new OTOperationsClass(j,o.getString("AUFNR"),o.getString("VORNR"),o.getString("ARBPL"), "", o.getString("WERKS"),
                                    "", o.getString("OBJID"), o.getString("STEUS"), o.getString("LTXA1"), o.getString("BEGDAOP"), o.getString("BEGTIOP"),
                                    o.getString("ENDDAOP"), o.getString("ENDTIOP"), ConstValue.getUserCodeString(), status, o.getString("OPOBS"), o.getString("OPTIME"));
                            loadOTOperation.add(OTOperation);
                            SqliteClass.getInstance(context).databasehelp.otOperationsSql.addOTOperation(OTOperation);
                        }
                    }
                }jParser = null; json = null;
                /* OT_COMPONENTS CLASS */
                loadOTComponent = new ArrayList<OTComponentsClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_OTCOMPONENT+ConstValue.getUserTypeString()+"/"+ConstValue.getSendCodeString()+"/";
                json = jParser.getJSONFromUrl(url);
                if(json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList =  json.getJSONArray("data");
                        System.out.println("Cloud PM - OTCOMPONENT "+jsonList.length());
                        for (int j=0; j<jsonList.length();j++){
                            JSONObject o =  jsonList.getJSONObject(j);
                            String value = "";
                            String status = "";
                            if(o.getString("DONE").equals("")){ status="0";}else{status=o.getString("DONE");}
                            if(o.getString("MENGE").equals("None") || o.getString("MENGE").equals("null")){ value="0"; }else{ value=o.getString("MENGE"); }
                            OTComponent =  new OTComponentsClass(j,o.getString("AUFNR"),o.getString("VORNR"),o.getString("POSNR"), o.getString("OBJID"), o.getString("MATNR"),
                                    o.getString("MAKTX"), o.getString("MEINS_ISO"), o.getString("MEINS"), value, ConstValue.getUserCodeString(), status,
                                    o.getString("CPOBS"), o.getString("QUANTITY"));
                            loadOTComponent.add(OTComponent);
                            SqliteClass.getInstance(context).databasehelp.otComponentsSql.addOTComponent(OTComponent);
                        }
                    }
                }jParser = null; json = null;
                /* OT_MEASUREMENT_POINT CLASS */
                loadOTMeasurementPoint = new ArrayList<OTMeasurementPointClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_OTMEASURES+ConstValue.getUserTypeString()+"/"+ConstValue.getSendCodeString()+"/";
                json = jParser.getJSONFromUrl(url);
                if(json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList =  json.getJSONArray("data");
                        System.out.println("Cloud PM - OTMEASURES "+jsonList.length());
                        for (int j=0; j<jsonList.length();j++){
                            JSONObject obj =  jsonList.getJSONObject(j);
                            String status = "";
                            if(obj.getString("TAKEN").equals("")){ status="0";}else{status=obj.getString("TAKEN");}
                            OTMeasurementPoint =  new OTMeasurementPointClass(j,obj.getString("AUFNR"),obj.getString("POINT"),obj.getString("EQUNR"), obj.getString("NEQUNR"), obj.getString("OBJID"),
                                    "", "", status, obj.getString("PMOBS"), obj.getString("VALUE"));
                            loadOTComponent.add(OTComponent);
                            SqliteClass.getInstance(context).databasehelp.otMeasurementPointsSql.addMeasurementPoints(OTMeasurementPoint);
                        }
                    }
                }jParser = null; json = null;
                if(ConstValue.getUserTypeString().equals("C")) {
                    String dateStart = Util.getActualDateHour();
                    /* EQUIPMENT CLASS */
                    int correctRegister = 0;
                    int failureRegister = 0;
                    loadEquipmentClass = new ArrayList<EquipmentClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_EQUIPMENT + ConstValue.getSendCodeString() + "/";
                    json = jParser.getJSONFromUrl(url);
                    if (json.has("data")) {
                        if (json.get("data") instanceof JSONArray) {
                            JSONArray jsonList = json.getJSONArray("data");
                            for (int i = 0; i < jsonList.length(); i++) {
                                JSONObject obj = jsonList.getJSONObject(i);
                                try{
                                    EquipmentClass = new EquipmentClass(i, obj.getString("EQUNR"), obj.getString("OBJNR"), obj.getString("ABCKZ"),
                                            obj.getString("RBNR"), obj.getString("TPLNR"), obj.getString("SWERK"), obj.getString("STORT"), obj.getString("EQKTX"));
                                    loadEquipmentClass.add(EquipmentClass);
                                    SqliteClass.getInstance(context).databasehelp.equipmentSql.addEquipment(EquipmentClass);
                                    correctRegister++;
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                        }
                    }
                    String state = "1";
                    if(failureRegister > 0){ state = "0"; }
                    SynchronizationClass synchronizationClass = new SynchronizationClass(0, "Equipos", dateStart, Util.getActualDateHour(),
                            correctRegister+"", failureRegister+"", state);
                    SqliteClass.getInstance(context).databasehelp.synchronizationsql.addSynchronization(synchronizationClass);
                    jParser = null; json = null;

                    /* NOTICETYPE CLASS */
                    correctRegister = 0;
                    failureRegister = 0;
                    loadNoticeTypeClass = new ArrayList<NoticeTypeClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_NOTICETYPE;
                    json = jParser.getJSONFromUrl(url);
                    if (json.has("data")) {
                        if (json.get("data") instanceof JSONArray) {
                            JSONArray jsonList = json.getJSONArray("data");
                            for (int i = 0; i < jsonList.length(); i++) {
                                JSONObject obj = jsonList.getJSONObject(i);
                                try{
                                    NoticeTypeClass = new NoticeTypeClass(i, obj.getString("CODE"), obj.getString("DESCRIPTION"), obj.getString("STATUS"));
                                    loadNoticeTypeClass.add(NoticeTypeClass);
                                    SqliteClass.getInstance(context).databasehelp.noticeTypeSql.addNoticeType(NoticeTypeClass);
                                    correctRegister++;
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                        }
                    }
                    state = "1";
                    if(failureRegister > 0){ state = "0"; }
                    SynchronizationClass synchronizationClass2 = new SynchronizationClass(0, "Tipo de Avisos", dateStart, Util.getActualDateHour(),
                            correctRegister+"", failureRegister+"", state);
                    SqliteClass.getInstance(context).databasehelp.synchronizationsql.addSynchronization(synchronizationClass2);
                    jParser = null;json = null;

                    /* SYMPTOM CLASS */
                    correctRegister = 0;
                    failureRegister = 0;
                    loadSymptomClass = new ArrayList<SymptomClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_SYMPTOM + ConstValue.getSendCodeString() + "/";
                    json = jParser.getJSONFromUrl(url);
                    if (json.has("data")) {
                        if (json.get("data") instanceof JSONArray) {
                            JSONArray jsonList = json.getJSONArray("data");
                            for (int j = 0; j < jsonList.length(); j++) {
                                JSONObject tech = jsonList.getJSONObject(j);
                                try{
                                    SymptomClass = new SymptomClass(j, tech.getString("RBNR"), tech.getString("CODE"), tech.getString("CODEGRUPPE"), tech.getString("VERSION"), tech.getString("INAKTIV"), tech.getString("KURZTEXT"));
                                    loadSymptomClass.add(SymptomClass);
                                    SqliteClass.getInstance(context).databasehelp.symptomSql.addSymptom(SymptomClass);
                                    correctRegister++;
                                }
                                catch(Exception e){
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                        }
                    }
                    state = "1";
                    if(failureRegister > 0){ state = "0"; }
                    SynchronizationClass synchronizationClass3 = new SynchronizationClass(0, "Síntomas", dateStart, Util.getActualDateHour(),
                            correctRegister+"", failureRegister+"", state);
                    SqliteClass.getInstance(context).databasehelp.synchronizationsql.addSynchronization(synchronizationClass3);
                    jParser = null;json = null;

                    /* TECHNICALLOCATIONS CLASS */
                    correctRegister = 0;
                    failureRegister = 0;
                    loadTechnicalLocationsClass = new ArrayList<TechnicalLocationsClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_TECHNICALLOCATION + ConstValue.getSendCodeString() + "/";
                    json = jParser.getJSONFromUrl(url);
                    if (json.has("data")) {
                        if (json.get("data") instanceof JSONArray) {
                            JSONArray jsonList = json.getJSONArray("data");
                            for (int j = 0; j < jsonList.length(); j++) {
                                JSONObject tech = jsonList.getJSONObject(j);
                                try{
                                    TechnicalLocationsClass = new TechnicalLocationsClass(j, tech.getString("TPLNR"), tech.getString("PLTXT"), tech.getString("SWERK"));
                                    loadTechnicalLocationsClass.add(TechnicalLocationsClass);
                                    SqliteClass.getInstance(context).databasehelp.technicalLocationsSql.addTechnicalLocations(TechnicalLocationsClass);
                                    correctRegister++;
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                        }
                    }
                    state = "1";
                    if(failureRegister > 0){ state = "0"; }
                    SynchronizationClass synchronizationClass4 = new SynchronizationClass(0, "Locaciones Técnicas", dateStart, Util.getActualDateHour(),
                            correctRegister+"", failureRegister+"", state);
                    SqliteClass.getInstance(context).databasehelp.synchronizationsql.addSynchronization(synchronizationClass4);
                    jParser = null;json = null;


                    /* TICKET CLASS*/
                    ticketList = new ArrayList<SupportTicketClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_GTICKET+ConstValue.getSendCodeString()+"/";
                    json = jParser.getJSONFromUrl(url);
                    Log.i("JSON Ticket: ", json.toString());
                    if(json.has("data")){
                        if (json.get("data")instanceof JSONArray){
                            JSONArray jsonList =  json.getJSONArray("data");
                            for (int j=0; j<jsonList.length();j++){
                                JSONObject o =  jsonList.getJSONObject(j);
                                Ticket = new SupportTicketClass(j, o.getString("id"), o.getString("QMART"), o.getString("NQMART"), o.getString("TICNR"),
                                        o.getString("WERKS"), o.getString("NWERKS"), o.getString("EQUNR"), o.getString("NEQUNR"), o.getString("NDATE"),
                                        o.getString("NTIME"),o.getString("CODEGRUPPE"),o.getString("CODE"), o.getString("NCODE"), o.getString("REASON"),
                                        o.getString("STOPPED"), o.getString("APPROVAL"),"1");
                                ticketList.add(Ticket);
                                SqliteClass.getInstance(context).databasehelp.supportTicketSql.addSupportTicket(Ticket);
                            }
                        }
                    }


                }
            } catch (Exception e) {
                System.out.println("CloudPM "+ e);
                // TODO: handle exception
                responseString = "CloudPM - Error al recuperar la información del servidor.";
            }
            // TODO Auto-generated method stub
            return responseString;
        }
    }

}

