package com.pm.views.activities;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import com.pm.R;
import com.pm.adapters.MenuAdapter;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.imgLoader.JSONParser;
import com.pm.models.OTClass;
import com.pm.models.OTComponentsClass;
import com.pm.models.OTMeasurementPointClass;
import com.pm.models.OTOperationsClass;
import com.pm.utils.GpsTracker;
import com.pm.utils.Util;
import com.pm.views.fragments.HomeFragment;
import com.pm.views.fragments.OTClosingFragment;
import com.pm.views.fragments.ProfileFragment;
import com.pm.views.fragments.SyncFragment;
import com.pm.views.fragments.TicketFragment;
import com.pm.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends ActionBarActivity implements ActionBar.TabListener {
    ActionBar actionBar;
    public static final String TAG = "MainActivity";
    public Context context;

    private DrawerLayout mDrawerLayout;
    private LinearLayout mDeawerView;
    private ActionBarDrawerToggle mDrawerToggle;

    private OTClass OT; private ArrayList<OTClass> loadOT;
    private OTComponentsClass OTComponent; private ArrayList<OTComponentsClass> loadOTComponent;
    private OTMeasurementPointClass OTMeasurementPoint; private ArrayList<OTMeasurementPointClass> loadOTMeasurementPoint;
    private OTOperationsClass OTOperation; private ArrayList<OTOperationsClass> loadOTOperation;
    /*notificacion*/
    private NotificationCompat.Builder notification;
    private static final int uniqueID = 51623;
    /**/

    ArrayList<HashMap<String, Integer>> menuArray;
    ListView mListView;
    MenuAdapter mAdapter;

    public SharedPreferences settings;
    public ConnectionDetector cd;
    ProgressDialog dialogSynchronization;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        settings = getSharedPreferences(ConstValue.MAIN_PREF, 0);
        cd=new ConnectionDetector(this);
        actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(R.string.app_name);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        Util.enableLocation(MainActivity.this, context);

        //mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDeawerView = (LinearLayout)findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getSupportActionBar().setTitle("Home");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getSupportActionBar().setTitle("User");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        menuArray = new ArrayList<HashMap<String,Integer>>();
        HashMap<String, Integer> map = new HashMap<String, Integer>();

        map.put("option", R.string.navigation_order);
        map.put("image", R.drawable.ic_menu_order);
        menuArray.add(map);

        map = new HashMap<String, Integer>();
        map.put("option", R.string.navigation_perfil);
        map.put("image", R.drawable.ic_menu_ticket);
        menuArray.add(map);

        map = new HashMap<String, Integer>();
        map.put("option", R.string.navigation_order_closing);
        map.put("image", R.drawable.ic_menu_order_pending);
        menuArray.add(map);

        if(ConstValue.getUserTypeString().equals("C")){
            map = new HashMap<String, Integer>();
            map.put("option", R.string.navigation_ticket);
            map.put("image", R.drawable.ic_ticket);
            menuArray.add(map);

            map = new HashMap<String, Integer>();
            map.put("option", R.string.navigation_sync);
            map.put("image", R.drawable.ic_sync_blue);
            menuArray.add(map);

        }


        map = new HashMap<String, Integer>();
        map.put("option", R.string.navigation_logout);
        map.put("image", R.drawable.ic_sign_off);
        menuArray.add(map);

        mListView = (ListView) findViewById(R.id.list_navigability);
        mAdapter = new MenuAdapter(getApplicationContext(), menuArray);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                set_fragment_page(arg2);
            }
        });

        if (getIntent().hasExtra("fragment_position")) {
            set_fragment_page(getIntent().getExtras().getInt("fragment_position"));
        } else {
            if(ConstValue.getFragmentView().equals("Ticket")) {
                set_fragment_page(3);
            }else if(ConstValue.getFragmentView().equals("OrderClosing")){
                set_fragment_page(2);
            }else{
                set_fragment_page(0);
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...
        int id = item.getItemId();
        if (id == R.id.action_synchronization) {
            if(cd.isConnectingToInternet()) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_info);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("CloudPM");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("Está seguro en traer nuevas OT del servidor, las OT no trabajadas, serán actualizadas?");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        new dataTask().execute(true);
                    }
                });
                Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }else{
                Toast.makeText(context, getString(R.string.app_no_connection), Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onTabSelected(ActionBar.Tab arg0,
                              android.support.v4.app.FragmentTransaction arg1) {
        // TODO Auto-generated method stub
        set_fragment_page(Integer.parseInt(arg0.getTag().toString()));
    }

    @Override
    public void onTabUnselected(ActionBar.Tab arg0,
                                android.support.v4.app.FragmentTransaction arg1) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onTabReselected(ActionBar.Tab arg0,
                                android.support.v4.app.FragmentTransaction arg1) {
        // TODO Auto-generated method stub
    }
    public void set_fragment_page(int position){
        Fragment fragment = null;
        Intent intent = null;
        Bundle args;
        if(ConstValue.getUserTypeString().equals("C")){
            switch (position) {
                case 0:
                    fragment = new HomeFragment();
                    args = new Bundle();
                    fragment.setArguments(args);
                    break;
                case 1:
                    fragment = new ProfileFragment();
                    break;
                case 2:
                    fragment = new OTClosingFragment();
                    break;
                case 3:
                    fragment =  new TicketFragment();
                    break;
                case 4:
                    fragment =  new SyncFragment();
                    break;
                case 5:
                    Util.logout(MainActivity.this, context, "0");
                default:
                    break;
            }
        } else {
            switch (position) {
                case 0:
                    fragment = new HomeFragment();
                    if(ConstValue.getFragmentView().equals("Order")){
                        args = new Bundle();
                        fragment.setArguments(args);
                    }
                    break;
                case 1:
                    fragment = new ProfileFragment();
                    break;
                case 2:
                    fragment = new OTClosingFragment();
                    break;
                case 3:
                    Util.logout(MainActivity.this, context, "0");
                    break;
                default:
                    break;
            }
        }
        if (intent != null) {
            startActivity(intent);
        }
        if (fragment!=null) {
            // Insert the fragment by replacing any existing fragment
            FragmentManager fragmentManager = getFragmentManager();
            if (position==0) {
                fragmentManager.beginTransaction()
                        .replace(R.id.sample_content_fragment, fragment)
                        .commit();
            }else{
                fragmentManager.beginTransaction()
                        .replace(R.id.sample_content_fragment, fragment)
                        .addToBackStack(null)
                        .commit();
            }
            mDrawerLayout.closeDrawer(mDeawerView);
        }
    }

    @Override
    public void onBackPressed() {
        Util.logout(MainActivity.this, context, "1");
    }

    class dataTask extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialogSynchronization = ProgressDialog.show(MainActivity.this, "CloudPM", getString(R.string.action_loading), true);
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Toast.makeText(getApplicationContext(), "CloudPM: " + result, Toast.LENGTH_LONG).show();
            } else {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_info);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("CloudPM - SINCRONIZAR");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("Se actualizo la lista de Ordenes Trabajo");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });
                dialog.show();
            }
            // TODO Auto-generated method stub
            dialogSynchronization.dismiss();
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }
        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }
        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = null;
            ArrayList<String> listClear = new ArrayList<String>();
            listClear = SqliteClass.getInstance(context).databasehelp.otsql.getDeleteList();
            try {
                for(int i=0; i<listClear.size();i++){
                    SqliteClass.getInstance(context).databasehelp.otsql.deleteOT(listClear.get(i));
                    SqliteClass.getInstance(context).databasehelp.otComponentsSql.deleteOTComponents(listClear.get(i));
                    SqliteClass.getInstance(context).databasehelp.otOperationsSql.deleteOTOperation(listClear.get(i));
                    SqliteClass.getInstance(context).databasehelp.otMeasurementPointsSql.deleteOTMeasurementPoint(listClear.get(i));
                }
                /* OT CLASS */
                JSONParser jParser;
                JSONObject json;
                String url = "";
                loadOT = new ArrayList<OTClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_OTHEAD+ConstValue.getUserTypeString()+"/"+ConstValue.getSendCodeString()+"/";
                json = jParser.getJSONFromUrl(url);
                if(json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList =  json.getJSONArray("data");
                        for (int j=0; j<jsonList.length();j++){
                            JSONObject o =  jsonList.getJSONObject(j);
                            if(SqliteClass.getInstance(context).databasehelp.otsql.checkIfExists(o.getString("AUFNR"))==0) {
                                ConstValue.setOrderTechnicalString(o.getString("OBJID"));
                                OT = new OTClass(j, o.getString("AUFNR"), o.getString("AUART"), o.getString("NAUART"), o.getString("INGRP"), o.getString("NINGRP"),
                                        o.getString("WERKS"), o.getString("NWERKS"), o.getString("ARBPL"), o.getString("NARBPL"), o.getString("SOWRK"), o.getString("NSOWRK"),
                                        o.getString("OBJID"), o.getString("OBJID2"), o.getString("TPLNR"), o.getString("NTPLNR"), o.getString("KTEXT"), o.getString("GSTRP"), o.getString("GLTRP"),
                                        o.getString("BEGTI"), o.getString("ENDTI"), o.getString("NOTNR"), o.getString("PRIOK"), o.getString("NPRIOK"), o.getString("TPLNR"), o.getString("NTPLNR"),
                                        o.getString("EQUNR"), o.getString("NEQUNR"), ConstValue.getUserCodeString(), o.getString("TECHAPP"), "0", o.getString("OBSH"), "",
                                        "", "", "", "", "0","","","","", "0", "0",
                                        o.getString("CREATED"), o.getString("OBSI"));
                                loadOT.add(OT);
                                SqliteClass.getInstance(context).databasehelp.otsql.addOT(OT);
                            }
                        }
                    }
                }jParser = null; json = null;
                /* OT_OPERATION CLASS */
                loadOTOperation = new ArrayList<OTOperationsClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_OTOPERATIONS+ConstValue.getUserTypeString()+"/"+ConstValue.getSendCodeString()+"/";
                json = jParser.getJSONFromUrl(url);
                if(json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList =  json.getJSONArray("data");
                        for (int j=0; j<jsonList.length();j++){
                            JSONObject o =  jsonList.getJSONObject(j);
                            if(SqliteClass.getInstance(context).databasehelp.otsql.checkIfExists(o.getString("AUFNR"))==0) {
                                String status = "";
                                if (o.getString("DONE").equals("")) {
                                    status = "0";
                                } else {
                                    status = o.getString("DONE");
                                }
                                OTOperation = new OTOperationsClass(j, o.getString("AUFNR"), o.getString("VORNR"), o.getString("ARBPL"), "", o.getString("WERKS"),
                                        "", o.getString("OBJID"), o.getString("STEUS"), o.getString("LTXA1"), o.getString("BEGDAOP"), o.getString("BEGTIOP"),
                                        o.getString("ENDDAOP"), o.getString("ENDTIOP"), ConstValue.getUserCodeString(), status, o.getString("OPOBS"), o.getString("OPTIME"));
                                loadOTOperation.add(OTOperation);
                                SqliteClass.getInstance(context).databasehelp.otOperationsSql.addOTOperation(OTOperation);
                            }
                        }
                    }
                }jParser = null; json = null;
                /* OT_COMPONENTS CLASS */
                loadOTComponent = new ArrayList<OTComponentsClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_OTCOMPONENT+ConstValue.getUserTypeString()+"/"+ConstValue.getSendCodeString()+"/";
                json = jParser.getJSONFromUrl(url);
                if(json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList =  json.getJSONArray("data");
                        for (int j=0; j<jsonList.length();j++){
                            JSONObject o =  jsonList.getJSONObject(j);
                            if(SqliteClass.getInstance(context).databasehelp.otsql.checkIfExists(o.getString("AUFNR"))==0) {
                                String value = "";
                                String status = "";
                                if (o.getString("DONE").equals("")) {
                                    status = "0";
                                } else {
                                    status = o.getString("DONE");
                                }
                                if (o.getString("MENGE").equals("None") || o.getString("MENGE").equals("null")) {
                                    value = "0";
                                } else {
                                    value = o.getString("MENGE");
                                }
                                OTComponent = new OTComponentsClass(j, o.getString("AUFNR"), o.getString("VORNR"), o.getString("POSNR"), o.getString("OBJID"), o.getString("MATNR"),
                                        o.getString("MAKTX"), o.getString("MEINS_ISO"), o.getString("MEINS"), value, ConstValue.getUserCodeString(), status,
                                        o.getString("CPOBS"), o.getString("QUANTITY"));
                                loadOTComponent.add(OTComponent);
                                SqliteClass.getInstance(context).databasehelp.otComponentsSql.addOTComponent(OTComponent);
                            }
                        }
                    }
                }jParser = null; json = null;
                /* OT_MEASUREMENT_POINT CLASS */
                loadOTMeasurementPoint = new ArrayList<OTMeasurementPointClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_OTMEASURES+ConstValue.getUserTypeString()+"/"+ConstValue.getSendCodeString()+"/";
                json = jParser.getJSONFromUrl(url);
                if(json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList =  json.getJSONArray("data");
                        for (int j=0; j<jsonList.length();j++){
                            JSONObject obj =  jsonList.getJSONObject(j);
                            if(SqliteClass.getInstance(context).databasehelp.otsql.checkIfExists(obj.getString("AUFNR"))==0) {
                                String status = "";
                                if (obj.getString("TAKEN").equals("")) {
                                    status = "0";
                                } else {
                                    status = obj.getString("TAKEN");
                                }
                                OTMeasurementPoint = new OTMeasurementPointClass(j, obj.getString("AUFNR"), obj.getString("POINT"), obj.getString("EQUNR"), obj.getString("NEQUNR"), obj.getString("OBJID"),
                                        "", "", status, obj.getString("PMOBS"), obj.getString("VALUE"));
                                loadOTComponent.add(OTComponent);
                                SqliteClass.getInstance(context).databasehelp.otMeasurementPointsSql.addMeasurementPoints(OTMeasurementPoint);
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return responseString;
        }
    }
}
