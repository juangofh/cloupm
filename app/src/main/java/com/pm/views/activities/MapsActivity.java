package com.pm.views.activities;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pm.R;
import com.pm.db.SqliteClass;
import com.pm.models.CenterClass;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    List<CenterClass> centerList;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //CentarClass
        centerList =  new ArrayList<CenterClass>();
        centerList = SqliteClass.getInstance(context).databasehelp.centerSql.getAllItem();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        /*mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        */

        mMap = googleMap;
        LatLng honduras = new LatLng(15.18, -87.19);
        for(int z=0; z<centerList.size(); z++) {
            //Los datos de center Sergio

            LatLng customerMap = new LatLng(Double.parseDouble(centerList.get(z).getLatitude()), Double.parseDouble(centerList.get(z).getLongitude()));
            //if(centerList.get(z).get.equals("0")) {
            //mMap.addMarker(new MarkerOptions().position(customerMap).title(centerList.get(z).getDecriptionCenter()));
            //}else{
            //mMap.addMarker(new MarkerOptions().position(customerMap).title(centerList.get(z).getDecriptionCenter())
              //          .icon(centerList.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            //}
        }
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(honduras));

        // Add a marker in Sydney and move the camera
        /*LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/
    }
}