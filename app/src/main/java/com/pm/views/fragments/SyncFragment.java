package com.pm.views.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pm.R;
import com.pm.adapters.SpinnerSyncAdapter;
import com.pm.adapters.SynchronizationAdapter;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.imgLoader.JSONParser;
import com.pm.models.EquipmentClass;
import com.pm.models.NoticeTypeClass;
import com.pm.models.SymptomClass;
import com.pm.models.SynchronizationClass;
import com.pm.models.TechnicalLocationsClass;
import com.pm.utils.Common;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class SyncFragment extends Fragment {

    Activity activity;
    Context context;
    Common common;
    static ArrayList<HashMap<String, String>> data;
    ListView listView;
    ProgressDialog dialog;

    Spinner spSyncOptions;
    ArrayList<String> syncOptions;

    ListView itemListView;
    List<SynchronizationClass> itemList;

    View rootView;

    boolean isFABOpen = false, allChecked = false;
    FrameLayout flOrderNewEvent, flOrderPhoto;
    TextView tvOrderNewEvent, tvOrderPhoto;

    private EquipmentClass EquipmentClass; private ArrayList<EquipmentClass> loadEquipmentClass;
    private NoticeTypeClass NoticeTypeClass; private ArrayList<NoticeTypeClass> loadNoticeTypeClass;
    private SymptomClass SymptomClass; private ArrayList<SymptomClass> loadSymptomClass;
    private TechnicalLocationsClass TechnicalLocationsClass; private ArrayList<TechnicalLocationsClass> loadTechnicalLocationsClass;


    public SyncFragment() {}



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        syncOptions = new ArrayList<String>();
        syncOptions.add("Sincronizadas"); syncOptions.add("Por Sincronizar");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        common = new Common();
        activity = getActivity();
        rootView = inflater.inflate(R.layout.fragment_sync, container, false);
        data = new ArrayList<HashMap<String,String>>();
        itemListView = (ListView) rootView.findViewById(R.id.lst_classes_sync);

        itemList = SqliteClass.getInstance(context).databasehelp.synchronizationsql.getSynchronizationData();
        final TextView emptyTextView = (TextView) rootView.findViewById(android.R.id.empty);

        final FloatingActionButton fabEvent = (FloatingActionButton) rootView.findViewById(R.id.action_new_event_trip);
        flOrderNewEvent = (FrameLayout)rootView.findViewById(R.id.frame_order_new_event);
        flOrderPhoto = (FrameLayout)rootView.findViewById(R.id.frame_order_photo);
        tvOrderNewEvent = (TextView) rootView.findViewById(R.id.text_order_new_event); tvOrderNewEvent.setVisibility(View.GONE);
        tvOrderPhoto = (TextView) rootView.findViewById(R.id.text_order_potho); tvOrderPhoto.setVisibility(View.GONE);

        fabEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isFABOpen){
                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }
        });

        spSyncOptions = (Spinner) rootView.findViewById(R.id.sp_sync_options);
        spSyncOptions.setAdapter(new SpinnerSyncAdapter(context, R.layout.spinner_row, syncOptions));
        spSyncOptions.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);

        spSyncOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> spn, android.view.View v, int position, long id) {
                switch (position) {
                    case 0:
                        fabEvent.setVisibility(View.GONE); flOrderNewEvent.setVisibility(View.GONE); flOrderPhoto.setVisibility(View.GONE);//fabSync.setVisibility(View.GONE); syncFab.setVisibility(View.GONE);
                        itemList = SqliteClass.getInstance(context).databasehelp.synchronizationsql.getSynchronizationData();
                        getList();
                        if(itemList.size()>0) emptyTextView.setVisibility(View.GONE); else emptyTextView.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        fabEvent.setVisibility(View.VISIBLE); flOrderNewEvent.setVisibility(View.VISIBLE); flOrderPhoto.setVisibility(View.VISIBLE);
                        //Util.getClassesList(activity, itemListView);
                        ArrayList<String> classes = new ArrayList<String>();
                        classes.add("Equipos"); classes.add("Tipo de Avisos"); classes.add("Sintomas"); classes.add("Locaciones Tecnicas");
                        ArrayAdapter<String> adapter_multiple_choice = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_multiple_choice, classes);
                        itemListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                        itemListView.setAdapter(adapter_multiple_choice);

                        emptyTextView.setVisibility(View.GONE);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        flOrderNewEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(allChecked){
                    for ( int i=0; i < itemListView.getChildCount(); i++) {
                        itemListView.setItemChecked(i, false);
                    }
                    allChecked = false;
                }
                else{
                    for ( int i=0; i < itemListView.getChildCount(); i++) {
                        itemListView.setItemChecked(i, true);
                    }
                    allChecked = true;
                }
            }
        });

        flOrderPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(itemListView.getCheckedItemCount() > 0){
                    final SparseBooleanArray sparseBooleanArray = itemListView.getCheckedItemPositions();

                    if(sparseBooleanArray.get(0)) { ConstValue.setCheckedEquipments("1"); }
                    else { ConstValue.setCheckedEquipments("0"); }

                    if(sparseBooleanArray.get(1)) { ConstValue.setCheckedNoticeType("1"); }
                    else { ConstValue.setCheckedNoticeType("0"); }

                    if(sparseBooleanArray.get(2)) { ConstValue.setCheckedSymptom("1"); }
                    else { ConstValue.setCheckedSymptom("0"); }

                    if(sparseBooleanArray.get(3)) { ConstValue.setCheckedTechnicaLocation("1"); }
                    else { ConstValue.setCheckedTechnicaLocation("0"); }


                    String message = "";
                    if(sparseBooleanArray.get(0)) message += "\n - \tEquipos";
                    if(sparseBooleanArray.get(1)) message += "\n - \tTipo de Avisos";
                    if(sparseBooleanArray.get(2)) message += "\n - \tSintomas";
                    if(sparseBooleanArray.get(3)) message += "\n - \tLocaciones Tecnicas";

                    final Dialog dialog = new Dialog(activity);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.alert_info);
                    TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                    head.setText("CloudPM - Sincronizar Información");
                    TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);

                    content.setText("¿Esta seguro de descargar nueva información del servidor? \n" +
                            "Se descargaran los siguientes datos: \n" + message);

                    Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                    dbOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new loadTask().execute(true);
                            dialog.dismiss();
                        }
                    });
                    Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                    dbCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
                else{ Toast.makeText(context, "Seleccione alguna opción para sincronizar.", Toast.LENGTH_SHORT).show(); }
            }
        });


        return rootView;
    }


    public void getList() {
        data = new ArrayList<HashMap<String,String>>();
        itemListView.setChoiceMode(ListView.CHOICE_MODE_NONE);
        for(int z=0; z < itemList.size(); z++){
            SynchronizationClass cc = itemList.get(z);

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("id", cc.getId()+"");
            map.put("class_name", cc.getClassName());
            map.put("date_hour_start", cc.getDateHourStart());
            map.put("date_hour_end", cc.getDateHourEnd());
            map.put("registers", cc.getRegisterNumber());
            map.put("failures", cc.getFailureNumber());
            map.put("state", cc.getState());
            data.add(map);

        }
        SynchronizationAdapter adapter = new SynchronizationAdapter(activity, data);
        itemListView.setAdapter(adapter);
    }

    private void showFABMenu(){
        isFABOpen=true;
        flOrderNewEvent.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        tvOrderNewEvent.setVisibility(View.VISIBLE);
        flOrderPhoto.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
        tvOrderPhoto.setVisibility(View.VISIBLE);
    }

    private void closeFABMenu(){
        isFABOpen=false;
        flOrderNewEvent.animate().translationY(0);
        tvOrderNewEvent.setVisibility(View.GONE);
        flOrderPhoto.animate().translationY(0);
        tvOrderPhoto.setVisibility(View.GONE);
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    class loadTask extends AsyncTask<Boolean, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(activity, "", "Sincronizando, espere por favor...", true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Toast.makeText(activity, "CloudSales " + result, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(activity, "Sincronización exitosa.", Toast.LENGTH_LONG).show();
            }
            // TODO Auto-generated method stub
            dialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }

        @Override
        protected String doInBackground(Boolean... params) {

            String responseString = null;
            SimpleDateFormat dateFormatHour = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            SimpleDateFormat dateFormatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date date = new Date();
            String _date_sta = dateFormatDate.format(date);
            String _hour_sta = dateFormatHour.format(date);

            try {
                JSONParser jParser;
                JSONObject json;
                String url = "";
                if(ConstValue.getCheckedEquipments().equals("1")){

                    loadEquipmentClass =  new ArrayList<EquipmentClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_EQUIPMENT + ConstValue.getSendCodeString() + "/";;
                    json = jParser.getJSONFromUrl(url);
                    System.out.println("MI JSON EQUIPMENT: " + json);
                    if (json.has("data")){
                        if (json.get("data")instanceof JSONArray){
                            JSONArray jsonList = json.getJSONArray("data");
                            int correctRegister = 0;
                            int failureRegister = 0;
                            for(int j=0;j<jsonList.length(); j++){
                                JSONObject obj =  jsonList.getJSONObject(j);

                                //Ya no puede ser el ID
                                String idComing = obj.getString("EQUNR");
                                boolean exists = SqliteClass.getInstance(context).databasehelp.equipmentSql.checkIfExists(String.valueOf(idComing));
                                try{
                                    if(exists){
                                        System.out.println("ACTUALIZO EL REGISTRO EQUIPMENT");
                                        EquipmentClass = new EquipmentClass(0, obj.getString("EQUNR"), obj.getString("OBJNR"), obj.getString("ABCKZ"),
                                                obj.getString("RBNR"), obj.getString("TPLNR"), obj.getString("SWERK"), obj.getString("STORT"), obj.getString("EQKTX"));
                                        loadEquipmentClass.add(EquipmentClass);
                                        SqliteClass.getInstance(context).databasehelp.equipmentSql.updateEquipment(idComing, EquipmentClass);
                                    }
                                    else{
                                        System.out.println("DESCARGO UNO NUEVO EQUIPMENT");
                                        EquipmentClass = new EquipmentClass(SqliteClass.getInstance(context).databasehelp.equipmentSql.getAllItem().size(),
                                                obj.getString("EQUNR"), obj.getString("OBJNR"), obj.getString("ABCKZ"),
                                                obj.getString("RBNR"), obj.getString("TPLNR"), obj.getString("SWERK"),
                                                obj.getString("STORT"), obj.getString("EQKTX"));
                                        loadEquipmentClass.add(EquipmentClass);
                                        SqliteClass.getInstance(context).databasehelp.equipmentSql.addEquipment(EquipmentClass);
                                    }
                                    correctRegister++;
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                            Date _date = new Date();
                            String _date_end = dateFormatDate.format(_date);
                            String _hour_end = dateFormatHour.format(_date);
                            /** Crear la sincronizacion */
                            String state = "1";
                            if(failureRegister > 0){ state = "0"; }
                            SynchronizationClass synchronizationClass = new SynchronizationClass(0, "Equipos", _date_sta +" "+_hour_sta,
                                    _date_end+" "+_hour_end, correctRegister+"", failureRegister+"", state);
                            SqliteClass.getInstance(context).databasehelp.synchronizationsql.addSynchronization(synchronizationClass);
                        }
                    }
                }


                if(ConstValue.getCheckedNoticeType().equals("1")){
                    loadNoticeTypeClass =  new ArrayList<NoticeTypeClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_NOTICETYPE;
                    json = jParser.getJSONFromUrl(url);
                    System.out.println("MI JSON NOTYCETYPE: " + json);
                    if (json.has("data")){
                        if (json.get("data")instanceof JSONArray){
                            JSONArray jsonList = json.getJSONArray("data");
                            int correctRegister = 0;
                            int failureRegister = 0;
                            for(int j=0;j<jsonList.length(); j++){
                                JSONObject obj =  jsonList.getJSONObject(j);
                                String idComing = obj.getString("CODE");
                                boolean exists = SqliteClass.getInstance(context).databasehelp.noticeTypeSql.checkIfExists(idComing);
                                try{
                                    if(exists){
                                        System.out.println("ACTUALIZO EL REGISTRO NOTICETYPE");
                                        NoticeTypeClass = new NoticeTypeClass(0, obj.getString("CODE"), obj.getString("DESCRIPTION"), obj.getString("STATUS"));
                                        loadNoticeTypeClass.add(NoticeTypeClass);
                                        SqliteClass.getInstance(context).databasehelp.noticeTypeSql.updateNoticeType(idComing, NoticeTypeClass);
                                    }
                                    else{
                                        System.out.println("DESCARGO UNO NUEVO NOTICETYPE");
                                        NoticeTypeClass = new NoticeTypeClass(SqliteClass.getInstance(context).databasehelp.noticeTypeSql.getAllItem().size(), obj.getString("CODE"), obj.getString("DESCRIPTION"), obj.getString("STATUS"));
                                        loadNoticeTypeClass.add(NoticeTypeClass);
                                        SqliteClass.getInstance(context).databasehelp.noticeTypeSql.addNoticeType(NoticeTypeClass);
                                    }
                                    correctRegister++;
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                            Date _date = new Date();
                            String _date_end = dateFormatDate.format(_date);
                            String _hour_end = dateFormatHour.format(_date);

                            String state = "1";
                            if(failureRegister > 0){ state = "0"; }
                            SynchronizationClass synchronizationClass = new SynchronizationClass(0, "Tipo de Avisos", _date_sta +" "+_hour_sta,
                                    _date_end+" "+_hour_end, correctRegister+"", failureRegister+"", state);
                            SqliteClass.getInstance(context).databasehelp.synchronizationsql.addSynchronization(synchronizationClass);
                        }
                    }
                }

                if(ConstValue.getCheckedSymptom().equals("1")){
                    SqliteClass.getInstance(context).databasehelp.symptomSql.deleteSymptom();
                    loadSymptomClass =  new ArrayList<SymptomClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_SYMPTOM + ConstValue.getSendCodeString() + "/";
                    json = jParser.getJSONFromUrl(url);
                    System.out.println("MI JSON SYMPTOM: " + json);
                    if (json.has("data")){
                        if (json.get("data")instanceof JSONArray){
                            JSONArray jsonList = json.getJSONArray("data");
                            int correctRegister = 0;
                            int failureRegister = 0;

                            for (int j = 0; j < jsonList.length(); j++) {
                                JSONObject tech = jsonList.getJSONObject(j);
                                try{
                                    SymptomClass = new SymptomClass(j, tech.getString("RBNR"), tech.getString("CODE"), tech.getString("CODEGRUPPE"), tech.getString("VERSION"), tech.getString("INAKTIV"), tech.getString("KURZTEXT"));
                                    loadSymptomClass.add(SymptomClass);
                                    SqliteClass.getInstance(context).databasehelp.symptomSql.addSymptom(SymptomClass);
                                    correctRegister++;
                                }
                                catch(Exception e){
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                            /*
                            for(int j=0;j<jsonList.length(); j++){
                                JSONObject tech =  jsonList.getJSONObject(j);
                                String idComing = tech.getString("CODE");
                                boolean exists = SqliteClass.getInstance(context).databasehelp.symptomSql.checkIfExists(String.valueOf(idComing));
                                try{
                                    if(exists){
                                        System.out.println("ACTUALIZO EL REGISTRO SYMPTOM");
                                        SymptomClass = new SymptomClass(0, tech.getString("RBNR"), tech.getString("CODE"), tech.getString("CODEGRUPPE"),
                                                tech.getString("VERSION"), tech.getString("INAKTIV"), tech.getString("KURZTEXT"));
                                        loadSymptomClass.add(SymptomClass);
                                        SqliteClass.getInstance(context).databasehelp.symptomSql.updateSymptom(idComing, SymptomClass);
                                    }
                                    else{
                                        System.out.println("DESCARGO UN NUEVO SYMPTOM");
                                        SymptomClass = new SymptomClass(SqliteClass.getInstance(context).databasehelp.symptomSql.getAllItem().size(), tech.getString("RBNR"), tech.getString("CODE"), tech.getString("CODEGRUPPE"),
                                                tech.getString("VERSION"), tech.getString("INAKTIV"), tech.getString("KURZTEXT"));
                                        SqliteClass.getInstance(context).databasehelp.symptomSql.addSymptom(SymptomClass);
                                    }
                                    correctRegister++;
                                }
                                catch(Exception e){
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                            */

                            Date _date = new Date();
                            String _date_end = dateFormatDate.format(_date);
                            String _hour_end = dateFormatHour.format(_date);

                            String state = "1";
                            if(failureRegister > 0){ state = "0"; }
                            SynchronizationClass synchronizationClass = new SynchronizationClass(0, "Síntomas", _date_sta +" "+_hour_sta,
                                    _date_end+" "+_hour_end, correctRegister+"", failureRegister+"", state);
                            SqliteClass.getInstance(context).databasehelp.synchronizationsql.addSynchronization(synchronizationClass);
                        }
                    }
                }

                if(ConstValue.getCheckedTechnicaLocation().equals("1")){
                    loadTechnicalLocationsClass =  new ArrayList<TechnicalLocationsClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_TECHNICALLOCATION + ConstValue.getSendCodeString() + "/";
                    json = jParser.getJSONFromUrl(url);
                    System.out.println("MI JSON PRICE LIST: " + json);
                    if (json.has("data")){
                        if (json.get("data")instanceof JSONArray){
                            JSONArray jsonList = json.getJSONArray("data");
                            int correctRegister = 0;
                            int failureRegister = 0;

                            for(int j=0;j<jsonList.length(); j++){
                                JSONObject tech =  jsonList.getJSONObject(j);
                                String idComing = tech.getString("TPLNR");
                                boolean exists = SqliteClass.getInstance(context).databasehelp.technicalLocationsSql.checkIfExists(idComing);
                                try{
                                    if(exists){
                                        System.out.println("ACTUALIZO EL REGISTRO TECHNICAL LOCATION");
                                        TechnicalLocationsClass = new TechnicalLocationsClass(0, tech.getString("TPLNR"), tech.getString("PLTXT"), tech.getString("SWERK"));
                                        loadTechnicalLocationsClass.add(TechnicalLocationsClass);
                                        SqliteClass.getInstance(context).databasehelp.technicalLocationsSql.updateTechnicalLocations(idComing, TechnicalLocationsClass);
                                    }
                                    else{
                                        System.out.println("DESCARGO UNO TECHNICAL");
                                        TechnicalLocationsClass = new TechnicalLocationsClass(SqliteClass.getInstance(context).databasehelp.technicalLocationsSql.getAllItem().size(), tech.getString("TPLNR"), tech.getString("PLTXT"), tech.getString("SWERK"));
                                        loadTechnicalLocationsClass.add(TechnicalLocationsClass);
                                        SqliteClass.getInstance(context).databasehelp.technicalLocationsSql.addTechnicalLocations(TechnicalLocationsClass);
                                    }
                                    correctRegister++;
                                }
                                catch(Exception e){
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                            Date _date = new Date();
                            String _date_end = dateFormatDate.format(_date);
                            String _hour_end = dateFormatHour.format(_date);

                            String state = "1";
                            if(failureRegister > 0){ state = "0"; }
                            SynchronizationClass synchronizationClass = new SynchronizationClass(0, "Locaciones Técnicas", _date_sta +" "+_hour_sta,
                                    _date_end+" "+_hour_end, correctRegister+"", failureRegister+"", state);
                            SqliteClass.getInstance(context).databasehelp.synchronizationsql.addSynchronization(synchronizationClass);
                        }
                    }

                }
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
                responseString = "CloudSales - Error al recuperar la información del portal.";
            }
            // TODO Auto-generated method stub
            return responseString;
        }
    }
}
