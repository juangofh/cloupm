package com.pm.views.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pm.R;
import com.pm.adapters.OTDetailAdapter;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.models.OTComponentsClass;
import com.pm.utils.InputFilterMinMax;
import com.pm.utils.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OTComponentFragment extends Fragment {
    List<OTComponentsClass> itemList;
    static ArrayList<HashMap<String, String>> itemArray;
    OTDetailAdapter adapter;
    ListView itemListView;
    TextView empty;
    public Activity activity;
    public Context context;
    public View rootView;

    private static final String TAG = "OTComponentFragment";
    public String type = "Componentes";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ot_component, container, false);
        activity =getActivity();
        context = getContext();

        itemArray = new ArrayList<HashMap<String,String>>();
        itemList = new ArrayList<OTComponentsClass>();
        itemList = SqliteClass.getInstance(context).databasehelp.otComponentsSql.getOrderNumber(ConstValue.getOrderNumberString());
        for(int z=0; z < itemList.size(); z++){
            OTComponentsClass cc = itemList.get(z);
            Double _quantity = Double.parseDouble(cc.getQuantity());
            int quantity = _quantity.intValue();
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("id", String.valueOf(cc.getId()));
            map.put("name","[ "+ cc.getQuantity() +" "+cc.getUnitMeasure() +" ] - "+cc.getItemDescription());
            map.put("quantity", String.valueOf(quantity));
            map.put("description",cc.getItemCode());
            map.put("status", cc.getStatus());
            itemArray.add(map);
        }
        itemListView = (ListView) rootView.findViewById(R.id.listComponent);
        empty = (TextView) rootView.findViewById(R.id.emptyComponent);
        adapter = new OTDetailAdapter(activity, itemArray);
        itemListView.setAdapter(adapter);
        if(itemList.size()>0){empty.setVisibility(rootView.GONE);} else {empty.setVisibility(rootView.VISIBLE);}

        itemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                // TODO Auto-generated method stub
                if(ConstValue.getUserView()==1){
                    Toast.makeText(getContext(),"Modo VISUALIZAR activado, no se permite registrar datos.",Toast.LENGTH_SHORT).show();
                    dialogText(1, Integer.parseInt(itemArray.get(arg2).get("id")), itemArray.get(arg2).get("quantity"));
                }else{
                    String _r = SqliteClass.getInstance(context).databasehelp.otComponentsSql.getOTComponent(Integer.parseInt(itemArray.get(arg2).get("id")), "DONE");
                    if(_r.equals("1")){
                        final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.alert_info);
                        ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                        image.setImageResource(R.drawable.ic_alert_info);
                        TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                        head.setText("CLOUD PM - COMPONENTES");
                        TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                        content.setText("El registro ya contiene información registrada, que desea hacer con la información?");
                        Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                        dbOk.setText("EDITAR");
                        dbOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogText(1, Integer.parseInt(itemArray.get(arg2).get("id")), itemArray.get(arg2).get("quantity"));
                                dialog.dismiss();
                            }
                        });
                        Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                        dbCancel.setText("REINICIAR");
                        dbCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SqliteClass.getInstance(context).databasehelp.otComponentsSql.updateOTComponent(Integer.parseInt(itemArray.get(arg2).get("id")), "QUANTITY", "");
                                SqliteClass.getInstance(context).databasehelp.otComponentsSql.updateOTComponent(Integer.parseInt(itemArray.get(arg2).get("id")), "DONE", "0");
                                SqliteClass.getInstance(context).databasehelp.otComponentsSql.updateOTComponent(Integer.parseInt(itemArray.get(arg2).get("id")), "CPOBS", "");
                                Toast.makeText(getContext(),"Información reiniciada con éxito.",Toast.LENGTH_SHORT).show();
                                listData();
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }else{
                        dialogText(0, Integer.parseInt(itemArray.get(arg2).get("id")), itemArray.get(arg2).get("quantity"));
                    }
                }
            }
        });

        return rootView;
    }

    private void dialogText(int action, final int id, final String quantity){
        String observation = "";
        String value="";
        if(action == 1){
            value = SqliteClass.getInstance(context).databasehelp.otComponentsSql.getOTComponent(id, "QUANTITY");
        }
        observation = SqliteClass.getInstance(context).databasehelp.otComponentsSql.getOTComponent(id, "CPOBS");
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_otcomponent);
        ImageView image = (ImageView) dialog.findViewById(R.id.alert_image);
        image.setImageResource(R.drawable.ic_alert_info);
        TextView content = (TextView) dialog.findViewById(R.id.alert_title);
        content.setText("CLOUD PM - COMPONENTES");
        final EditText etValue = (EditText) dialog.findViewById(R.id.etQuantity);
        etValue.setText(value);
        etValue.setFilters(new InputFilter[]{ new InputFilterMinMax("0", quantity)});
        final EditText etObservation = (EditText) dialog.findViewById(R.id.etObservation);
        etObservation.setText(observation);
        Button dialogOk = (Button) dialog.findViewById(R.id.alert_ok);
        dialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ConstValue.getUserView()==1){ dialog.dismiss(); }else{
                    String _value = etValue.getText().toString();
                    String _observation = etObservation.getText().toString();
                    if(_value.isEmpty()){}else{
                        SqliteClass.getInstance(context).databasehelp.otComponentsSql.updateOTComponent(id, "QUANTITY", _value);
                        SqliteClass.getInstance(context).databasehelp.otComponentsSql.updateOTComponent(id, "DONE", "1");
                    }if(_observation.equals("")){}else{
                        SqliteClass.getInstance(context).databasehelp.otComponentsSql.updateOTComponent(id, "CPOBS", _observation);
                    }
                    Toast.makeText(getContext(),"Información guardada con éxito.",Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    listData();
                }
            }
        });
        Button dialogCancel = (Button) dialog.findViewById(R.id.alert_cancel);
        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void listData(){
        itemArray = new ArrayList<HashMap<String,String>>();
        itemList = new ArrayList<OTComponentsClass>();
        itemList = SqliteClass.getInstance(context).databasehelp.otComponentsSql.getOrderNumber(ConstValue.getOrderNumberString());
        for(int z=0; z < itemList.size(); z++){
            OTComponentsClass cc = itemList.get(z);
            Double _quantity = Double.parseDouble(cc.getQuantity());
            int quantity = _quantity.intValue();
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("id", String.valueOf(cc.getId()));
            map.put("name","[ "+ cc.getQuantity() +" "+cc.getUnitMeasure() +" ] - "+cc.getItemDescription());
            map.put("quantity", String.valueOf(quantity));
            map.put("description",cc.getItemCode());
            map.put("status", cc.getStatus());
            itemArray.add(map);
        }
        adapter = new OTDetailAdapter(activity, itemArray);
        itemListView.setAdapter(adapter);
        if(itemList.size()>0){empty.setVisibility(rootView.GONE);} else {empty.setVisibility(rootView.VISIBLE);}
    }

}
