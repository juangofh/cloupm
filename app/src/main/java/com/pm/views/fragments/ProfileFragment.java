package com.pm.views.fragments;

import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pm.db.SqliteClass;
import com.pm.models.UserClass;
import com.pm.utils.Common;
import com.pm.utils.ConnectionDetector;
import com.pm.config.ConstValue;
import com.pm.R;
import com.pm.views.activities.LoginActivity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint("NewApi")
public class ProfileFragment extends Fragment {
	Activity activity;
	Context context;
	public ConnectionDetector cd;
	TextView user, code, name, type, dateConnection;
	String userId, userNewPassword;
	Button btnUpdate;
	Common common;
	List<UserClass> userList;
    ProgressDialog dialogPasswordUpdate;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	public void onDetach() {
		super.onDetach();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.fragment_profile, container, false);
		activity =getActivity();
		//context= getContext();
		common = new Common();
		cd=new ConnectionDetector(activity);
		user = (TextView) layout.findViewById(R.id.tv_user);
		code = (TextView) layout.findViewById(R.id.tv_code);
		name = (TextView) layout.findViewById(R.id.tv_name);
		type = (TextView) layout.findViewById(R.id.tv_type);
		dateConnection = (TextView) layout.findViewById(R.id.tv_date);
		userList = new ArrayList<UserClass>();
		userList = SqliteClass.getInstance(context).databasehelp.usersql.getUserData();
		user.setText(userList.get(0).getUser());
		if(ConstValue.getUserTypeString().equals("T")) {
			code.setText(userList.get(0).getCode());
		}else{
			code.setText(userList.get(0).getCenter());
		}
        name.setText(userList.get(0).getName()+ ", "+userList.get(0).getLastname());
		if(ConstValue.getUserTypeString().equals("C")){
			type.setText("Gerente de Tienda");
		}else{
			type.setText("Técnico");
		}
		dateConnection.setText(userList.get(0).getLastConnection());
		btnUpdate = (Button)layout.findViewById(R.id.btn_update);
		btnUpdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(cd.isConnectingToInternet()){
					final Dialog dialog = new Dialog(getActivity());
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.alert_change_password);
					TextView content = (TextView) dialog.findViewById(R.id.alert_rtn_content);
					content.setText("CloudPM - Cambio de Contraseña");
					final TextView passOld = (TextView) dialog.findViewById(R.id.et_pass_old);
					final String _passOld = userList.get(0).getPassword();
					final TextView passNew = (TextView) dialog.findViewById(R.id.et_pass_new);
					Button dialogOk = (Button) dialog.findViewById(R.id.alert_ok_pass);
					dialogOk.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							String tv_passOld = passOld.getText().toString();
							userNewPassword = passNew.getText().toString();
							if (tv_passOld.equals("") || userNewPassword.equals("")) {
								Toast.makeText(getActivity(), "Las contraseñas no pueden estar vacías.", Toast.LENGTH_SHORT).show();
							} else {
								if (_passOld.equals(tv_passOld)) {
									dialog.dismiss();
									new passwordUpdateTask().execute(true);
								} else {
									Toast.makeText(getActivity(), "La contraseña anterior no es correcta.", Toast.LENGTH_SHORT).show();
								}
							}
						}
					});
					Button dialogCancel = (Button) dialog.findViewById(R.id.alert_cancel_pass);
					dialogCancel.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
						}
					});
					dialog.show();
				}else{
					Toast.makeText(getActivity(), getString(R.string.app_no_connection), Toast.LENGTH_SHORT).show();
				}
			}
		});
		return layout;
	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_order, menu);
		menu.removeItem(R.id.action_synchronization);
		menu.removeItem(R.id.action_logout);
		super.onCreateOptionsMenu(menu, inflater);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

    class passwordUpdateTask extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            userId = String.valueOf(userList.get(0).getId());
            dialogPasswordUpdate = ProgressDialog.show(getActivity(), "CloudPM", getString(R.string.action_loading), true);
			super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String result) {
			if (result != null) {
                Toast.makeText(getActivity(), "CloudPM " + result, Toast.LENGTH_LONG).show();
            } else {
                final Dialog success = new Dialog(getActivity());
				success.requestWindowFeature(Window.FEATURE_NO_TITLE);
				success.setContentView(R.layout.alert_info);
                ImageView image = (ImageView) success.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) success.findViewById(R.id.alert_info_title);
                head.setText("CloudPM - SINCRONIZAR");
                TextView content = (TextView) success.findViewById(R.id.alert_info_content);
                content.setText("Contraseña cambiada con éxito, será redirigido al login.");
                Button dbOk = (Button) success.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
						success.dismiss();
                        Intent login=new Intent(activity, LoginActivity.class);
                        login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(login);
                        activity.finish();
                    }
                });

                Button dbCancel = (Button) success.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
						success.dismiss();
                        Intent login=new Intent(activity, LoginActivity.class);
                        login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(login);
                        activity.finish();
                    }
                });
				success.show();
            }
            // TODO Auto-generated method stub
            dialogPasswordUpdate.dismiss();
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }
        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }
        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = null;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("id",userId));
            nameValuePairs.add(new BasicNameValuePair("password",userNewPassword));
            JSONObject jObj = common.sendJsonData(ConstValue.JSON_UPDATE, nameValuePairs);
            try {
                if (jObj.getString("response").equalsIgnoreCase("success")) {
                    SqliteClass.getInstance(context).databasehelp.usersql.updateUserPass(userNewPassword);
					SqliteClass.getInstance(context).databasehelp.usersql.update("0");
                }else {
                    Toast.makeText(getActivity(), "Error al procesar la solicitud, "+userId, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return responseString;
        }
    }
}
