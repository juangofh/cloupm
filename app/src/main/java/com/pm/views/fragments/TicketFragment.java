package com.pm.views.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pm.R;
import com.pm.adapters.TicketAdapter;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.imgLoader.JSONParser;
import com.pm.models.SupportTicketClass;
import com.pm.utils.ConnectionDetector;
import com.pm.views.activities.MainActivity;
import com.pm.views.activities.TicketActivity;
import com.pm.views.activities.TicketDActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TicketFragment extends Fragment {
    List<SupportTicketClass> itemList;
    List<SupportTicketClass> itemLoad;
    TicketAdapter tAdapter;
    static ArrayList<HashMap<String, String>> itemArray;
    ListView listTickets;
    TextView empty;
    Activity activity;
    Context context;
    ProgressDialog dialogSync;
    private SupportTicketClass ticket;
    public ConnectionDetector cd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_ticket, container, false);
        activity = getActivity();
        //context = getActivity().getApplicationContext();
        cd=new ConnectionDetector(activity);
        itemArray = new ArrayList<HashMap<String,String>>();
        itemList = new ArrayList<SupportTicketClass>();
        itemList = SqliteClass.getInstance(getActivity()).databasehelp.supportTicketSql.getAllItem();
        for (int z=0; z<itemList.size();z++){
            SupportTicketClass cc= itemList.get(z);
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("id", String.valueOf(cc.getId()));
            map.put("noticeTypeN",cc.getNnoticeType());
            map.put("ticketNumber",cc.getTicketNumber());
            map.put("equipment",cc.getEquipment());
            map.put("equipmentName",cc.getNequipment());
            map.put("center",cc.getCenter());
            map.put("location",cc.getLocation());
            map.put("team",cc.getEquipment());
            map.put("noticeDate",cc.getNoticeDate());
            map.put("noticeHour",cc.getNoticeHour());
            map.put("symptomGroupCode",cc.getSymptomGroupCode());
            map.put("symptomGroupSymptom",cc.getSymptomGroupSymptom());
            map.put("stoppedEquipment",cc.getStoppedEquipment());
            map.put("approval",cc.getApproval());
            itemArray.add(map);
        }
        listTickets = (ListView) layout.findViewById(android.R.id.list);
        empty=(TextView) layout.findViewById(R.id.empty);
        tAdapter= new TicketAdapter(activity,itemArray);
        listTickets.setAdapter(tAdapter);
        if(itemList.size()>0){empty.setVisibility(layout.GONE);} else {empty.setVisibility(layout.VISIBLE);}
        listTickets.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int arg2, long id) {
                ConstValue.setNumberTicket(itemList.get(arg2).getTicketNumber());
                ConstValue.setTnotice(itemList.get(arg2).getNnoticeType());
                ConstValue.setEquipmentTicket(itemList.get(arg2).getNequipment());
                ConstValue.setLocationTicket(itemList.get(arg2).getNlocation());
                ConstValue.setDateTicket(itemList.get(arg2).getNoticeDate()+" "+itemList.get(arg2).getNoticeHour());
                ConstValue.setSymptomTicket(itemList.get(arg2).getNsymptomGroupSymptom());
                ConstValue.setAproval(itemList.get(arg2).getApproval());
                ConstValue.setReason(itemList.get(arg2).getCauseText());
                ConstValue.setLoadTicket(itemList.get(arg2).getLoad());
                ConstValue.setFragmentView("Ticket");
                // TODO Auto-generated method stub
                activity.finish();
                Intent intent = new Intent(activity,TicketDActivity.class);
                startActivityForResult(intent,100);
            }
        });
        return layout;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_ticket, menu);
        menu.removeItem(R.id.action_synchronization);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_ticket_sync) {
            if(cd.isConnectingToInternet()) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_ticket);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_send);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) dialog.findViewById(R.id.alert_ticket_title);
                head.setText("CloudPM - TICKET");
                TextView content = (TextView) dialog.findViewById(R.id.alert_ticket_content);
                content.setText("¿Seguro de actualizar la lista de ticket?");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ConstValue.setFragmentView("Ticket");
                        dialog.dismiss();
                        new ticketTask().execute(true);

                    }
                });
                Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }else{
                Toast.makeText(getActivity(), getString(R.string.app_no_connection), Toast.LENGTH_SHORT).show();
            }
        }
        if (id == R.id.action_add) {
            if(cd.isConnectingToInternet()) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_ticket);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_send);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) dialog.findViewById(R.id.alert_ticket_title);
                head.setText("CLOUD PM - TICKET");
                TextView content = (TextView) dialog.findViewById(R.id.alert_ticket_content);
                content.setText("¿Seguro de generar un ticket?");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setText("GENERAR");
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ConstValue.setFragmentView("Ticket");
                        activity.finish();
                        Intent intent = new Intent(activity, TicketActivity.class);
                        startActivityForResult(intent, 100);
                        dialog.dismiss();
                    }
                });
                Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }else{
                Toast.makeText(getActivity(), getString(R.string.app_no_connection), Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    class ticketTask extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialogSync = ProgressDialog.show(getActivity(), "CloudPM", getString(R.string.action_loading), true);
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Toast.makeText(getActivity(), "CloudPM " + result, Toast.LENGTH_LONG).show();
            } else {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_info);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("CloudPM - SINCRONIZAR");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("Se actualizo la lista de Ticket");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(activity, MainActivity.class);
                        startActivity(intent);
                    }
                });
                Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(activity, MainActivity.class);
                        startActivity(intent);
                    }
                });
                dialog.show();
            }
            // TODO Auto-generated method stub
            dialogSync.dismiss();
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }
        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }
        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = null;
            SqliteClass.getInstance(context).databasehelp.supportTicketSql.deleteTicket();
            try {
                JSONParser jParser;
                JSONObject json;
                String url = "";
                itemLoad = new ArrayList<SupportTicketClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_GTICKET+ConstValue.getSendCodeString()+"/";
                json = jParser.getJSONFromUrl(url);
                if(json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList =  json.getJSONArray("data");
                        for (int j=0; j<jsonList.length();j++){
                            JSONObject o =  jsonList.getJSONObject(j);
                            ticket = new SupportTicketClass(j, o.getString("id"), o.getString("QMART"), o.getString("NQMART"), o.getString("TICNR"),
                                    o.getString("WERKS"), o.getString("NWERKS"), o.getString("EQUNR"), o.getString("NEQUNR"), o.getString("NDATE"),
                                    o.getString("NTIME"),o.getString("CODEGRUPPE"),o.getString("CODE"), o.getString("NCODE"), o.getString("REASON"),
                                    o.getString("STOPPED"), o.getString("APPROVAL"),"1");
                            itemLoad.add(ticket);
                            SqliteClass.getInstance(context).databasehelp.supportTicketSql.addSupportTicket(ticket);
                        }
                    }
                }
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return responseString;
        }
    }
}
