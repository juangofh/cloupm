package com.pm.views.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pm.R;
import com.pm.adapters.OTDetailAdapter;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.models.OTOperationsClass;
import com.pm.utils.InputFilterMinMax;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OTOperationFragment extends Fragment {
    List<OTOperationsClass> itemList;
    private ArrayAdapter<String> mAdapter;
    static ArrayList<HashMap<String, String>> itemArray;
    OTDetailAdapter adapter;
    ListView itemListView;
    TextView empty;
    public Activity activity;
    public Context context;
    public View rootView;

    private static final String TAG = "OTOperationFragment";
    public String type = "Tareas";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_ot_operation, container, false);

        activity =getActivity();
        context = getContext();
        itemArray = new ArrayList<HashMap<String,String>>();
        itemList = new ArrayList<OTOperationsClass>();
        itemList = SqliteClass.getInstance(context).databasehelp.otOperationsSql.getOrderNumber(ConstValue.getOrderNumberString());
        for(int z=0; z < itemList.size(); z++){
            OTOperationsClass cc = itemList.get(z);
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("id", String.valueOf(cc.getId()));
            map.put("name", cc.getShortText());
            map.put("description",cc.getStartDate()+" "+cc.getStartTime()+" - "+cc.getEndDate()+" "+cc.getEndTime());
            map.put("status", cc.getStatus());
            itemArray.add(map);
        }
        itemListView = (ListView) rootView.findViewById(R.id.listOperation);
        empty = (TextView) rootView.findViewById(R.id.emptyOperation);
        adapter = new OTDetailAdapter(activity, itemArray);
        itemListView.setAdapter(adapter);
        if(itemList.size()>0){empty.setVisibility(rootView.GONE);} else {empty.setVisibility(rootView.VISIBLE);}
        itemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
                // TODO Auto-generated method stub
                if(ConstValue.getUserView()==1){
                    Toast.makeText(getContext(),"Modo VISUALIZAR activado, no se permite registrar datos.",Toast.LENGTH_SHORT).show();
                    String _r = SqliteClass.getInstance(context).databasehelp.otOperationsSql.getOTOperation(Integer.parseInt(itemArray.get(arg2).get("id")), "DONE");
                    if(_r.equals("1")){
                        dialogText(1, Integer.parseInt(itemArray.get(arg2).get("id")));
                    }else{
                        dialogText(0, Integer.parseInt(itemArray.get(arg2).get("id")));
                    }
                }else{
                    String _r = SqliteClass.getInstance(context).databasehelp.otOperationsSql.getOTOperation(Integer.parseInt(itemArray.get(arg2).get("id")), "DONE");
                    if(_r.equals("1")){
                        final Dialog dialog = new Dialog(context);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.alert_info);
                        ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                        image.setImageResource(R.drawable.ic_alert_info);
                        TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                        head.setText("CLOUD PM - TAREAS");
                        TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                        content.setText("El registro ya contiene información registrada, que desea hacer con la información?");
                        Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                        dbOk.setText("EDITAR");
                        dbOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogText(1, Integer.parseInt(itemArray.get(arg2).get("id")));
                                dialog.dismiss();
                            }
                        });
                        Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                        dbCancel.setText("REINICIAR");
                        dbCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SqliteClass.getInstance(context).databasehelp.otOperationsSql.updateOTOperation(Integer.parseInt(itemArray.get(arg2).get("id")), "OPTIME", "");
                                SqliteClass.getInstance(context).databasehelp.otOperationsSql.updateOTOperation(Integer.parseInt(itemArray.get(arg2).get("id")), "DONE", "0");
                                SqliteClass.getInstance(context).databasehelp.otOperationsSql.updateOTOperation(Integer.parseInt(itemArray.get(arg2).get("id")), "OPOBS", "");
                                Toast.makeText(getContext(),"Información reiniciada con éxito.",Toast.LENGTH_SHORT).show();
                                listData();
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }else{
                        dialogText(0, Integer.parseInt(itemArray.get(arg2).get("id")));
                    }
                }
            }
        });
        return rootView;
    }

    private void dialogText(int action, final int id){
        String observation = "";
        String value=""; String hour=""; String minute="";
        String[] _value = value.split("\\.");
        if(action == 1){
            value = SqliteClass.getInstance(context).databasehelp.otOperationsSql.getOTOperation(id, "OPTIME");
            _value = value.split("\\.");
            hour = _value[0];
            minute = _value[1];
        }

        observation = SqliteClass.getInstance(context).databasehelp.otOperationsSql.getOTOperation(id, "OPOBS");
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_otoperation);
        ImageView image = (ImageView) dialog.findViewById(R.id.alert_image);
        image.setImageResource(R.drawable.ic_alert_info);
        TextView content = (TextView) dialog.findViewById(R.id.alert_title);
        content.setText("CLOUD PM - TAREAS");
        final EditText etHour = (EditText) dialog.findViewById(R.id.etHour);
        etHour.setText(hour);
        etHour.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "12")});
        final EditText etMinute = (EditText) dialog.findViewById(R.id.etMinute);
        etMinute.setText(minute);
        etMinute.setFilters(new InputFilter[]{ new InputFilterMinMax("0", "59")});
        final EditText etObservation = (EditText) dialog.findViewById(R.id.etObservation);
        etObservation.setText(observation);
        Button dialogOk = (Button) dialog.findViewById(R.id.alert_ok);
        dialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ConstValue.getUserView()==1){ dialog.dismiss(); }else {
                    if (etMinute.getText().toString().length()==1) {
                        etMinute.setText("0"+etMinute.getText());
                    }

                    String _value = "";
                    String _hour = etHour.getText().toString();
                    String _minute = etMinute.getText().toString();
                    String _observation = etObservation.getText().toString();
                    if (_hour.isEmpty() && _minute.isEmpty()) {
                        _value = "";
                    } else if (_hour.isEmpty() && !_minute.isEmpty()) {
                        _value = "0." + _minute;
                    } else if (!_hour.isEmpty() && _minute.isEmpty()) {
                        _value = _hour + ".0";
                    } else if (!_hour.isEmpty() && !_minute.isEmpty()) {
                        _value = _hour + "." + _minute;
                    }
                    if (_value.isEmpty()) {
                    } else {
                        SqliteClass.getInstance(context).databasehelp.otOperationsSql.updateOTOperation(id, "OPTIME", _value);
                        SqliteClass.getInstance(context).databasehelp.otOperationsSql.updateOTOperation(id, "DONE", "1");
                    }
                    if (_observation.isEmpty()) {
                    } else {
                        SqliteClass.getInstance(context).databasehelp.otOperationsSql.updateOTOperation(id, "OPOBS", _observation);
                    }
                    Toast.makeText(getContext(), "Información guardada con éxito.", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    listData();
                }
            }
        });
        Button dialogCancel = (Button) dialog.findViewById(R.id.alert_cancel);
        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void listData(){
        itemArray = new ArrayList<HashMap<String,String>>();
        itemList = new ArrayList<OTOperationsClass>();
        itemList = SqliteClass.getInstance(context).databasehelp.otOperationsSql.getOrderNumber(ConstValue.getOrderNumberString());
        for(int z=0; z < itemList.size(); z++){
            OTOperationsClass cc = itemList.get(z);
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("id", String.valueOf(cc.getId()));
            map.put("name", cc.getShortText());
            map.put("description",cc.getStartDate()+" "+cc.getStartTime()+" - "+cc.getEndDate()+" "+cc.getEndTime());
            map.put("status", cc.getStatus());
            itemArray.add(map);
        }
        adapter = new OTDetailAdapter(activity, itemArray);
        itemListView.setAdapter(adapter);
        if(itemList.size()>0){empty.setVisibility(rootView.GONE);} else {empty.setVisibility(rootView.VISIBLE);}
    }

}