package com.pm.views.fragments;

/**
 * Created by Johnny on 21/12/2018.
 */

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import com.pm.R;
import com.pm.adapters.OTAdapter;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.models.OTClass;
import com.pm.views.activities.OrderHActivity;

public class HomeFragment extends Fragment implements SearchView.OnQueryTextListener, MenuItem.OnActionExpandListener {
    List<OTClass> itemList;
    static ArrayList<HashMap<String, String>> itemArray;
    OTAdapter adapter;
    ListView itemListView;
    TextView count, empty;
    Switch today;
    Activity activity;
    Context context;
    int cnt;
    int res;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_home, container, false);
        activity = getActivity();
        itemArray = new ArrayList<HashMap<String,String>>();
        itemListView = (ListView) layout.findViewById(android.R.id.list);
        empty = (TextView) layout.findViewById(android.R.id.empty);
        count = (TextView) layout.findViewById(R.id.orderQuantity);
        today = (Switch) layout.findViewById(R.id.orderToday);
        today.setChecked(true);
        today.setText(R.string.drawer_today);
        today.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked){
                    today.setText(R.string.drawer_today);
                    getList(itemList, true);
                }else{
                    today.setText(R.string.drawer_allday);
                    getList(itemList, false);
                }
            }
        });
        itemList = SqliteClass.getInstance(context).databasehelp.otsql.getOT();
        getList(itemList, true);
        if(itemList.size()>0){empty.setVisibility(layout.GONE);} else {empty.setVisibility(layout.VISIBLE);}
        itemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                ConstValue.setOrderIdInt(Integer.valueOf(itemArray.get(arg2).get("id")));
                ConstValue.setOrderNumberString(itemArray.get(arg2).get("orderNumber"));
                ConstValue.setOrderTechnicalString(itemArray.get(arg2).get("objectId2"));
                ConstValue.setFragmentView("Order");
                // TODO Auto-generated method stub
                activity.finish();
                Intent intent = new Intent(activity,OrderHActivity.class);
                startActivityForResult(intent,100);
            }
        });
        return layout;
    }

    public void getList(List<OTClass> list, boolean _checked){
        itemArray = new ArrayList<HashMap<String,String>>();
        itemList = new ArrayList<OTClass>();
        itemList = list;
        cnt=0;res=0;
        for(int z=0; z < itemList.size(); z++){
            OTClass cc = itemList.get(z);
            if(ConstValue.getUserTypeString().equals("T")) {
                if(cc.getOrderReference().equals("ESSE") || cc.getOrderReference().equals("COMP") || cc.getOrderReference().equals("ESRP")){ res++;}else {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("id", String.valueOf(cc.getId()));
                    map.put("orderNumber", cc.getNumberOrder());
                    map.put("objectId", cc.getObjID());
                    map.put("objectId2", cc.getObjID2());
                    map.put("technicalLocation", cc.getTechnicalLocation());
                    map.put("_date", cc.getExtremeStartDate() + " " + cc.getExtremeEndDate());
                    map.put("orderText", cc.getText());
                    map.put("priority", cc.getPriority());
                    map.put("type", cc.getClassOrder());
                    map.put("approval", cc.getTechnicianClosing());
                    map.put("status", cc.getStatus());
                    if (cc.getStatus().equals("2")) {
                        cnt++;
                    }
                    SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                    Date todayDate = new Date();
                    Date otDate;
                    try {
                        otDate = formatDate.parse(cc.getExtremeStartDate());
                        if (_checked) {
                            if (otDate.before(todayDate) || otDate.equals(todayDate)) {
                                itemArray.add(map);
                            } else {
                            }
                        } else {
                            if (otDate.before(todayDate) || otDate.equals(todayDate)) {
                            } else {
                                itemArray.add(map);
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }else{
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("id", String.valueOf(cc.getId()));
                map.put("orderNumber", cc.getNumberOrder());
                map.put("objectId", cc.getObjID());
                map.put("objectId2", cc.getObjID2());
                map.put("technicalLocation", cc.getTechnicalLocation());
                map.put("_date", cc.getExtremeStartDate() + " " + cc.getExtremeEndDate());
                map.put("orderText", cc.getText());
                map.put("priority", cc.getPriority());
                map.put("type", cc.getClassOrder());
                map.put("approval", cc.getTechnicianClosing());
                map.put("status", cc.getStatus());
                if (cc.getStatus().equals("2")) {
                    cnt++;
                }
                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                Date todayDate = new Date();
                Date otDate;
                try {
                    otDate = formatDate.parse(cc.getExtremeStartDate());
                    if (_checked) {
                        if (otDate.before(todayDate) || otDate.equals(todayDate)) {
                            itemArray.add(map);
                        } else {
                        }
                    } else {
                        if (otDate.before(todayDate) || otDate.equals(todayDate)) {
                        } else {
                            itemArray.add(map);
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        adapter = new OTAdapter(activity, itemArray);
        itemListView.setAdapter(adapter);
        count.setText(String.valueOf(itemList.size()-res)+" | "+String.valueOf(cnt));
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Buscar...");
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText == null || newText.trim().isEmpty()) {
            resetSearch();
            return false;
        }
        List<OTClass> filteredValues = new ArrayList<OTClass>(itemList);
        for (OTClass value : itemList) {
            if (!value.getNumberOrder().toLowerCase().contains(newText.toLowerCase())) {
                filteredValues.remove(value);
            }
        }
        if(today.isChecked()){getList(filteredValues, true);}else{getList(filteredValues, false);}
        return false;
    }

    public void resetSearch() {
        itemList = SqliteClass.getInstance(context).databasehelp.otsql.getOT();
        if(today.isChecked()){getList(itemList, true);}else{getList(itemList, false);}
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return true;
    }
}