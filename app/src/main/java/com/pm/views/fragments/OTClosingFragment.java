package com.pm.views.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.pm.R;
import com.pm.adapters.OTClosingAdapter;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.models.OTClass;
import com.pm.models.OTComponentsClass;
import com.pm.models.OTMeasurementPointClass;
import com.pm.models.OTOperationsClass;
import com.pm.utils.Common;
import com.pm.utils.ConnectionDetector;
import com.pm.utils.Util;
import com.pm.views.activities.MainActivity;
import com.pm.views.activities.OrderHActivity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint("NewApi")
public class OTClosingFragment extends Fragment {
	List<OTClass> itemList;
	OTClosingAdapter otAdapter;
	static ArrayList<HashMap<String, String>> itemArray;
	ListView listOrder;
	TextView empty;
	Activity activity;
	Context context;
	Common common;
	ArrayList<OTClass> otLoad; ArrayList<OTClass> otLoadClosing;
	ArrayList<OTComponentsClass>otcomponentLoad;
	ArrayList<OTMeasurementPointClass>otmeasurementLoad;
	ArrayList<OTOperationsClass>otoperationtLoad;
	ProgressDialog dialogOTClosing;
	ProgressDialog dialogOTClosingUpdate;
	public ConnectionDetector cd;

	EditText observation;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	public void onDetach() {
		super.onDetach();
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.fragment_ot_closing, container, false);
		activity = getActivity();
		//context = getActivity().getApplicationContext();
		cd=new ConnectionDetector(activity);
		itemArray = new ArrayList<HashMap<String,String>>();
		itemList = new ArrayList<OTClass>();
		itemList = SqliteClass.getInstance(getActivity()).databasehelp.otsql.getOTReady();
		for (int z=0; z<itemList.size();z++){
			OTClass cc= itemList.get(z);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("id", String.valueOf(cc.getId()));
			map.put("orderNumber",cc.getNumberOrder());
			map.put("objectId2",cc.getObjID2());
			map.put("orderText", cc.getText());
			map.put("technicalLocation", cc.getTechnicalLocation());
			map.put("status", cc.getStatus());
			map.put("load", cc.getLoad());
			map.put("date_created",cc.getCreated());
			itemArray.add(map);
		}
		listOrder = (ListView) layout.findViewById(android.R.id.list);
		empty=(TextView) layout.findViewById(R.id.empty);
		otAdapter= new OTClosingAdapter(activity,itemArray);
		listOrder.setAdapter(otAdapter);
		if(itemList.size()>0){empty.setVisibility(layout.GONE);} else {empty.setVisibility(layout.VISIBLE);}
        listOrder.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int arg2, long id) {
				ConstValue.setOrderIdInt(Integer.valueOf(itemArray.get(arg2).get("id")));
				ConstValue.setOrderNumberString(itemArray.get(arg2).get("orderNumber"));
				ConstValue.setOrderTechnicalString(itemArray.get(arg2).get("objectId2"));
				ConstValue.setFragmentView("OrderClosing");
				// TODO Auto-generated method stub
				activity.finish();
				Intent intent = new Intent(activity,OrderHActivity.class);
				startActivityForResult(intent,100);
            }
        });


		listOrder.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                ConstValue.setFragmentView("OrderClosing");
				final ConnectionDetector cd = new ConnectionDetector(getActivity());
				final Dialog dialog = new Dialog(getActivity());
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialog_observation);
				TextView head = (TextView) dialog.findViewById(R.id.alert_title);
				head.setText("CloudPM");
				TextView content = (TextView) dialog.findViewById(R.id.alert_content);
				content.setText("Observación de Descargo de Técnico");
				observation = (EditText) dialog.findViewById(R.id.tx_input_observation);

				observation.setText(itemList.get(position).getObsi());

				if (!ConstValue.getUserTypeString().equals("T")) {
					observation.setEnabled(false);
				}

				Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
				dbOk.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if(ConstValue.getUserTypeString().equals("T")){
							if (itemList.get(position).getLoad().equals("1")) { //si el load == 1
								SqliteClass.getInstance(context).databasehelp.otsql.updateOT(itemList.get(position).getNumberOrder(),"OBSI",observation.getText().toString());
								if(cd.isConnectingToInternet()){
									new updateObservationTechnic().execute(true);
								}
							}
							else {
								SqliteClass.getInstance(context).databasehelp.otsql.updateOT(itemList.get(position).getNumberOrder(),"OBSI",observation.getText().toString());
							}
							dialog.dismiss();
						}
						else {
							dialog.dismiss();
						}
                        Intent intent = new Intent(activity, MainActivity.class);
                        startActivity(intent);
					}
				});
				Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
				dbCancel.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
				dialog.show();

				return true;
			}
		});

		return layout;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_order_closing, menu);
		menu.removeItem(R.id.action_synchronization);
		super.onCreateOptionsMenu(menu, inflater);
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_order_sync) {
			if(cd.isConnectingToInternet()) {
				final Dialog dialog = new Dialog(getActivity());
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.alert_ticket);
				ImageView image = (ImageView) dialog.findViewById(R.id.alert_send);
				image.setImageResource(R.drawable.ic_alert_info);
				TextView head = (TextView) dialog.findViewById(R.id.alert_ticket_title);
				head.setText("CloudPM - SINCRONIZAR");
				TextView content = (TextView) dialog.findViewById(R.id.alert_ticket_content);
				content.setText("¿Seguro de sincronizar la lista de OT pendientes?");
				Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
				dbOk.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						ConstValue.setFragmentView("OrderClosing");
						dialog.dismiss();
						if (ConstValue.getUserTypeString().equals("T")) {
							new OTClosingFragment.OTClosingTask().execute(true);
						} else {
							new OTClosingFragment.OTClosingUpdateTask().execute(true);
						}

					}
				});
				Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
				dbCancel.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
				dialog.show();
			}else{
				Toast.makeText(getActivity(), getString(R.string.app_no_connection), Toast.LENGTH_SHORT).show();
			}
		}
		return super.onOptionsItemSelected(item);
	}

	class OTClosingTask extends AsyncTask<Boolean, Void, String> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialogOTClosing = ProgressDialog.show(getActivity(), "CloudPM", getString(R.string.action_loading), true);
			super.onPreExecute();
		}
		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				Toast.makeText(context, "CloudPM " + result, Toast.LENGTH_LONG).show();
			} else {
				final Dialog dialog = new Dialog(getActivity());
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.alert_info);
				ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
				image.setImageResource(R.drawable.ic_alert_info);
				TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
				head.setText("CloudPM - SINCRONIZAR");
				TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
				content.setText("Se envío las OT pendientes al portal");
				Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
				dbOk.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						Intent intent = new Intent(activity, MainActivity.class);
						startActivity(intent);
					}
				});
				Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
				dbCancel.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						Intent intent = new Intent(activity, MainActivity.class);
						startActivity(intent);
					}
				});
				dialog.show();
			}
			// TODO Auto-generated method stub
			dialogOTClosing.dismiss();
		}
		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}
		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}
		@Override
		protected void onCancelled(String result) {
			// TODO Auto-generated method stub
			super.onCancelled(result);
		}
		@Override
		protected String doInBackground(Boolean... params) {
			// TODO Auto-generated method stub
			String responseString = null;
			otLoad = new ArrayList<OTClass>();
			otLoad = SqliteClass.getInstance(context).databasehelp.otsql.getPendingOT();
			try {
				JSONObject jsonobj;
				for (int z = 0; z < otLoad.size(); z++) {
					String total = Util.getDifferenceTime(otLoad.get(z).getWorkStartDate(),otLoad.get(z).getWorkStartTime(), otLoad.get(z).getWorkEndDate(), otLoad.get(z).getEndTimeWork());
					common = new Common();
					jsonobj = new JSONObject();
					jsonobj.put("AUFNR", otLoad.get(z).getNumberOrder());
					jsonobj.put("OBJID", ConstValue.getUserCodeString());
					jsonobj.put("TECHAPP", "1");
					jsonobj.put("CUSAPP", otLoad.get(z).getClientClosure());
					jsonobj.put("OBSH", otLoad.get(z).getObservation());
					jsonobj.put("BEGDA", otLoad.get(z).getWorkStartDate());
					jsonobj.put("BEGTI", otLoad.get(z).getWorkStartTime());
					jsonobj.put("ENDDA", otLoad.get(z).getWorkEndDate());
					jsonobj.put("ENDTI", otLoad.get(z).getEndTimeWork());
					jsonobj.put("TOTALHRS", total);
					jsonobj.put("OBSUP", "");
					jsonobj.put("ADVANCE", "");
					JSONArray jsonArrayOtComp = new JSONArray();
					JSONArray jsonArrayOtMesu = new JSONArray();
					JSONArray jsonArrayOtOper = new JSONArray();
					otcomponentLoad  = new ArrayList<OTComponentsClass>();
					otcomponentLoad =  SqliteClass.getInstance(context).databasehelp.otComponentsSql.getOrderNumber(otLoad.get(z).getNumberOrder());
					for (int i=0;i<otcomponentLoad.size();i++){
						JSONObject detailOtComp = new JSONObject();
						detailOtComp.put("AUFNR",otcomponentLoad.get(i).getNumberOrder());
						detailOtComp.put("VORNR",otcomponentLoad.get(i).getNumberOperation());
						detailOtComp.put("POSNR",otcomponentLoad.get(i).getPosition());
						detailOtComp.put("OBJID",otcomponentLoad.get(i).getObjID());
						detailOtComp.put("DONE",otcomponentLoad.get(i).getStatus());
						detailOtComp.put("CPOBS",otcomponentLoad.get(i).getObservation());
						detailOtComp.put("QUANTITY",otcomponentLoad.get(i).getAmountUsed());
						detailOtComp.put("MAKTX",otcomponentLoad.get(i).getItemDescription());
						detailOtComp.put("MATNR",otcomponentLoad.get(i).getItemCode());
						detailOtComp.put("MENGE",otcomponentLoad.get(i).getQuantity());
						detailOtComp.put("MEINS",otcomponentLoad.get(i).getUnitMeasure());
						jsonArrayOtComp.put(detailOtComp);
					}
					jsonobj.put("OTcomponent",jsonArrayOtComp);
					otmeasurementLoad = new ArrayList<OTMeasurementPointClass>();
					otmeasurementLoad = SqliteClass.getInstance(context).databasehelp.otMeasurementPointsSql.getOrderNumber(otLoad.get(z).getNumberOrder(), ConstValue.getOrderTechnicalString());
					for(int y=0; y<otmeasurementLoad.size(); y++){
						JSONObject detailOtMes = new JSONObject();
						detailOtMes.put("AUFNR", otmeasurementLoad.get(y).getNumberOrder());
						detailOtMes.put("POINT", otmeasurementLoad.get(y).getNumberPoint());
						detailOtMes.put("EQUNR", otmeasurementLoad.get(y).getEquipment());
						detailOtMes.put("VALUE", otmeasurementLoad.get(y).getMeasuredValue());
						detailOtMes.put("VALUE_DATE", otmeasurementLoad.get(y).get_date());
						detailOtMes.put("VALUE_TIME", otmeasurementLoad.get(y).getHour());
						detailOtMes.put("OBJID", ConstValue.getOrderTechnicalString());
						detailOtMes.put("TAKEN", otmeasurementLoad.get(y).getStatus());
						detailOtMes.put("NEQUNR", otmeasurementLoad.get(y).getEquipmentName());
						detailOtMes.put("PMOBS", otmeasurementLoad.get(y).getObservation());
						jsonArrayOtMesu.put(detailOtMes);
					}
					jsonobj.put("OTmeasure", jsonArrayOtMesu);
					otoperationtLoad = new ArrayList<OTOperationsClass>();
					otoperationtLoad = SqliteClass.getInstance(context).databasehelp.otOperationsSql.getOrderNumber(otLoad.get(z).getNumberOrder());
					for(int y=0; y<otoperationtLoad.size(); y++){
						JSONObject detailOtOpe = new JSONObject();
						detailOtOpe.put("AUFNR", otoperationtLoad.get(y).getNumberOrder());
						detailOtOpe.put("VORNR", otoperationtLoad.get(y).getNumberOperation());
						detailOtOpe.put("OBJID", otoperationtLoad.get(y).getObjID());
						detailOtOpe.put("DONE", otoperationtLoad.get(y).getStatus());
						detailOtOpe.put("OPOBS", otoperationtLoad.get(y).getObservation());
						detailOtOpe.put("OPTIME", otoperationtLoad.get(y).getTimeRequired());
						detailOtOpe.put("LTXA1", otoperationtLoad.get(y).getShortText());
						jsonArrayOtOper.put(detailOtOpe);
					}
					jsonobj.put("OToperation", jsonArrayOtOper);
					List<NameValuePair> nvps = new ArrayList<NameValuePair>();
					nvps.add(new BasicNameValuePair("data", jsonobj.toString()));
					JSONObject json = common.sendJsonData(ConstValue.JSON_OT, nvps);
					if (json.getString("response").equalsIgnoreCase("success")) {
						SqliteClass.getInstance(context).databasehelp.otsql.updateOT(otLoad.get(z).getNumberOrder(), "LOAD", "1");
					} else {
						responseString = json.getString("response");
					}
				}
			} catch (JSONException e) {
				Log.e("JSON Parser", "Error parsing data " + e.toString());
			}
			return responseString;
		}
	}

	class OTClosingUpdateTask extends AsyncTask<Boolean, Void, String> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialogOTClosingUpdate = ProgressDialog.show(getActivity(), "CloudPM", getString(R.string.action_loading), true);
			super.onPreExecute();
		}
		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				Toast.makeText(context, "CloudPM: " + result, Toast.LENGTH_LONG).show();
			} else {
				final Dialog dialog = new Dialog(getActivity());
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.alert_info);
				ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
				image.setImageResource(R.drawable.ic_alert_info);
				TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
				head.setText("CloudPM - SINCRONIZAR");
				TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
				content.setText("Se envío las OT pendientes al portal");
				Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
				dbOk.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						Intent intent = new Intent(activity, MainActivity.class);
						startActivity(intent);
					}
				});
				Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
				dbCancel.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						Intent intent = new Intent(activity, MainActivity.class);
						startActivity(intent);
					}
				});
				dialog.show();
			}
			// TODO Auto-generated method stub
			dialogOTClosingUpdate.dismiss();
		}
		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}
		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}
		@Override
		protected void onCancelled(String result) {
			// TODO Auto-generated method stub
			super.onCancelled(result);
		}
		@Override
		protected String doInBackground(Boolean... params) {
			// TODO Auto-generated method stub
			String responseString = null;
			otLoadClosing = new ArrayList<OTClass>();
			otLoadClosing = SqliteClass.getInstance(context).databasehelp.otsql.getPendingOT();
			try {
				JSONObject jsonobj;
				for (int z = 0; z < otLoadClosing.size(); z++) {
					common = new Common();
					jsonobj = new JSONObject();

					jsonobj.put("AUFNR", otLoadClosing.get(z).getNumberOrder());
					jsonobj.put("OBJID", otLoadClosing.get(z).getObjID());
					jsonobj.put("CUSAPP", otLoadClosing.get(z).getClientClosure());
					jsonobj.put("OBSUP", otLoadClosing.get(z).getObservationc());
					jsonobj.put("ADVANCE", ConstValue.getOrderAdvance());

					List<NameValuePair> nvps = new ArrayList<NameValuePair>();
					nvps.add(new BasicNameValuePair("data", jsonobj.toString()));

					String jsonString = jsonobj.toString();
					JSONObject json = common.sendJsonData(ConstValue.JSON_OT_POST, nvps);
					if (json.getString("response").equalsIgnoreCase("success")) {
						SqliteClass.getInstance(context).databasehelp.otsql.updateOT(otLoadClosing.get(z).getNumberOrder(), "LOAD", "1");
					} else {
						responseString = json.getString("response");
					}
				}
			} catch (JSONException e) {
				Log.e("JSON Parser", "Error parsing data " + e.toString());
			}
			return responseString;
		}
	}

	class updateObservationTechnic extends AsyncTask<Boolean, Void, String> {
		@Override
		protected String doInBackground(Boolean... params) {
			// TODO Auto-generated method stub
			String responseString = null;
			try {
				JSONObject jsonobj;
				common = new Common();
				jsonobj = new JSONObject();

				jsonobj.put("AUFNR", ConstValue.getOrderNumberString());
				jsonobj.put("OBJID", ConstValue.getOrderTechnicalString());
				jsonobj.put("OBSI", observation.getText().toString());

				List <NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("data", jsonobj.toString()));

				JSONObject jObj = common.sendJsonData(ConstValue.JSON_INCIDENCE, nvps);
				if(jObj.getString("response").equalsIgnoreCase("success")){
					System.out.println("Se envio con exito");

					responseString = null;
				}else{
					responseString = jObj.getString("response");
				}

			} catch (JSONException e) {
				Log.e("JSON Parser", "Error parsing data " + e.toString());
			}

			return responseString;
		}



		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialogOTClosingUpdate = ProgressDialog.show(getActivity(), "", "Enviando observación al servidor. Por favor, espere ...", true);
			super.onPreExecute();
		}
		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				Toast.makeText(getActivity(), "CloudPM: " + result, Toast.LENGTH_LONG).show();
			} else {
				final Dialog dialog = new Dialog(getActivity());
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.alert_info);
				ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
				image.setImageResource(R.drawable.ic_alert_info);
				TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
				head.setText("CloudPM");
				TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
				content.setText("Se envió la Observación de Descargo.");
				Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
				dbOk.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						Intent intent = new Intent(activity, MainActivity.class);
						startActivity(intent);
					}
				});
				Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
				dbCancel.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
						Intent intent = new Intent(activity, MainActivity.class);
						startActivity(intent);
					}
				});
				dialog.show();
			}
			// TODO Auto-generated method stub
			dialogOTClosingUpdate.dismiss();
		}
		private ArrayList<HashMap<String, String>> get_items() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}
		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			super.onCancelled();
		}

		@Override
		protected void onCancelled(String result) {
			// TODO Auto-generated method stub
			super.onCancelled(result);
		}
	}

}
