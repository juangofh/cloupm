package com.pm.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.pm.R;

public class MyCustomProgressDialog extends ProgressDialog {
    private AnimationDrawable animation;AnimationDrawable frameAnimation;
    public  MyCustomProgressDialog(Context context) {
        super(context);
    }
    public static ProgressDialog ctor(Context context) {
        MyCustomProgressDialog dialog = new MyCustomProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        return dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_custom_progress_dialog);

        ImageView la = (ImageView) findViewById(R.id.animation);
        la.setBackgroundResource(R.color.trans);
        la.setBackgroundResource(R.drawable.custom_progress_dialog_animation);
        frameAnimation = (AnimationDrawable) la.getBackground();
    }
    @Override
    public void show() {
        super.show();
        frameAnimation.start();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        frameAnimation.stop();
    }
}
