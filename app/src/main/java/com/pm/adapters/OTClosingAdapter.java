package com.pm.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pm.R;
import com.pm.utils.Common;
import com.pm.utils.Util;


public class OTClosingAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<HashMap<String, String>> postOrder;

	public OTClosingAdapter(Context context, ArrayList<HashMap<String, String>> postOrder) {
		this.context = context;
		this.postOrder = postOrder;
	}

	@Override
	public int getCount() {
		return postOrder.size();
	}

	@Override
	public Object getItem(int position) {
		return postOrder.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater)
					context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.row_otclosing, null);
		}
		final HashMap<String, String> map = postOrder.get(position);

		ImageView imgOrder = (ImageView)convertView.findViewById(R.id.img_otpending);
		if(map.get("load").equals("1")){
			imgOrder.setImageResource(R.drawable.ic_ticket_ok);
		}else{
			imgOrder.setImageResource(R.drawable.ic_ticket_pending);
		}

		TextView ticketNumber= (TextView)convertView.findViewById(R.id.tv_otpendingNumber);
		ticketNumber.setText("OT "+map.get("orderNumber")+ " - " + map.get("technicalLocation"));

		TextView typeOrder = (TextView)convertView.findViewById(R.id.tv_otpendingType);
		typeOrder.setText(map.get("orderText"));

		TextView dateCreated = (TextView) convertView.findViewById(R.id.tx_date_created);
		dateCreated.setText("F. SAP: " + Util.convertHourFromServer(map.get("date_created")));
		return convertView;
	}
}

