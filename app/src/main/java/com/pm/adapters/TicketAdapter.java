package com.pm.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pm.R;

import java.util.ArrayList;
import java.util.HashMap;

public class TicketAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<HashMap<String, String>> postTickets;

    public TicketAdapter(Context context, ArrayList<HashMap<String, String>> postTickets) {
        this.context = context;
        this.postTickets = postTickets;
    }

    @Override
    public int getCount() {
        return postTickets.size();
    }

    @Override
    public Object getItem(int position) {
        return postTickets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_ticket, null);
        }
        final HashMap<String, String> map = postTickets.get(position);

        ImageView imgTicket = (ImageView)convertView.findViewById(R.id.img_ticket);
        if(map.get("approval").equals("1")){
            imgTicket.setImageResource(R.drawable.ic_ticket_ok);
        }else if(map.get("approval").equals("2")){
            imgTicket.setImageResource(R.drawable.ic_ticket_rejected);
        }else {
            imgTicket.setImageResource(R.drawable.ic_ticket_pending);
        }

        TextView ticketNumber= (TextView)convertView.findViewById(R.id.tv_ticketNumber);
        ticketNumber.setText("Ticket # "+map.get("ticketNumber")+ " - " + map.get("noticeTypeN"));

        TextView typeTicket = (TextView)convertView.findViewById(R.id.tv_ticketType);
        typeTicket.setText(map.get("equipment")+" - "+map.get("equipmentName"));

        return convertView;
    }
}
