package com.pm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.pm.config.ConstValue;
import com.pm.db.SqliteClass;
import com.pm.imgLoader.AnimateFirstDisplayListener;

import com.pm.R;

public class OTAdapter extends BaseAdapter {
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    private Context context;
    private ArrayList<HashMap<String, String>> postItems;
    public SharedPreferences settings;
    public final String PREFS_NAME = "OT";

    DisplayImageOptions options;
    ImageLoaderConfiguration imgconfig;

    public OTAdapter(Context context, ArrayList<HashMap<String, String>> arraylist){
        this.context = context;
        File cacheDir = StorageUtils.getCacheDirectory(context);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.loading)
                .showImageForEmptyUri(R.drawable.loading)
                .showImageOnFail(R.drawable.loading)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new SimpleBitmapDisplayer())
                .imageScaleType(ImageScaleType.EXACTLY)
                .build();
        imgconfig = new ImageLoaderConfiguration.Builder(context)
                .build();
        ImageLoader.getInstance().init(imgconfig);
        postItems = arraylist;
        settings = context.getSharedPreferences(PREFS_NAME, 0);
    }
    @Override
    public int getCount() {
        return postItems.size();
    }
    @Override
    public Object getItem(int position) {
        return postItems.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_order, null);
        }
        final HashMap<String, String> map = postItems.get(position);

        TextView imgOrderClass = (TextView) convertView.findViewById(R.id.orderClass);
        if(map.get("type").equals("ZPM1")){
            imgOrderClass.setTextColor(Color.parseColor(ConstValue.getColorEmergencyString()));
        }else if(map.get("type").equals("ZPM2")){
            imgOrderClass.setTextColor(Color.parseColor(ConstValue.getColorRoutineString()));
        }else if(map.get("type").equals("ZPM3")){
            imgOrderClass.setTextColor(Color.parseColor(ConstValue.getColorOtherString()));
        }
        LinearLayout ll = (LinearLayout) convertView.findViewById(R.id.status);
        if(map.get("status").equals("3")){
            ll.setBackgroundColor(Color.parseColor(ConstValue.getColorRefuseString()));
        }else if(map.get("status").equals("2")){
            ll.setBackgroundColor(Color.parseColor(ConstValue.getColorFinishString()));
            //imgOrderClass.setTextColor(Color.parseColor(ConstValue.getColorFinishString()));
        }else if(map.get("status").equals("1")){
            ll.setBackgroundColor(Color.parseColor(ConstValue.getColorProcessString()));
        }else {
            ll.setBackgroundColor(Color.parseColor(ConstValue.getColorPendingString()));
        }

        TextView txtNumber = (TextView)convertView.findViewById(R.id.orderNumber);

        if(ConstValue.getUserTypeString().equals("C")){
            String advance = "";
            int total = SqliteClass.getInstance(context).databasehelp.otsql.countOrderStatus(map.get("orderNumber"),"ALL");
            int finish = SqliteClass.getInstance(context).databasehelp.otsql.countOrderStatus(map.get("orderNumber"),"F");
            if(finish==0){
                advance = "<font color="+ConstValue.getColorPendingString()+">0%</font>";
                ConstValue.setOrderAdvance("0");
            }else if (total==finish){
                advance = "<font color="+ConstValue.getColorFinishString()+">100%</font>";
                ConstValue.setOrderAdvance("100");
            }else{
                float result = (finish*100)/total;
                String _advance = String.format("%.2f", result);
                advance = "<font color="+ConstValue.getColorProcessString()+">"+_advance+"%</font>";
                ConstValue.setOrderAdvance(_advance);
            }

            txtNumber.setText(Html.fromHtml(map.get("orderNumber")+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>AVANCE: "+advance+"</small>"));
            /*if(map.get("approval").equals("1")) {
                txtNumber.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_tech_check, 0, 0, 0);
            }else{
                txtNumber.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_tech, 0, 0, 0);
            }*/

        }else{
            txtNumber.setText(map.get("orderNumber"));
        }

        TextView txtLocation = (TextView)convertView.findViewById(R.id.orderLocation);
        txtLocation.setText(map.get("technicalLocation"));

        TextView txtDate = (TextView)convertView.findViewById(R.id.orderDate);
        txtDate.setText(map.get("_date"));

        TextView txtText = (TextView)convertView.findViewById(R.id.orderText);
        txtText.setText(map.get("orderText"));
        ImageView ivPriority = (ImageView)convertView.findViewById(R.id.orderPriority);
        if(map.get("priority").equals("1")){
            ivPriority.setVisibility(View.VISIBLE);
            ivPriority.setImageResource(R.drawable.ic_priority_very_high);
        /*}else if(map.get("priority").equals("2")){
            ivPriority.setVisibility(View.VISIBLE);
            ivPriority.setImageResource(R.drawable.ic_priority_high);*/
        }else{
            ivPriority.setVisibility(View.GONE);
        }
        return convertView;
    }
}
