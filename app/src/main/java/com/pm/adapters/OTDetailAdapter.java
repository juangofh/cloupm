package com.pm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pm.R;

import java.util.ArrayList;
import java.util.HashMap;


public class OTDetailAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<HashMap<String, String>> postItems;
	public SharedPreferences settings;
	public final String PREFS_NAME = "Event";

	public OTDetailAdapter(Context context, ArrayList<HashMap<String, String>> arraylist){
		this.context = context;

		postItems = arraylist;
		settings = context.getSharedPreferences(PREFS_NAME, 0);
	}
	@Override
	public int getCount() {
		return postItems.size();
	}
	@Override
	public Object getItem(int position) {
		return postItems.get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater)
			context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.row_ot, null);
		}
		final HashMap<String, String> map = postItems.get(position);

		ImageView imgOT = (ImageView)convertView.findViewById(R.id.iv_ot_status);
		if(map.get("status").equals("0")){
			imgOT.setImageResource(R.drawable.ic_check_box_outline);
		}else{
			imgOT.setImageResource(R.drawable.ic_check_box);
		}

		TextView txtName = (TextView)convertView.findViewById(R.id.tv_ot_name);
		txtName.setText(map.get("name"));

		TextView txtDateHour = (TextView)convertView.findViewById(R.id.tv_ot_description);
		txtDateHour.setText(map.get("description"));

        return convertView;
	}
}

